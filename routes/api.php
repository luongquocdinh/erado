<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::delete('image/{id}', 'Admin\ImageController@delete')->name('admin.image.delete');
Route::put('partner/icon/{id}', 'Admin\PartnerController@deleteIcon')->name('admin.partner.icon.delete');

// Product change status
Route::get('/manager/product/{id}/changeStatus', 'Admin\ProductController@changeStatus')->name('admin.product.changeStatus');

// Partner change status
Route::get('/manager/partner/{id}/changeStatus', 'Admin\PartnerController@changeStatus')->name('admin.partner.changeStatus');

// Category change status
Route::get('/manager/category/{id}/changeStatus', 'Admin\CategoryController@changeStatus')->name('admin.category.changeStatus');
