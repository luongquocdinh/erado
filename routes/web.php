<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('admin.pages.index');
// });
Route::post('language', 'LocaleController@change')->name('language');

Route::group([
    'prefix' => 'manager'
], function () {
    Auth::routes(); // router=login
    Route::group(['namespace' => 'Admin', 'middleware' => 'admin'], function () {
        Route::get('/', 'IndexController@index')->name('admin.home');
        
        // Product
        Route::get('/product', 'ProductController@list')->name('admin.product.get');
        Route::get('/product/add', 'ProductController@create')->name('admin.product.create');
        Route::post('/product/store', 'ProductController@store')->name('admin.product.store');
        Route::get('/product/{id}', 'ProductController@edit')->name('admin.product.edit');
        Route::post('/product/{id}', 'ProductController@update')->name('admin.product.update');

        // Category
        Route::get('/categories', 'CategoryController@list')->name('admin.category.get');
        Route::get('/category/add', 'CategoryController@create')->name('admin.category.create');
        Route::post('/category/store', 'CategoryController@store')->name('admin.category.store');
        Route::get('/category/{id}', 'CategoryController@edit')->name('admin.category.edit');
        Route::post('/category/{id}', 'CategoryController@update')->name('admin.category.update');

        // news
        Route::get('/news', 'NewsController@list')->name('admin.news.get');
        Route::get('/news/add', 'NewsController@create')->name('admin.news.create');
        Route::post('/news/store', 'NewsController@store')->name('admin.news.store');
        Route::get('/news/{id}', 'NewsController@edit')->name('admin.news.edit');
        Route::post('/news/{id}', 'NewsController@update')->name('admin.news.update');

        // recruitment
        Route::get('/recruitment', 'RecruitmentController@list')->name('admin.recruitment.get');
        Route::get('/recruitment/add', 'RecruitmentController@create')->name('admin.recruitment.create');
        Route::post('/recruitment/store', 'RecruitmentController@store')->name('admin.recruitment.store');
        Route::get('/recruitment/{id}', 'RecruitmentController@edit')->name('admin.recruitment.edit');
        Route::post('/recruitment/{id}', 'RecruitmentController@update')->name('admin.recruitment.update');

        // about
        Route::get('/about_us', 'AboutController@list')->name('admin.about_us.get');
        Route::get('/about_us/add', 'AboutController@create')->name('admin.about_us.create');
        Route::post('/about_us/store', 'AboutController@store')->name('admin.about_us.store');
        Route::get('/about_us/{id}', 'AboutController@edit')->name('admin.about_us.edit');
        Route::post('/about_us/{id}', 'AboutController@update')->name('admin.about_us.update');

        // Partner
        Route::get('/partners', 'PartnerController@list')->name('admin.partner.get');
        Route::get('/partner/add', 'PartnerController@create')->name('admin.partner.create');
        Route::post('/partner/store', 'PartnerController@store')->name('admin.partner.store');
        Route::get('/partner/{id}', 'PartnerController@edit')->name('admin.partner.edit');
        Route::post('/partner/{id}', 'PartnerController@update')->name('admin.partner.update');

    });
});

Route::group(['namespace' => 'Client', 'middleware' => 'lang'], function () {
    /*Route::get('/', 'IndexController@index')->name('client.home');
    Route::get('/product', 'ProductController@index')->name('client.product');
    Route::get('/product/{slug}', 'ProductController@detail')->name('client.product.detail');*/
    Route::get('/', 'IndexController@index')->name('client.home');
    Route::get('/{lang}/product/', 'ProductController@index')->name('client.product');
    Route::get('/news/getProductByCat', 'ProductController@index')->name('client.get_product_by_cat');
    Route::post('/news/getProductByCat', 'ProductController@getProductByCat')->name('client.getproductByCat');
    Route::get('/{lang}/product/detail/{slug}'.'.html', 'ProductController@detail')->name('client.product_detail');

//    Route::get('/{lang}/news/{cat}', 'NewsController@index')->name('client.news');
    Route::get('/{lang}/news/', 'NewsController@index')->name('client.news');
    Route::get('/news/getNewsByCat', 'NewsController@index')->name('client.get_news_by_cat');
    Route::post('/news/getNewsByCat', 'NewsController@getNewsByCat')->name('client.getNewsByCat');
    Route::get('/{lang}/news/detail/{slug}'.'.html', 'NewsController@detail')->name('client.news_detail');

    Route::get('/{lang}/recruitment/', 'RecruitmentController@index')->name('client.recruitment');
    Route::get('/{lang}/recruitment/detail/{slug}'.'.html', 'RecruitmentController@detail')->name('client.recruitment_detail');

    Route::get('/{lang}/about_us/', 'AboutController@index')->name('client.about_us');
    Route::get('/{lang}/about_us/detail/{slug}'.'.html', 'AboutController@detail')->name('client.about_us_detail');
});
