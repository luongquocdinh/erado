<?php

return [
    'bar_management' => 'THANH QUẢN LÝ',
    'product' => 'Sản Phẩm',
    'news' => 'Tin tức',
    'category' => 'Danh mục',
    'recruitment' => 'Tuyển dụng',
    'order' => 'Đơn hàng',
    'about_us' => 'Về chúng tôi',
    'vn'    => 'Tiếng Việt',
    'en'    => 'Tiếng Anh',
    'meta' => 'Meta',
    // Table
    'No' => 'STT',
    'name' => 'Tên',
    'active' => 'Trạng thái',
    'action' => 'Hoạt động',
    'add' => 'Thêm mới',
    'edit' => 'Chỉnh sửa',
    'created_at' => 'Ngày',

    // Product
    'type' => 'Loại',
    'product_type_1' => 'Loại 1',
    'product_type_2' => 'Loại 2',
    'product_type_3' => 'Loại 3',
    'tag'   => 'Tag',
    'unit'  => 'Đơn vị tiền',

    // Database
    'description' => 'Mô tả',
    'enable' => 'Hoạt động',
    'disable' => 'Tạm dừng',

    // Form
    'min_price' => 'Giá thấp nhất',
    'max_price' => 'Giá cao nhất',
    'sapo'      => 'Đoạn mô tả ngắn',
    'language'  => 'Ngôn ngữ',
    'sku'       => 'Sku',

    // news
    'title' => 'Tiêu đề',
    'author' => 'Tác giả',
    'content' => 'Nội dung',
    'images' => 'Hình ảnh',
    'edit' => 'Sửa',
    'update' => 'Cập nhật',

    // partner
    'partner' => 'Đối tác',
    'link'  => 'Website',
];