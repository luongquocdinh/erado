<?php

return [
    'notification' => 'Thông báo',
    'success' => 'Thành công ',
    'error'   => 'Xảy ra lỗi ',
    'create'  => 'Tạo ',
    'update'  => 'Cập nhật '
];