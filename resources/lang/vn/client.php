<?php

return [
    // home
    'home' => 'Trang chủ',
    // product
    'product' => 'Sản phẩm',
    // news
    'news' => 'Tin tức',
    'read_more' => 'Xem thêm...',

    // recruitment
    'recruitment' => 'Tuyển dụng',
    'detail' => 'Chi tiết',

    //about
    'about' => 'Về chúng tôi'

];