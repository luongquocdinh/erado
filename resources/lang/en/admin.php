<?php

return [
    'bar_management' => 'MANAGEMENT',
    'product' => 'Product',
    'news' => 'News',
    'category' => 'Category',
    'recruitment' => 'Recruitments',
    'order' => 'Order',
    'about_us' => 'About Us',
    'vn'    => 'Vietnamese',
    'en'    => 'English',
    'meta' => 'Metadescription',
    // Table
    'No' => 'No',
    'name' => 'Name',
    'active' => 'Active',
    'action' => 'Action',
    'add' => 'Add New',
    'edit' => 'Edit',
    'created_at' => 'Date',

    // Product
    'type' => 'Type',
    'product_type_1' => 'Type 1',
    'product_type_2' => 'Type 2',
    'product_type_3' => 'Type 3',
    'tag'   => 'Tag',
    'unit'  => 'Unit money',

    // Database
    'description' => 'Description',
    'enable' => 'Enable',
    'disable' => 'Disable',

    // Form
    'min_price' => 'Min Price',
    'max_price' => 'Max Price',
    'sapo'      => 'Brief',
    'language'  => 'Language',
    'sku'       => 'Sku',

    // news
    'title' => 'Title',
    'author' => 'Author',
    'content' => 'Content',
    'images' => 'Images',
    'edit' => 'Edit',
    'update' => 'Update',

    // partner
    'partner' => 'Partner',
    'link'  => 'Website',
];