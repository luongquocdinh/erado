<?php

return [
    // home
    'home' => 'Home',
    //product
    'product' => 'Product',
    // news
    'news' => 'News',
    'read_more' => 'Read more...',

    // recruitment
    'recruitment' => 'Recruitment',
    'detail' => 'Detail',

    // about
    'about_us' => 'About'
];