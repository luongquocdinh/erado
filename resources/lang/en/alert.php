<?php

return [
    'notification' => 'Notification',
    'success' => 'Successful',
    'error'   => 'Error',
    'create'  => 'Created ',
    'update'  => 'Update '
];