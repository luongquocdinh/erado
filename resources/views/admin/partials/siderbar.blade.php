@php
    // $routeRoute = \Illuminate\Support\Facades\Request::route();
    // $str = '';
    // if ($routeRoute){
    //     $route = $routeRoute->getName();
    //     $str = substr($route, 0, strpos($route, '.'));
    // }
@endphp
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel" style="height: 40px;">
        <div class="pull-left info">
            <strong>{{ (\Illuminate\Support\Facades\Auth::check()) ? \Illuminate\Support\Facades\Auth::user()->name : null }}</strong>
        </div>
    </div>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <li class="header">@lang('admin.bar_management')</li>
        <li class="treeview">
            <a href="{{ route('admin.product.get') }}">
                <i class="fa fa-database"></i> <span>@lang('admin.product')</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route('admin.category.get') }}">
                <i class="fa  fa-list"></i> <span>@lang('admin.category')</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route('admin.news.get') }}">
                <i class="fa fa-newspaper-o"></i> <span>@lang('admin.news')</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route('admin.recruitment.get') }}">
                <i class="fa fa-users"></i> <span>@lang('admin.recruitment')</span>
            </a>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-folder-open"></i> <span>@lang('admin.order')</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{route('admin.about_us.get')}}">
                <i class="fa fa-home"></i> <span>@lang('admin.about_us')</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{ route('admin.partner.get') }}">
                <i class="fa fa-users"></i> <span>@lang('admin.partner')</span>
            </a>
        </li>
    </ul>
</section>