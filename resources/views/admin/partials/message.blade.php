@if(\Illuminate\Support\Facades\Session::has('error'))
	<div class="row clearfix">
		<div class="alert alert-error" style="margin: 1em 2em 0 2em">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>@lang('alert.notification')!</strong> {{ Session::get('error') }}
		</div>
	</div>
@endif

@if(\Illuminate\Support\Facades\Session::has('success'))
	<div class="row clearfix">
		<div class="alert alert-success" style="margin: 1em 2em 0 2em">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>@lang('alert.notification')!</strong> {{ Session::get('success') }}
		</div>
	</div>
@endif