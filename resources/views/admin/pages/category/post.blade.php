@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('admin') }}/plugins/bootstrap-fileinput/css/fileinput.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin') }}/plugins/select2/select2.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin') }}/plugins/bootstrap-fileinput/themes/explorer-fas/theme.css">
@endsection


@section('content')
<section class="content" data-locale={{ Session::get('locale') }}>
    @if (isset($detail))
        <h1>@lang('admin.category') {{$detail->name}}</h1>
    @else
        <h1>@lang('admin.add') @lang('admin.category')</h1>
    @endif
    <div class="box box-info">
        <!-- /.box-header -->
        <div class="box-body pad">
            <form role="form" action="{{ isset($detail) ? route('admin.category.update', $detail->id) : route('admin.category.store') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('admin.name') (@lang('admin.vn'))</label>
                            <input type="text" name="name_vn" value="{{ isset($detail) ? $detail->name_vn : '' }}" class="form-control" required />
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('admin.name') (@lang('admin.en'))</label>
                            <input type="text" name="name_en" value="{{ isset($detail) ? $detail->name_en : '' }}" class="form-control" required />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('admin.title') (@lang('admin.vn'))</label>
                            <input type="text" name="title_vn" value="{{ isset($detail) ? $detail->title_vn : '' }}" class="form-control" required />
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('admin.title') (@lang('admin.en'))</label>
                            <input type="text" name="title_en" value="{{ isset($detail) ? $detail->title_en : '' }}" class="form-control" required />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('admin.meta') (@lang('admin.vn'))</label>
                            <input type="text" name="meta_vn" value="{{ isset($detail) ? $detail->meta_vn : '' }}" class="form-control" required />
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('admin.meta') (@lang('admin.en'))</label>
                            <input type="text" name="meta_en" value="{{ isset($detail) ? $detail->meta_en : '' }}" class="form-control" required />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <label>@lang('admin.description') (@lang('admin.vn'))</label>
                        <textarea name="description_vn" class="form-control" required="required">
                            {{ isset($detail) ? $detail->description_vn : '' }}
                        </textarea>
                    </div>

                    <div class="col-md-6">
                        <label>@lang('admin.description') (@lang('admin.en'))</label>
                        <textarea name="description_en" class="form-control" required="required">
                            {{ isset($detail) ? $detail->description_en : '' }}
                        </textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label>@lang('admin.type')</label>
                    <select for="type" name="type" class="form-control">
                        <option value="1" >@lang('admin.product')</option>
                        <option value="2">@lang('admin.news')</option>
                    </select>
                </div>

                <div class="form-group">
                    <div class="file-loading">
                        <input id="kv-explorer" name="images[]" type="file" multiple>
                    </div>
                </div>


                <button type="submit" class="btn btn-success" >Submit</button>
            </form>
        </div>
    </div>
</section>
@endsection


@section('script')
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/js/fileinput.js"></script>
    <script src="{{ asset('admin') }}/plugins/select2/select2.js"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/js/plugins/piexif.js"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/js/plugins/sortable.js"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/js/locales/vi.js" type="text/javascript"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/js/locales/LANG.js" type="text/javascript"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/themes/fas/theme.js"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/themes/explorer-fas/theme.js"></script>
    <script>

        $(document).ready(function() {
            var language = $('.content').data('locale');

            $("#kv-explorer").fileinput({
                theme: 'explorer-fas',
                uploadUrl: '#',
                ajaxDeleteSettings: { method: "DELETE" },
                language: language,
                overwriteInitial: false,
                uploadAsync: false,
                fileActionSettings: {
                    showZoom: false,
                    showUpload: false,
                    showRemove: true
                },
                initialPreviewAsData: true,
                initialPreviewFileType: 'image',
                showUpload: false,
                initialPreview: [
                    @if (isset($images))
                            @foreach ($images as $image)
                        window.location.origin + "/{{ $image->path }}",
                    @endforeach
                    @endif
                ],
                initialPreviewConfig: [
                        @if (isset($images))
                        @foreach ($images as $image)
                    {type: 'image', caption: "{{ $image->name }}", url: "/api/image/{{ $image->id }}", key: "{{ $image->id }}"},
                    @endforeach
                    @endif
                ],
            });

            $("#kv-explorer").on("filepredelete", function(jqXHR) {
                var abort = true;
                if (confirm("Are you sure you want to delete this image?")) {
                    abort = false;
                }
                return abort; // you can also send any data/object that you can receive on `filecustomerror` event
            });
        });

    </script>
@endsection