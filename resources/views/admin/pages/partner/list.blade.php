@extends('admin.layouts.main')

@section('content')
<section class="content">
    <h1>@lang('admin.partner')</h1>
    <div class="box box-primary">
        <div class="box-header">
            <a href="{{ route('admin.partner.create') }}" class="btn btn-success">
                <i class="fa fa-plus"></i> @lang('admin.add')
            </a>
        </div>
        <div class="box-body">
            <table id="list_training" class="table table-bordered table-striped text-center">
                <thead>
                    <tr>
                        <th>@lang('admin.No')</th>
                        <th>@lang('admin.name')</th>
                        <th>@lang('admin.active')</th>
                        <th>@lang('admin.action')</th>
                    </tr>
                </thead>

                <tbody>
                        @foreach($data as $index => $item)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td><a href="{{ route('admin.partner.edit', $item->id) }}">{{ $item->name }}</a></td>
                                <td>
                                    @if( $item->is_active == 1 )
                                        <span id="{{ $item->id }}" class="label label-success">@lang('admin.enable')</span>
                                    @else
                                        <span id="{{ $item->id }}" class="label label-danger">@lang('admin.disable')</span>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle"
                                                data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a id="status_{{ $item['id'] }}" class="changeStatus" data-id="{{ $item['id'] }}">
                                                    @if ($item['is_active'])
                                                        @lang('admin.disable')
                                                    @else
                                                        @lang('admin.enable')
                                                    @endif
                                                </a>
                                            </li>
                                            <li>
                                                <a href="{{ route('admin.partner.edit', $item->id) }}">@lang('admin.edit')</a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                </tbody>
            </table>
            <div class="pull-right">
                {{ $data->links() }}
            </div>
        </div>
    </div>
</section>

@endsection

@section('script')
    <script type="text/javascript">
        $("a.changeStatus").click(function (event) {
            event.preventDefault();
            id = $(this).data('id');
            $.ajax({
                type: "GET",
                url: '/api/manager/partner/' + id + '/changeStatus',
                success: function (response) {
                    $("#" + id).removeClass();
                    $("#" + id).addClass('label ' + (response.data.is_active ? "label-success" : "label-danger"));
                    $("#" + id).html(response.data.is_active ? "{{ @trans('admin.enable') }}" :"{{ @trans('admin.disable') }}" );
                    $('#status_' + id).html(response.data.is_active ? "{{ @trans('admin.disable') }}" : "{{ @trans('admin.enable') }}" );
                },
                error: function (err) {
                    console.log(err);
                    alert("Server Error!!!");
                }
            })
        })
    </script>
@endsection