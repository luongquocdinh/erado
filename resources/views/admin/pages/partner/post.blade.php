@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('admin') }}/plugins/bootstrap-fileinput/css/fileinput.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin') }}/plugins/bootstrap-fileinput/themes/explorer-fas/theme.css">
@endsection

@section('content')
    <section class="content" data-locale={{ Session::get('locale') }}>
        @if (isset($detail))
            <h1>@lang('admin.partner') {{$detail->name}}</h1>
        @else
            <h1>@lang('admin.add') @lang('admin.partner')</h1>
        @endif
        <div class="box box-info">
            <div class="box-body pad">
                <form role="form" action="{{ isset($detail) ? route('admin.partner.update', $detail->id) : route('admin.partner.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>@lang('admin.name')</label>
                        <input type="text" name="name" value="{{ isset($detail) ? $detail->name : '' }}" class="form-control" required />
                    </div>

                    <div class="form-group">
                        <label>@lang('admin.link')</label>
                        <input type="text" name="link" value="{{ isset($detail) ? $detail->link : '' }}" class="form-control" required />
                    </div>

                    <div class="form-group">
                        <div class="file-loading">
                            <input id="upload_partner" name="icon" type="file">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success" >Submit</button>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/js/fileinput.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/js/plugins/piexif.js"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/js/plugins/sortable.js"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/js/locales/vi.js" type="text/javascript"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/js/locales/LANG.js" type="text/javascript"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/themes/fas/theme.js"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/themes/explorer-fas/theme.js"></script>
    <script>
        var language = $('.content').data('locale');
        $("#upload_partner").fileinput({
            theme: 'explorer-fas',
            uploadUrl: '#',
            ajaxDeleteSettings: { method: "PUT" },
            language: language,
            overwriteInitial: false,
            uploadAsync: false,
            fileActionSettings: {
                showZoom: false,
                showUpload: false,
                showRemove: true
            },
            initialPreviewAsData: true,
            initialPreviewFileType: 'image',
            showUpload: false,
            initialPreview: [
                @if (isset($detail) && $detail->icon != '')
                    window.location.origin + "/{{ $detail->icon }}", 
                @endif   
            ],
            initialPreviewConfig: [
                @if (isset($detail) && $detail->icon != '')
                    {type: 'image', caption: "{{ $detail->name }}", url: "/api/partner/icon/{{ $detail->id }}", key: "{{ $detail->id }}"}, 
                @endif       
            ],
        });

        $("#upload_partner").on("filepredelete", function(jqXHR) {
            var abort = true;
            if (confirm("Are you sure you want to delete this image?")) {
                abort = false;
            }
            return abort; // you can also send any data/object that you can receive on `filecustomerror` event
        });
    </script>
@endsection