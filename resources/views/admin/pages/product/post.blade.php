@extends('admin.layouts.main')

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('admin') }}/plugins/bootstrap-fileinput/css/fileinput.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin') }}/plugins/select2/select2.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin') }}/plugins/bootstrap-fileinput/themes/explorer-fas/theme.css">
@endsection

@section('content')
<section class="content" data-locale={{ Session::get('locale') }}>

    @if (isset($detail))
        <h1>@lang('admin.product') {{$detail->name}}</h1>
    @else
        <h1>@lang('admin.add') @lang('admin.product')</h1>
    @endif

        <div class="box-header">
            <a href="{{ route('admin.product.create') }}" class="btn btn-success">
                <i class="fa fa-plus"></i> @lang('admin.add')
            </a>
        </div>

    <div class="box box-info">
        <!-- /.box-header -->
        <div class="box-body pad">
            <form role="form" action="{{ isset($detail) ? route('admin.product.update', $detail->id) : route('admin.product.store') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('admin.name') (@lang('admin.vn'))</label>
                            <input type="text" name="name_vn" value="{{ isset($detail) ? $detail->name_vn : '' }}" class="form-control" required />
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('admin.name') (@lang('admin.en'))</label>
                            <input type="text" name="name_en" value="{{ isset($detail) ? $detail->name_en : '' }}" class="form-control" required />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="min_price_vn">@lang('admin.min_price') (@lang('admin.vn'))</label>
                            <input type="text" class="form-control" name="min_price_vn" id="currency-field"  value="{{ isset($detail) ? $detail->min_price_vn : '' }}" data-type="currency" required >
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="min_price_en">@lang('admin.min_price') (@lang('admin.en'))</label>
                            <input type="text" class="form-control" name="min_price_en" id="currency-field"  value="{{ isset($detail) ? $detail->min_price_en : '' }}" data-type="currency" required >
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="max_price_vn">@lang('admin.max_price') (@lang('admin.vn'))</label>
                            <input type="text" class="form-control" name="max_price_vn" id="currency-field"  value="{{ isset($detail) ? $detail->max_price_vn : '' }}" data-type="currency" required >
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="max_price_en">@lang('admin.max_price') (@lang('admin.en'))</label>
                            <input type="text" class="form-control" name="max_price_en" id="currency-field"  value="{{ isset($detail) ? $detail->max_price_en : '' }}" data-type="currency" required >
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="unit_vn">@lang('admin.unit') (@lang('admin.vn'))</label>
                            <input type="text" name="unit_vn" value="{{ isset($detail) ? $detail->unit_vn : '' }}" class="form-control"/>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="unit_en">@lang('admin.unit') (@lang('admin.en'))</label>
                            <input type="text" name="unit_en" value="{{ isset($detail) ? $detail->unit_en : '' }}" class="form-control"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('admin.tag') (@lang('admin.vn'))</label>
                            <select for="tag_vn" id="tag_vn" name="tag_vn[]" class="tags form-control" multiple="multiple">
                                @if (isset($detail))
                                    @foreach ($detail->tags as $tag)
                                        @if ($tag->language == 'vn')
                                            <option value="{{ $tag->name }}" selected>{{ $tag->name }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('admin.tag') (@lang('admin.en'))</label>
                            <select for="tag_en" id="tag_en" name="tag_en[]" class="tags form-control" multiple="multiple">
                                @if (isset($detail))
                                    @foreach ($detail->tags as $tag)
                                        @if ($tag->language == 'en')
                                            <option value="{{ $tag->name }}" selected>{{ $tag->name }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>@lang('admin.category')</label>
                    <select for="category" name="category[]" class="js-example-basic-multiple  form-control" multiple="multiple">
                        @foreach ($categories as $category)
                            @if (isset($product_category))
                                <option value="{{ $category->id }}" @if (in_array($category->id, $product_category)) selected @endif >
                                    {{ $category->name_vn }} - {{ $category->name_en }}
                                </option>
                            @else
                                <option value="{{ $category->id }}">
                                    {{ $category->name_vn }} - {{ $category->name_en }}
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>@lang('admin.sku')</label>
                    <input type="text" name="sku" value="{{ isset($detail) ? $detail->sku : '' }}" class="form-control" required="required" />
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('admin.sapo') (@lang('admin.vn'))</label>
                            <textarea name="sapo_vn" class="form-control" required="required" >
                                {{ isset($detail) ? $detail->sapo_vn : '' }}
                            </textarea>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('admin.sapo') (@lang('admin.en'))</label>
                            <textarea name="sapo_en" class="form-control" required="required" >
                                {{ isset($detail) ? $detail->sapo_en : '' }}
                            </textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('admin.title') (@lang('admin.vn'))</label>
                            <input type="text" name="title_vn" value="{{ isset($detail) ? $detail->title_vn : '' }}" class="form-control" required />
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('admin.title') (@lang('admin.en'))</label>
                            <input type="text" name="title_en" value="{{ isset($detail) ? $detail->title_en : '' }}" class="form-control" required />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('admin.meta') (@lang('admin.vn'))</label>
                            <input type="text" name="meta_vn" value="{{ isset($detail) ? $detail->meta_vn : '' }}" class="form-control" required />
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('admin.meta') (@lang('admin.en'))</label>
                            <input type="text" name="meta_en" value="{{ isset($detail) ? $detail->meta_en : '' }}" class="form-control" required />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('admin.description') (@lang('admin.vn'))</label>
                            <textarea id="content_vn" name="description_vn" rows="10" cols="80" class="form-control document-editor" required >
                                {{ isset($detail) ? $detail->description_vn : '' }}
                            </textarea>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>@lang('admin.description') (@lang('admin.en'))</label>
                            <textarea id="content_en" name="description_en" rows="10" cols="80" class="form-control document-editor" required >
                                {{ isset($detail) ? $detail->description_en : '' }}
                            </textarea>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="file-loading">
                        <input id="kv-explorer" name="images[]" type="file" multiple>
                    </div>
                </div>

                <button type="submit" class="btn btn-success" >Submit</button>
            </form>
        </div>
    </div>
</section>
@endsection

@section('script')
    <script src="{{ asset('admin') }}/plugins/ckeditor/ckeditor.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/js/fileinput.js"></script>
    <script src="{{ asset('admin') }}/plugins/select2/select2.js"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/js/plugins/piexif.js"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/js/plugins/sortable.js"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/js/locales/vi.js" type="text/javascript"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/js/locales/LANG.js" type="text/javascript"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/themes/fas/theme.js"></script>
    <script src="{{ asset('admin') }}/plugins/bootstrap-fileinput/themes/explorer-fas/theme.js"></script>
    <script>
        $(document).ready(function() {  
            $('.js-example-basic-multiple').select2();

            $('#tag_vn').select2({
                tags: true
            });

            $('#tag_en').select2({
                tags: true
            });
        });
        var language = $('.content').data('locale');
        $(function () {

            CKEDITOR.replace( 'content_vn', {
                language:'vi',
                filebrowserBrowseUrl: '{{ asset('admin') }}/plugins/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl: '{{ asset('admin') }}/plugins/ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl: '{{ asset('admin') }}/plugins/ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl: '{{ asset('admin') }}/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl: '{{ asset('admin') }}/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl: '{{ asset('admin') }}/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
            });

            CKEDITOR.replace( 'content_en', {
                language:'vi',
                filebrowserBrowseUrl: '{{ asset('admin') }}/plugins/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl: '{{ asset('admin') }}/plugins/ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl: '{{ asset('admin') }}/plugins/ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl: '{{ asset('admin') }}/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl: '{{ asset('admin') }}/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl: '{{ asset('admin') }}/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
            });

            $("#kv-explorer").fileinput({
                theme: 'explorer-fas',
                uploadUrl: '#',
                ajaxDeleteSettings: { method: "DELETE" },
                language: language,
                overwriteInitial: false,
                uploadAsync: false,
                fileActionSettings: {
                    showZoom: false,
                    showUpload: false,
                    showRemove: true
                },
                initialPreviewAsData: true,
                initialPreviewFileType: 'image',
                showUpload: false,
                initialPreview: [
                    @if (isset($images))
                            @foreach ($images as $image)
                        window.location.origin + "/{{ $image->path }}",
                    @endforeach
                    @endif
                ],
                initialPreviewConfig: [
                        @if (isset($images))
                        @foreach ($images as $image)
                    {type: 'image', caption: "{{ $image->name }}", url: "/api/image/{{ $image->id }}", key: "{{ $image->id }}"},
                    @endforeach
                    @endif
                ],
            });

            $("#kv-explorer").on("filepredelete", function(jqXHR) {
                var abort = true;
                if (confirm("Are you sure you want to delete this image?")) {
                    abort = false;
                }
                return abort; // you can also send any data/object that you can receive on `filecustomerror` event
            });
        });



    </script>
@endsection