@extends('admin.layouts.main')

@section('content')
<section class="content">
    <h1>@lang('admin.recruitment')</h1>
    <div class="box box-primary">
        <div class="box-header">
            <a href="{{ route('admin.recruitment.create') }}" class="btn btn-success">
                <i class="fa fa-plus"></i> @lang('admin.add')
            </a>
        </div>
        <div class="box-body">
            <table id="list_training" class="table table-bordered table-striped text-center">
                <thead>
                    <tr>
                        <th>@lang('admin.No')</th>
                        <th>@lang('admin.recruitment')</th>
                        <th>@lang('admin.active')</th>
                        <th>@lang('admin.action')</th>
                    </tr>
                </thead>

                <tbody>
                        @foreach($data as $index => $item)
                            <tr>
                                <td>{{ $index + 1 }}</td>
                                <td><a href="{{ route('admin.recruitment.edit', $item->id) }}">{{ $item->title_vn }}</a></td>
                                <td><a href="{{ route('admin.recruitment.edit', $item->id) }}">{{ $item->title_en }}</a></td>
                                <td>
                                    @if( $item->is_del == \App\Helpers\Business::IS_ACTIVE  )
                                        <span class="label label-success">@lang('admin.enable')</span>
                                    @else
                                        <span class="label label-danger">@lang('admin.disable')</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                </tbody>
            </table>
            <div class="pull-right">
                {{ $data->links() }}
            </div>
        </div>
    </div>
</section>

@endsection