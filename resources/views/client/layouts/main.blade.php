<!DOCTYPE html>
<html lang="en-US">
   <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
       <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="profile" href="https://gmpg.org/xfn/11">

        <title>Erado &#8211; Just another WordPress site</title>
        <style>
            .wishlist_table .add_to_cart, a.add_to_wishlist.button.alt { border-radius: 16px; -moz-border-radius: 16px; -webkit-border-radius: 16px; }
        </style>
        <link rel='stylesheet' href='{{ asset('client') }}/plugins/form/includes/css/styles.css?ver=5.0.3' type='text/css' media='all' />
        <link rel='stylesheet' href='{{ asset('client') }}/plugins/jms-imageswatch/assets/css/style.css?ver=4.9.8' type='text/css' media='all' />
        <link rel='stylesheet' href='{{ asset('client') }}/plugins/revslider/public/assets/css/settings.css?ver=5.4.7.4' type='text/css' media='all' />
        <link rel='stylesheet' href='{{ asset('client') }}/plugins/compare/assets/css/colorbox.css?ver=4.9.8' type='text/css' media='all' />
        <link rel='stylesheet' href='{{ asset('client') }}/plugins/popup/assets/fonts/retinaicon-font/style.css?ver=4.9.8' type='text/css' media='all' />
        <link rel='stylesheet' href='{{ asset('client') }}/plugins/popup/assets/css/font-awesome.min.css?ver=4.9.8' type='text/css' media='all' />
        <style id='font-awesome-inline-css' type='text/css'>
            [data-font="FontAwesome"]:before {font-family: 'FontAwesome' !important;content: attr(data-icon) !important;speak: none !important;font-weight: normal !important;font-variant: normal !important;text-transform: none !important;line-height: 1 !important;font-style: normal !important;-webkit-font-smoothing: antialiased !important;-moz-osx-font-smoothing: grayscale !important;}
        </style>
        <link rel='stylesheet' href='{{ asset('client') }}/plugins/popup/assets/css/frontend.css?ver=4.9.8' type='text/css' media='all' />
        <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Open+Sans:400,700' type='text/css' media='all' />
        <link rel='stylesheet' href='{{ asset('client') }}/plugins/product-countdown/assets/css/ywpc-style-1.min.css?ver=4.9.8' type='text/css' media='all' />
        <link rel='stylesheet'href='{{ asset('client') }}/plugins/ecommerce/assets/css/prettyPhoto.css?ver=3.4.4' type='text/css' media='all' />
        <link rel='stylesheet' href='{{ asset('client') }}/plugins/wishlist/assets/css/jquery.selectBox.css?ver=1.2.0' type='text/css' media='all' />
        <link rel='stylesheet' href='{{ asset('client') }}/plugins/wishlist/assets/css/style.css?ver=2.2.3' type='text/css' media='all' />
        <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Poppins%3A200%2C300%2C400%2C500%2C600%2C700%2C800&#038;subset=cyrillic%2Ccyrillic-ext%2Cgreek%2Cgreek-ext%2Clatin%2Clatin-ext' type='text/css' media='all' />
        <link rel='stylesheet' href='{{ asset('client') }}/assets/3rd-party/bootstrap/css/bootstrap.min.css?ver=3.3.7' type='text/css' media='all' />
        <link rel='stylesheet' href='{{ asset('client') }}/assets/3rd-party/pe-icon-7-stroke/css/pe-icon-7-stroke.css?ver=4.9.8' type='text/css' media='all' />
        <link rel='stylesheet' href='{{ asset('client') }}/assets/3rd-party/font-awesome//css/font-awesome.min.css?ver=4.9.8' type='text/css' media='all' />
        <link rel='stylesheet' href='{{ asset('client') }}/assets/3rd-party/owl-carousel/owl.carousel.min.css?ver=4.9.8' type='text/css' media='all' />
        <link rel='stylesheet' href='{{ asset('client') }}/assets/3rd-party/owl-carousel/owl.theme.default.min.css?ver=4.9.8' type='text/css' media='all' />
        <link rel='stylesheet'href='{{ asset('client') }}/assets/3rd-party/slick/slick.css?ver=4.9.8' type='text/css' media='all' />
        <link rel='stylesheet' href='{{ asset('client') }}/assets/3rd-party/magnific-popup/magnific-popup.css?ver=4.9.8' type='text/css' media='all' />
        <link rel='stylesheet' href='{{ asset('client') }}/assets/3rd-party/magnific-popup/magnific-popup-effect.css?ver=4.9.8' type='text/css' media='all' />

        <link rel='stylesheet'href='{{ asset('client') }}/css/style.css?ver=4.9.8' type='text/css' media='all' />

        <link rel='stylesheet'  href='//fonts.googleapis.com/css?family=Playfair+Display%3Aitalic&#038;subset=latin&#038;ver=2.7.6' type='text/css' media='all' />
        <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Poppins%3A300%2Cregular%2C500%2C600%2C700%2C800&#038;subset=latin&#038;ver=2.7.6' type='text/css' media='all' />
        <link rel='stylesheet'  href='//fonts.googleapis.com/css?family=Lato%3A100%2C100italic%2C300%2C300italic%2Cregular%2Citalic%2C700%2C700italic%2C900%2C900italic&#038;subset=latin-ext%2Clatin&#038;ver=2.7.6' type='text/css' media='all' />
        <link rel='stylesheet' href='{{ asset('client') }}/plugins/kingcomposer/assets/frontend/css/kingcomposer.min.css?ver=2.7.6' type='text/css' media='all' />
        <link rel='stylesheet' href='{{ asset('client') }}/plugins/kingcomposer/assets/css/animate.css?ver=2.7.6' type='text/css' media='all' />
        <link rel='stylesheet' href='{{ asset('client') }}/assets/3rd-party/pe-icon-7-stroke/css/pe-icon-7-stroke.css?ver=2.7.6' type='text/css' media='all' />
        <link rel='stylesheet'href='{{ asset('client') }}/plugins/kingcomposer/assets/css/icons.css?ver=2.7.6' type='text/css' media='all' />
        <link rel='stylesheet'href='{{ asset('client') }}/css/custom.css' type='text/css' media='all' />
        @yield('style')
         {{--<script type='text/javascript' src='{{ asset('client') }}/includes/js/jquery/jquery.js?ver=1.12.4'></script>--}}
        <!-- jQuery 2.2.3 -->
        <script src="{{ asset('admin') }}/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <script type='text/javascript' src='{{ asset('client') }}/includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
        <script type='text/javascript' src='{{ asset('client') }}/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.4.7.4'></script>
        <script type='text/javascript' src='{{ asset('client') }}/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.7.4'></script>
        <script type='text/javascript' src='{{ asset('client') }}/plugins/popup/assets/js/jquery.cookie.min.js?ver=1.2.4'></script>
        <script type='text/javascript' src='{{ asset('client') }}/plugins/popup/assets/js/jquery.yitpopup.min.js?ver=1.2.4'></script>
        <script type='text/javascript' src='{{ asset('client') }}/plugins/product-countdown/assets/js/jquery.plugin.min.js?ver=4.9.8'></script>
        <script type='text/javascript' src='{{ asset('client') }}/plugins/product-countdown/assets/js/jquery.countdown.min.js?ver=2.0.2'></script>
        <script type='text/javascript' src='{{ asset('client') }}/plugins/erado-addons//inc/megamenu/js/megamenu.js?ver=4.9.8'></script>


        <script type="text/javascript" >
            var imageswatch_ajax_url = "https://wp.jmstheme.com/erado/wp-admin/admin-ajax.php";
        </script><script type="text/javascript">var kc_script_data={ajax_url:"https://wp.jmstheme.com/erado/wp-admin/admin-ajax.php"}</script>
        <noscript>
            <style>.woocommerce-product-gallery{ opacity: 1 !important; }</style>
        </noscript>
        <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
        <meta name="generator" content="Powered by Slider Revolution 5.4.7.4 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
        <script type="text/javascript">function setREVStartSize(e){
            try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
                if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
            }catch(d){console.log("Failure at Presize of Slider:"+d)}
            };
        </script>
   </head>
    <body data-rsssl=1 class="home page-template-default page page-id-149 kingcomposer kc-css-system woocommerce-no-js yith-wcan-free btn-sidebar">
        <div class="preloader">
            <div class="spinner5">
                <div class="dot1"></div>
                <div class="dot2"></div>
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>
        <div class="toggle-sidebar-widget toggleSidebar">
            <div class="closetoggleSidebar"></div>
            <div class="widget-area">
                <aside id="text-3" class="widget widget_text">
                <div class="widget-title">
                    <h3>Welcome</h3>
                </div>
                <div class="textwidget">
                    <p>Alienum phaedrum torquatos nec eu, vis detraxit ertssa periculiser ex, nihil expetendis in meinerst gers.</p>
                </div>
                </aside>
                <aside id="jms-instagram-3" class="widget jms-instagram">
                <div class="instagram clearfix cols-3">
                    <div class="item"><a target="_blank" href="https://www.instagram.com/p/Bbyt_nRgfz-/"><img width="320" height="320" class="img-responsive" src="" alt="Instagram" /></a></div>
                    <div class="item"><a target="_blank" href="https://www.instagram.com/p/Bbyt-ylAiyj/"><img width="320" height="320" class="img-responsive" src="" alt="Instagram" /></a></div>
                    <div class="item"><a target="_blank" href="https://www.instagram.com/p/Bbyt98JAf2l/"><img width="320" height="320" class="img-responsive" src="" alt="Instagram" /></a></div>
                    <div class="item"><a target="_blank" href="https://www.instagram.com/p/Bbyt8rdgVDJ/"><img width="320" height="320" class="img-responsive" src="" alt="Instagram" /></a></div>
                    <div class="item"><a target="_blank" href="https://www.instagram.com/p/Bbyt7mBApJV/"><img width="320" height="320" class="img-responsive" src="" alt="Instagram" /></a></div>
                    <div class="item"><a target="_blank" href="https://www.instagram.com/p/Bbyt6jsA4g7/"><img width="320" height="320" class="img-responsive" src="" alt="Instagram" /></a></div>
                </div>
                </aside>
                <aside id="social-network-3" class="widget widget_social_network">
                <div class="widget-title">
                    <h3>Follow us on socials network</h3>
                </div>
                <ul class="social-network">
                    <li><a href="#" class="facebook"><span class="fa fa-facebook"></span></a></li>
                    <li><a href="#" class="twitter"><span class="fa fa-twitter"></span></a></li>
                    <li><a href="#" class="gplus"><span class="fa fa-google-plus"></span></a></li>
                    <li><a href="#" class="instagram"><span class="fa fa-instagram"></span></a></li>
                    <li><a href="#" class="pinterest"><span class="fa fa-pinterest"></span></a></li>
                </ul>
                </aside>
            </div>
        </div>
        <div class="toggleSidebar cartSidebarWrap">
            <div class="cart-sidebar-header-bottom">
                <span>Shopping bag<span class="cart-count">0</span></span>
            </div>
            <div class="cart-sidebar-header flex between-xs">
                <div class="cart-sidebar-title">
                Shopping Bag
                <div class="cartContentsCount">
                    0
                </div>
                </div>
                <span class="pe-7s-close close_cart_sidebar"></span>
            </div>
            <div class="widget_shopping_cart_content"></div>
        </div>
        <div class="fl-mobile-nav">
            <div class="menu-title flex between-xs">MENU<i class="close-menu"></i></div>
            <div class="mobile-menu-wrapper">
                <ul class="mobile-menu">
                <li  class="menu_erado menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item current_page_item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-align-left menu-default menu-item-lv0">
                    <a  href="index.php" class="menu-item-link" ><span class="menu_title">Trang chủ</span></a>
                </li>
                <li  class="mustsee menu_erado menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-align-justify mega menu-item-lv0">
                    <a  href="shop.php" class="menu-item-link" ><span class="menu_title">Sản phẩm</span></a>
                    <div   class="dropdown-menu mega-dropdown-menu " style="width:900px;">
                        <div class="mega-dropdown-inner">
                            <div class="row">
                            <ul class="mega-nav col-sm-3">
                                <li  class="menu-item-153">
                                    <a  href="#" class="menu-item-link has-children column-heading" ><span class="menu_title">Shop Layout</span><i class="fa fa-angle-down mm-has-children"></i></a>
                                    <ul class="sub-menu">
                                        <li  class="menu-item-154"><a  href="shop.php?p=?grid=0" class="menu-item-link" ><span class="menu_title">Shop - List</span></a></li>
                                        <li  class="menu-item-155"><a  href="https://wp.jmstheme.com/erado/shop-four-columns-grid/" class="menu-item-link" ><span class="menu_title">Shop - Grid</span></a></li>

                                    </ul>
                                </li>
                            </ul>
                            <ul class="mega-nav col-sm-3">
                                <li  class="menu-item-161">
                                    <a  href="#" class="menu-item-link has-children column-heading" ><span class="menu_title">Shop Style</span><i class="fa fa-angle-down mm-has-children"></i></a>
                                    <ul class="sub-menu">
                                        <li  class="menu-item-162"><a  href="https://wp.jmstheme.com/erado/shop.php?sidebar=left" class="menu-item-link" ><span class="menu_title">Left Sidebar</span></a></li>
                                        <li  class="menu-item-163"><a  href="https://wp.jmstheme.com/erado/shop.php?sidebar=right" class="menu-item-link" ><span class="menu_title">Right Sidebar</span></a></li>

                                    </ul>
                                </li>
                            </ul>
                            <ul class="mega-nav col-sm-3">
                                <li  class="menu-item-180">
                                    <a  href="#" class="menu-item-link has-children column-heading" ><span class="menu_title">Header</span><i class="fa fa-angle-down mm-has-children"></i></a>
                                    <ul class="sub-menu">
                                        <li  class="menu-item-186"><a  href="shop.php?p=?sticky-header=1" class="menu-item-link" ><span class="menu_title">Sticky Header</span></a></li>
                                        <li  class="menu-item-187"><a  href="shop.php?p=?header=2&#038;header_container=0" class="menu-item-link" ><span class="menu_title">Fullwidth Header</span></a></li>

                                    </ul>
                                </li>
                            </ul>
                            <ul class="mega-nav col-sm-3">
                                <li  class="menu-item-199">
                                    <a  href="#" class="menu-item-link has-children column-heading" ><span class="menu_title">Footer Custom</span><i class="fa fa-angle-down mm-has-children"></i></a>
                                    <ul class="sub-menu">
                                        <li  class="menu-item-206"><a  href="shop.php?p=?fullwidth=1&#038;footer_container=1" class="menu-item-link" ><span class="menu_title">Fullwidth Footer</span></a></li>
                                        <li  class="menu-item-207"><a  href="shop.php?p=?fullwidth=1&#038;footer_container=0" class="menu-item-link" ><span class="menu_title">Boxed Footer</span></a></li>

                                    </ul>
                                </li>
                            </ul>
                            </div>
                            <div class="row">
                            <ul class="mega-nav col-sm-3">
                                <li  class="menu-item-169">
                                    <a  href="https://wp.jmstheme.com/erado/shop" class="menu-item-link has-children column-heading" ><span class="menu_title">Shop Column</span><i class="fa fa-angle-down mm-has-children"></i></a>
                                    <ul class="sub-menu">
                                        <li  class="menu-item-170"><a  href="shop.php?p=?column=2&#038;sidebar=right" class="menu-item-link" ><span class="menu_title">Two Columns</span></a></li>
                                        <li  class="menu-item-171"><a  href="shop.php?p=?column=3" class="menu-item-link" ><span class="menu_title">Three Columns</span></a></li>

                                    </ul>
                                </li>
                            </ul>
                            <ul class="mega-nav col-sm-3">
                                <li  class="menu-item-2010">
                                    <a  href="https://wp.jmstheme.com/erado?style=1" class="menu-item-link has-children column-heading" ><span class="menu_title">Product hover</span><i class="fa fa-angle-down mm-has-children"></i></a>
                                    <ul class="sub-menu">
                                        <li  class="menu-item-2006"><a  href="https://wp.jmstheme.com/erado?style=1" class="menu-item-link" ><span class="menu_title">Style 2</span></a></li>
                                        <li  class="menu-item-2007"><a  href="https://wp.jmstheme.com/erado?style=3" class="menu-item-link" ><span class="menu_title">Style 3</span></a></li>

                                    </ul>
                                </li>
                            </ul>
                            </div>
                        </div>
                    </div>
                </li>
                <li  class="menu_erado menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-align-left mega menu-item-lv0">
                    <a  href="recruit.php" class="menu-item-link" ><span class="menu_title">Tuyển dụng</span></a>
                    <div   class="dropdown-menu mega-dropdown-menu " style="width:600px;">
                        <div class="mega-dropdown-inner">
                            <ul class="mega-nav col-sm-6">
                            <li  class="menu-item-209">
                                <a  href="#" class="menu-item-link has-children column-heading" ><span class="menu_title">Product Types</span><i class="fa fa-angle-down mm-has-children"></i></a>
                                <ul class="sub-menu">
                                    <li  class="menu-item-1983"><a  href="single-product.php?p=posuare-condimentum/" class="menu-item-link" ><span class="menu_title">Standard Product</span></a></li>
                                    <li  class="menu-item-1855"><a  href="single-product.php?p=mauris-vestibulum/" class="menu-item-link" ><span class="menu_title">Variable Product</span></a></li>

                                </ul>
                            </li>
                            </ul>
                            <ul class="mega-nav col-sm-6">
                            <li  class="menu-item-220">
                                <a  href="#" class="menu-item-link has-children column-heading" ><span class="menu_title">Product Layout</span><i class="fa fa-angle-down mm-has-children"></i></a>
                                <ul class="sub-menu">
                                    <li  class="menu-item-1984"><a  href="single-product.php?p=posuare-condimentum/" class="menu-item-link" ><span class="menu_title">Default Layout</span></a></li>
                                    <li  class="menu-item-1846"><a  href="single-product.php?p=condimentum-libero-dolor/" class="menu-item-link" ><span class="menu_title">Gallery Layout</span></a></li>

                                </ul>
                            </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li  class="menu_erado menu_page menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-align-justify mega menu-item-lv0">
                    <a  href="https://wp.jmstheme.com/erado/feature-box/" class="menu-item-link" ><span class="menu_title">Pages</span></a>
                    <div   class="dropdown-menu mega-dropdown-menu " style="width:900px;">
                        <div class="mega-dropdown-inner">
                            <ul class="mega-nav col-sm-3">
                            <li  class="menu-item-253 menu-align-left">
                                <a  href="#" class="menu-item-link has-children column-heading" ><span class="menu_title">Page</span><i class="fa fa-angle-down mm-has-children"></i></a>
                                <ul class="sub-menu">
                                    <li  class="menu-item-256"><a  href="https://wp.jmstheme.com/erado/about-us/" class="menu-item-link" ><span class="menu_title">About us</span></a></li>
                                    <li  class="menu-item-257"><a  href="https://wp.jmstheme.com/erado/contact/" class="menu-item-link" ><span class="menu_title">Contact us</span></a></li>

                                </ul>
                            </li>
                            </ul>
                            <ul class="mega-nav col-sm-3">
                            <li  class="menu-item-239">
                                <a  href="#" class="menu-item-link has-children column-heading" ><span class="menu_title">Featured Element</span><i class="fa fa-angle-down mm-has-children"></i></a>
                                <ul class="sub-menu">
                                    <li  class="menu-item-240"><a  href="https://wp.jmstheme.com/erado/pricing/" class="menu-item-link" ><span class="menu_title">Pricing</span></a></li>
                                    <li  class="menu-item-259"><a  href="https://wp.jmstheme.com/erado/support-center/" class="menu-item-link" ><span class="menu_title">Support Center</span></a></li>

                                </ul>
                            </li>
                            </ul>
                            <ul class="mega-nav col-sm-3">
                            <li  class="menu-item-232">
                                <a  href="#" class="menu-item-link has-children column-heading" ><span class="menu_title">Featured Element</span><i class="fa fa-angle-down mm-has-children"></i></a>
                                <ul class="sub-menu">
                                    <li  class="menu-item-234"><a  href="https://wp.jmstheme.com/erado/divider/" class="menu-item-link" ><span class="menu_title">Divider</span></a></li>
                                    <li  class="menu-item-235"><a  href="https://wp.jmstheme.com/erado/feature-box/" class="menu-item-link" ><span class="menu_title">Feature Box</span></a></li>

                                </ul>
                            </li>
                            </ul>
                            <ul class="mega-nav col-sm-3">
                            <li  class="menu-item-246">
                                <a  href="#" class="menu-item-link has-children column-heading" ><span class="menu_title">Featured Element</span><i class="fa fa-angle-down mm-has-children"></i></a>
                                <ul class="sub-menu">
                                    <li  class="menu-item-247"><a  href="https://wp.jmstheme.com/erado/button/" class="menu-item-link" ><span class="menu_title">Button</span></a></li>
                                    <li  class="menu-item-249"><a  href="https://wp.jmstheme.com/erado/hero-image/" class="menu-item-link" ><span class="menu_title">Hero Image</span></a></li>

                                </ul>
                            </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li  class="menu_erado menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-align-left menu-default menu-item-lv0">
                    <a  href="blog.php" class="menu-item-link" ><span class="menu_title">Tin Tức</span></a>
                    <ul class="sub-menu" >
                        <li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-lv1"><a  href="https://wp.jmstheme.com/erado/portfolio-2-columns/" class="menu-item-link" ><span class="menu_title">Portfolio 2 Columns</span></a></li>
                        <li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-lv1"><a  href="https://wp.jmstheme.com/erado/portfolio-3-columns/" class="menu-item-link" ><span class="menu_title">Portfolio 3 Columns</span></a></li>

                    </ul>
                </li>
                <li  class="menu_erado menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-align-left menu-default menu-item-lv0">
                    <a  href="about-us.php" class="menu-item-link" ><span class="menu_title">Về chúng tôi</span></a>
                    <ul class="sub-menu" >
                        <li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-lv1">
                            <a  href="blog.php?" class="menu-item-link" ><span class="menu_title">List Layout</span></a>
                            <ul class="sub-menu">
                            <li  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-lv2"><a  href="blog.php??sidebar=no" class="menu-item-link" ><span class="menu_title">No Sidebar</span></a></li>
                            <li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-lv2"><a  href="blog.php?" class="menu-item-link" ><span class="menu_title">Left Sidebar</span></a></li>

                            </ul>
                        </li>
                        <li  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-lv1">
                            <a  href="blog.php??style=grid&#038;sidebar=no" class="menu-item-link" ><span class="menu_title">Grid Layout</span></a>
                            <ul class="sub-menu">
                            <li  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-lv2"><a  href="blog.php??style=grid&#038;sidebar=no" class="menu-item-link" ><span class="menu_title">No Sidebar</span></a></li>
                            <li  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-lv2"><a  href="blog.php??style=grid" class="menu-item-link" ><span class="menu_title">Left Sidebar</span></a></li>

                            </ul>
                        </li>

                        <li  class="menu-item menu-item-type-post_type menu-item-object-post menu-item-lv1"><a  href="single-blog.php?p=post-format-gallery/" class="menu-item-link" ><span class="menu_title">Blog Single Post</span></a></li>
                    </ul>
                </li>
                </ul>
            </div>
        </div>
        <div id="page" class="site oh">
        @include('client.partials.header')
        @yield('content')
        @include('client.partials.footer')
        @yield('script')
    </body>
</html>