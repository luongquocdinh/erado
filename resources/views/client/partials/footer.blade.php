<div class="clearfix"></div>
    <footer id="footer-wrapper" class="footer-1">
    <!-- <div class="newletter-footer-top">
        <div class="container">
            <aside id="mc4wp_form_widget-3" class="widget widget_mc4wp_form_widget">

                <form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-478" method="post" data-id="478" data-name="new letter" >
                <div class="mc4wp-form-fields">
                    <div class="newletter-text">
                        <div class="title-newletter">SIGN UP NEWSLETTER</div>
                        <div class="des-newletter">Get news and receive 20% off for your next buy!</div>
                    </div>
                    <div class="form-newletter">
                        <p>
                            <input type="email" name="EMAIL" placeholder="Your email address" required />
                        </p>
                        <p>
                            <input type="submit" value="SUBSCRIBE" />
                        </p>
                    </div>
                </div>
                <label style="display: none !important;">Leave this field empty if you're human: <input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off" /></label><input type="hidden" name="_mc4wp_timestamp" value="1538030803" /><input type="hidden" name="_mc4wp_form_id" value="478" /><input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1" />
                <div class="mc4wp-response"></div>
                </form>
                MailChimp for WordPress Plugin
            </aside>
        </div>
    </div> -->
    <div class="footer-top pt_70 pb_25">
        <div class="footer-container container">
            <div class="footer-row row">
                <div class="footer-position col-lg-4 col-md-4 col-sm-6 col-xs-12 mb_40">
                <aside id="text-2" class="widget widget_text">
                    <div class="textwidget">
                        <div class="info-contact">
                            <p><a class="logo-footer" href="https://wp.jmstheme.com/erado" rel="home"><img src="{{ asset('client') }}/uploads/2018/03/logo.png" alt="Erado" /></a></p>
                            <div class="des-info">Lorem ipsum dolor amet conseter adipis pulvinar fermentum. Praliquet molest justo eleifend haretra lacus pretiur lacinia nulla.</div>
                            <div class="address-info">Add: No. 1203 Jonh Rhjn Suite 120, NY 77101.</div>
                            <div class="phone-info">Call: <a href="tel:+01122687749">+01 1 2268 7749</a></div>
                            <div class="email-info">Email: <a href="mailto:eradocare@demo.com">eradocare@demo.com</a></div>
                        </div>
                    </div>
                </aside>
                </div>
                <div class="footer-position col-lg-2 col-md-2 col-sm-6 col-xs-12 mb_40">
                <aside id="nav_menu-2" class="widget widget_nav_menu">
                    <div class="widget-title">
                        <h3>Infomation</h3>
                    </div>
                    <div class="menu-information-container">
                        <ul class="menu">
                            <li  class="menu-item menu-item-type-post_type menu-item-object-page menu-align-left menu-default menu-item-lv0"><a  href="http://bi-acaovo.net/erado/about-us.php" class="menu-item-link" ><span class="menu_title">Về Chúng Tôi</span></a></li>
                            <li  class="menu-item menu-item-type-post_type menu-item-object-page menu-align-left menu-default menu-item-lv0"><a  href="http://bi-acaovo.net/erado/shop.php" class="menu-item-link" ><span class="menu_title">Sản Phẩm</span></a></li>
                            <li  class="menu-item menu-item-type-post_type menu-item-object-page menu-align-left menu-default menu-item-lv0"><a  href="http://bi-acaovo.net/erado/recruit.php" class="menu-item-link" ><span class="menu_title">Tuyển Dụng</span></a></li>
                            <li  class="menu-item menu-item-type-custom menu-item-object-custom menu-align-left menu-default menu-item-lv0"><a  href="http://bi-acaovo.net/erado/blog.php" class="menu-item-link" ><span class="menu_title">Tin Tức</span></a></li>
                        </ul>
                    </div>
                </aside>
                </div>
                <div class="footer-position col-lg-3 col-md-3 col-sm-6 col-xs-12 mb_40">
                <aside id="nav_menu-4" class="widget widget_nav_menu">
                    <div class="widget-title">
                        <h3>Need help</h3>
                    </div>
                    <div class="menu-need-help-container">
                        <ul class="menu">
                        <li  class="menu-item menu-item-type-post_type menu-item-object-page menu-align-left menu-default menu-item-lv0"><a  href="http://bi-acaovo.net/erado/about-us.php" class="menu-item-link" ><span class="menu_title">Về Chúng Tôi</span></a></li>
                        <li  class="menu-item menu-item-type-post_type menu-item-object-page menu-align-left menu-default menu-item-lv0"><a  href="http://bi-acaovo.net/erado/shop.php" class="menu-item-link" ><span class="menu_title">Sản Phẩm</span></a></li>
                        <li  class="menu-item menu-item-type-post_type menu-item-object-page menu-align-left menu-default menu-item-lv0"><a  href="http://bi-acaovo.net/erado/recruit.php" class="menu-item-link" ><span class="menu_title">Tuyển Dụng</span></a></li>
                        <li  class="menu-item menu-item-type-custom menu-item-object-custom menu-align-left menu-default menu-item-lv0"><a  href="http://bi-acaovo.net/erado/blog.php" class="menu-item-link" ><span class="menu_title">Tin Tức</span></a></li>
                        </ul>
                    </div>
                </aside>
                </div>
                <div class="footer-position col-lg-3 col-md-3 col-sm-6 col-xs-12 mb_40">
                <aside id="jms-instagram-2" class="widget jms-instagram">
                    <div class="widget-title">
                        <h3>Instagram</h3>
                    </div>
                    <div class="instagram clearfix cols-3">
                        <div class="item"><a target="_blank" href="https://www.instagram.com/p/Bbyt_nRgfz-/"><img width="320" height="320" class="img-responsive" src="" alt="Instagram" /></a></div>
                        <div class="item"><a target="_blank" href="https://www.instagram.com/p/Bbyt_nRgfz-/"><img width="320" height="320" class="img-responsive" src="" alt="Instagram" /></a></div>
                        <div class="item"><a target="_blank" href="https://www.instagram.com/p/Bbyt_nRgfz-/"><img width="320" height="320" class="img-responsive" src="" alt="Instagram" /></a></div>
                        <div class="item"><a target="_blank" href="https://www.instagram.com/p/Bbyt_nRgfz-/"><img width="320" height="320" class="img-responsive" src="" alt="Instagram" /></a></div>
                        <div class="item"><a target="_blank" href="https://www.instagram.com/p/Bbyt_nRgfz-/"><img width="320" height="320" class="img-responsive" src="" alt="Instagram" /></a></div>
                        <div class="item"><a target="_blank" href="https://www.instagram.com/p/Bbyt_nRgfz-/"><img width="320" height="320" class="img-responsive" src="" alt="Instagram" /></a></div>
                    </div>
                </aside>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer Top -->
    <div class="footer-container container">
        <div class="footer-bottom pt_20 pb_20">
            <div class="footer-row row">
                <div class="footer-position col-lg-6 col-md-6 col-sm-12 col-xs-12 copyright tl">
                <div class="footer-block">
                    Copyright 2018. All rights reserved. Design by <a href="#">Joommasters.com</a>.
                </div>
                </div>
                <div class="footer-position col-lg-6 col-md-6 col-sm-12 col-xs-12 tr">
                <div class="footer-block widget-bottom">
                    <aside id="social-network-2" class="widget widget_social_network">
                        <ul class="social-network">
                            <li><a href="#" class="facebook"><span class="fa fa-facebook"></span></a></li>
                            <li><a href="#" class="twitter"><span class="fa fa-twitter"></span></a></li>
                            <li><a href="#" class="gplus"><span class="fa fa-google-plus"></span></a></li>
                            <li><a href="#" class="instagram"><span class="fa fa-instagram"></span></a></li>
                            <li><a href="#" class="pinterest"><span class="fa fa-pinterest"></span></a></li>
                        </ul>
                    </aside>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer Bottom -->
    </footer>
</div>
<!-- #page -->
<button id="backtop"><i class="pe-7s-angle-up"></i></button>

<script type="text/javascript">
    function revslider_showDoubleJqueryError(sliderID) {
    var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
    errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
    errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
    errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
    errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
        jQuery(sliderID).show().html(errorMessage);
    }
</script>

<script type='text/javascript' src='{{ asset('client') }}/plugins/ecommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>
<script type='text/javascript' src='{{ asset('client') }}/plugins/ecommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4'></script>


<script type='text/javascript' src='{{ asset('client') }}/plugins/compare/assets/js/jquery.colorbox-min.js?ver=1.4.21'></script>

<script type='text/javascript' src='{{ asset('client') }}/plugins/ecommerce/assets/js/prettyPhoto/jquery.prettyPhoto.min.js?ver=3.1.6'></script>
<script type='text/javascript' src='{{ asset('client') }}/plugins/wishlist/assets/js/jquery.selectBox.min.js?ver=1.2.0'></script>

<script type='text/javascript' src='{{ asset('client') }}/assets/3rd-party/bootstrap/js/bootstrap.min.js?ver=3.3.7'></script>
<script type='text/javascript' src='{{ asset('client') }}/assets/3rd-party/isotope/isotope.pkgd.min.js?ver=4.9.8'></script>
<script type='text/javascript' src='{{ asset('client') }}/assets/3rd-party/owl-carousel/owl.carousel.min.js?ver=2.2.0'></script>
<script type='text/javascript' src='{{ asset('client') }}/assets/3rd-party/slick/slick.min.js?ver=1.5.9'></script>
<script type='text/javascript' src='{{ asset('client') }}/assets/3rd-party/theia-sticky-sidebar/theia-sticky-sidebar.js?ver=4.9.8'></script>
<script type='text/javascript' src='{{ asset('client') }}/assets/3rd-party/scrollreveal/scrollreveal.min.js?ver=3.3.6'></script>
<script type='text/javascript' src='{{ asset('client') }}/assets/3rd-party/magnific-popup/jquery.magnific-popup.min.js?ver=4.9.8'></script>

{{--<script type='text/javascript' src='{{ asset('client') }}/assets/js/shop-filter.js?ver=4.9.8'></script>--}}
<script type='text/javascript' src='{{ asset('client') }}/includes/js/imagesloaded.min.js?ver=3.2.0'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var FL_Data_Js = {"View Wishlist":"View Wishlist","viewall_wishlist":"View all","removed_notice":"%s has been removed from your cart.","load_more":"Load more","no_more_item":"All products loaded.","days":"days","hrs":"hrs","mins":"mins","secs":"secs","permalink":""};
    /* ]]> */
</script>
<script type='text/javascript' src='{{ asset('client') }}/assets/js/theme.js?ver=4.9.8'></script>
<script type='text/javascript'>
    var _nonce_erado = '0afd308256';
    var JmsAjaxURL = "https://wp.jmstheme.com/erado/wp-admin/admin-ajax.php"; var JmsSiteURL = "https://wp.jmstheme.com/erado/index.php";
</script>
<script type='text/javascript' src='{{ asset('client') }}/plugins/kingcomposer/assets/frontend/js/kingcomposer.min.js?ver=2.7.6'></script>

<script type="text/javascript">
    jQuery(document).ready(function($) {
    var rtl = false;
        if ($('body').hasClass('rtl')) rtl = true;

        $('.blog-carousel-1199695738').owlCarousel({
            responsive : {
                320 : {
                items: 1,
                    margin: 30
            },
            480 : {
                items: 1,
                    margin: 30
            },
            768 : {
                items: 2,
                    margin: 30
            },
            991 : {
                items: 3,
                    margin: 30
            },
            1199 : {
                items: 3,
            }
        },
        rtl: rtl,
            margin: 20,
            dots: false,
            nav: true,
            autoplay: false,
            loop: false,
            autoplayTimeout: 5000,
            smartSpeed: 1000,
        navText: ['<i class="icon-arrow prev"></i>','<i class="icon-arrow next"></i>']
        });
    });
</script>