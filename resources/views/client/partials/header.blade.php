<header id="header-wrapper" class="header-1">
    <div class="main-header pt_35 pb_35">
        <div class="container">
            <div class="header-row flex middle-xs">
                <div class="header-position hidden-lg hidden-md menu-toggle pt_7">
                <div class="header-block">
                    <div class="menu-button">
                        <i class="icon-menu"></i>
                    </div>
                </div>
                </div>
                <div class="header-position header-logo">
                <div class="header-block">
                    <a href="index.php" rel="home">
                    <img src="{{ asset('client') }}/uploads/2018/08/Logo3.png" alt="Erado">
                    </a>
                </div>
                </div>
                <!-- header-logo -->
                <div class="header-position header-position-center hidden-sm hidden-xs main-navigation tc">
                <div class="header-block">
                    <div class="primary-menu-wrapper">
                        <ul class="primary-menu">
                            <li  class="menu_erado">
                                <a  href="{{route('client.home')}}" class="menu-item-link" ><span class="menu_title">TRANG CHỦ</span></a>
                            </li>
                            <li  class="menu_erado ">
                                <a  href="{{route('client.product')}}" class="menu-item-link" ><span class="menu_title">@lang('client.product')</span></a>    
                            </li>
                            <li  class="menu_erado  menu-item-has-children menu-align-left mega menu-item-lv0">
                                <a  href="{{route('client.recruitment', ['lang' => \App\Helpers\Business::LANG_DEFAULT])}}" class="menu-item-link" ><span class="menu_title">@lang('client.recruitment')</span></a>
                            </li>
                            <li  class="menu_erado menu_page   menu-item-has-children menu-align-justify mega menu-item-lv0">
                                <a  href="https://wp.jmstheme.com/erado/feature-box/" class="menu-item-link" ><span class="menu_title">Pages</span></a>
                            </li>
                            <li  class="menu_erado  menu-item-has-children menu-align-left ">
                                <a  href="{{route('client.news', ['lang' => \App\Helpers\Business::LANG_DEFAULT])}}" class="menu-item-link" ><span class="menu_title">@lang('client.news')</span></a>
                            </li>
                            <li  class="menu_erado   menu-item-has-children menu-align-left ">
                                <a  href="{{route('client.about_us', ['lang' => \App\Helpers\Business::LANG_DEFAULT])}}" class="menu-item-link" ><span class="menu_title">VỀ CHÚNG TÔI</span></a>                            
                            </li>
                        </ul>
                    </div>
                </div>
                </div>
                <!-- main-navigation -->
                <div class="header-position header-action inherit">
                <div class="header-block">
                    <a href="#"><img src="{{ asset('client') }}/assets/images/icons/vietnam.png" alt="" /></a>
                </div>
                <div class="header-block">
                    <a href="#"><img src="{{ asset('client') }}/assets/images/icons/english.jpg" alt="" /></a>
                </div>
                </div>
                <!-- header-action -->
            </div>
        </div>
    </div>
</header>
    