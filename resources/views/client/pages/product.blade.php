@extends('client.layouts.main')

@section ('style')
    <style type="text/css" id="kc-css-general">
        .kc-off-notice{display: inline-block !important;}
        .kc-container{max-width:1222px;}
    </style>
    <link rel='stylesheet'href='{{ asset('client') }}/css/style_home.css' type='text/css' media='all' />
@endsection

@section('content')
    <div class="fl-page-content">
        <div class="page-heading pt_50 pb_50">
            <div class="container">
                <div class="row">
                    <div class="page-heading-position tc">
                        <h2 class="page-title">Sản phẩm</h2>
                    </div>
                    <div class="page-heading-position tc">
                        <div class="breadcrumb"><a href="https://wp.jmstheme.com/erado" title="Home">Home</a> <span></span> Sản phẩm</div>
                    </div>
                </div>
            </div>
        </div>
        <header class="woocommerce-products-header">
        </header>
        <div class="clearfix"></div>
        <div class="container mt_80 mb_70">
            <div class="row ">
                <div id="main-content" class="fullwidth">
                    <div class="kc_clfw"></div>
                    <div class="shop-filter-cat">
                        <ul class="product-categories filter-grid">
                            <li class="cat-item cat-item-0"><a href="javascript:;" data-filter="all">All</a></li>
                            <li class="cat-item cat-item-35"><a href="javascript:;" data-filter="decor">Decor</a></li>
                            <li class="cat-item cat-item-34"><a href="javascript:;" data-filter="dining-chair-collection">Dining Chair</a></li>
                            <li class="cat-item cat-item-63"><a href="javascript:;" data-filter="essentials">Essentials</a></li>
                            <li class="cat-item cat-item-36"><a href="javascript:;" data-filter="furniture">Furniture</a></li>
                            <li class="cat-item cat-item-37"><a href="javascript:;" data-filter="interiors">Interiors</a></li>
                            <li class="cat-item cat-item-62"><a href="javascript:;" data-filter="light">Light</a></li>
                            <li class="cat-item cat-item-33"><a href="javascript:;" data-filter="office-collection">Office</a></li>
                            <li class="cat-item cat-item-38"><a href="javascript:;" data-filter="stationary">Stationary</a></li>
                            <li class="cat-item cat-item-15"><a href="javascript:;" data-filter="uncategorized">Uncategorized</a></li>
                        </ul>
                    </div>
                    <div class="shop-action">
                        <div class="shop-action-inner flex between-xs pb_10 ">
                            <div class="wc-switch clearfix hidden-xs">
                                <span>View mode:</span>
                                <a href="javascript:void(0)" class="active view-grid" title="Grid"><i class="icon-grid"></i></a>
                                <a href="javascript:void(0)" class="view-list" title="List"><i class="icon-list"></i></a>
                            </div>
                            <div>
                            </div>
                        </div>
                        <input type="hidden" name="current_url" id="current_url" value="" />
                    </div>
                    <div class="clearfix"></div>
                    <div class="product-layout-wrapper">
                        <div class="wc-loading hide"></div>
                        <div class="products product-layout product-grid product-spacing-30 product-columns-4" >
                            <div class="item mb_20 col-lg-3 col-md-3 col-sm-4 col-xs-6 cat-slug-decor">
                                <div class="item-animation">
                                    <div class="product-box">
                                        <div class="style-2 ">
                                            <div class="product-thumb pr oh">
                                                <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="360" src="/client/uploads/2018/04/2-300x360.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="/client/uploads/2018/04/2-300x360.jpg 300w, /client/uploads/2018/04/2.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /><img width="300" height="360" src="/client/uploads/2018/04/15-300x360.jpg" class="secondary-image attachment-shop-catalog" alt="" srcset="/client/uploads/2018/04/15-300x360.jpg 300w, /client/uploads/2018/04/15.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="title-product middle-xs between-xs">
                                                    <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Condimentum Libero Dolor</a>
                                                </div>
                                                <span class="product-cat db mb_3"> <a href="javascript:;" data-filter="furniture/" rel="tag">Furniture</a></span>
                                                <div class="middle-xs price-box">
                                                    <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>120.00</span></span>
                                                </div>
                                                <div class="product-extra">
                                                    <div class="woocommerce-product-details__short-description">
                                                        <p>Proin accumsan porta miac maximus. In dapibus mollis risus, nec mattis sem gravida quam volutpat. Donec pellentesque vulputat. Aliquam vitae felis leo sodales tincidunt lectus.</p>
                                                    </div>
                                                    <div class="add-to-cart">
                                                        <a href="/erado/shop/?add-to-cart=393" data-quantity="1" data-product_id="393" data-product_sku="" class="button-cart button product_type_simple add_to_cart_button ajax_add_to_cart"><i class="fa fa-spinner spinner"></i><i class="fa fa-check"></i>Add to cart<span class="tooltip">Add to cart</span></a>
                                                    </div>
                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-393">
                                                        <div class="yith-wcwl-add-button pr show">
                                                            <a href="/erado/shop/?add_to_wishlist=393" data-product-id="393" data-product-type="simple" class="button add_to_wishlist" >
                                                                <span class="text-hidden">Add to wishlist</span>
                                                                <i class="fa-heart1"></i>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-remove-button pr yith-wcwl-wishlistaddedbrowse hide">
                                                            <a class="button" href="https://wp.jmstheme.com/erado/wishlist/" data-product-id="393">
                                                                <i class="fa-heart2"></i><span class="text-hidden">Remove from Wishlist</span>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-wishlistaddresponse"></div>
                                                    </div>
                                                    <div class="woocommerce product compare-button"><a href="https://wp.jmstheme.com/erado?action=yith-woocompare-add-product&id=393" class="compare button" data-product_id="393" rel="nofollow">Compare</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item mb_20 col-lg-3 col-md-3 col-sm-4 col-xs-6 cat-slug-decor">
                                <div class="item-animation">
                                    <div class="product-box">
                                        <div class="style-2 ">
                                            <div class="product-thumb pr oh">
                                 <span class="badge pa tc dib">
                                 <span class="onsale">Sale</span> </span>
                                                <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="360" src="/client/uploads/2018/04/14-300x360.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="/client/uploads/2018/04/14-300x360.jpg 300w, /client/uploads/2018/04/14.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /><img width="300" height="360" src="/client/uploads/2018/04/15-300x360.jpg" class="secondary-image attachment-shop-catalog" alt="" srcset="/client/uploads/2018/04/15-300x360.jpg 300w, /client/uploads/2018/04/15.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="title-product middle-xs between-xs">
                                                    <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Dapibus Mollis Risus</a>
                                                </div>
                                                <span class="product-cat db mb_3"> <a href="javascript:;" data-filter="furniture/" rel="tag">Furniture</a></span>
                                                <div class="middle-xs price-box">
                                                    <span class="price"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>55.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>50.00</span></ins></span>
                                                </div>
                                                <div class="product-extra">
                                                    <div class="woocommerce-product-details__short-description">
                                                        <p>Proin accumsan porta miac maximus. In dapibus mollis risus, nec mattis sem gravida quam volutpat. Donec pellentesque vulputat. Aliquam vitae felis leo sodales tincidunt lectus.</p>
                                                    </div>
                                                    <div class="add-to-cart">
                                                        <a href="https://themeforest.net/user/joommasters" data-quantity="1" data-product_id="381" data-product_sku="" class="button-cart button product_type_external"><i class="fa fa-spinner spinner"></i><i class="fa fa-check"></i>Read more<span class="tooltip">Read more</span></a>
                                                    </div>
                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-381">
                                                        <div class="yith-wcwl-add-button pr hide">
                                                            <a href="/erado/shop/?add_to_wishlist=381" data-product-id="381" data-product-type="external" class="button add_to_wishlist" >
                                                                <span class="text-hidden">Add to wishlist</span>
                                                                <i class="fa-heart1"></i>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-remove-button pr yith-wcwl-wishlistaddedbrowse show">
                                                            <a class="button" href="https://wp.jmstheme.com/erado/wishlist/" data-product-id="381">
                                                                <i class="fa-heart2"></i><span class="text-hidden">Remove from Wishlist</span>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-wishlistaddresponse"></div>
                                                    </div>
                                                    <div class="woocommerce product compare-button"><a href="https://wp.jmstheme.com/erado?action=yith-woocompare-add-product&id=381" class="compare button" data-product_id="381" rel="nofollow">Compare</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item mb_20 col-lg-3 col-md-3 col-sm-4 col-xs-6 cat-slug-decor">
                                <div class="item-animation">
                                    <div class="product-box">
                                        <div class="style-2 ">
                                            <div class="product-thumb pr oh">
                                                <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="360" src="/client/uploads/2018/04/14-300x360.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="/client/uploads/2018/04/14-300x360.jpg 300w, /client/uploads/2018/04/23.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /><img width="300" height="360" src="/client/uploads/2018/04/18-300x360.jpg" class="secondary-image attachment-shop-catalog" alt="" srcset="/client/uploads/2018/04/18-300x360.jpg 300w, /client/uploads/2018/04/18.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="title-product middle-xs between-xs">
                                                    <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Dapibus mollis risuss</a>
                                                </div>
                                                <span class="product-cat db mb_3"> <a href="javascript:;" data-filter="light/" rel="tag">Light</a></span>
                                                <div class="middle-xs price-box">
                                                    <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>76.00</span></span>
                                                </div>
                                                <div class="product-extra">
                                                    <div class="woocommerce-product-details__short-description">
                                                        <p>Proin accumsan porta miac maximus. In dapibus mollis risus, nec mattis sem gravida quam volutpat. Donec pellentesque vulputat. Aliquam vitae felis leo sodales tincidunt lectus.</p>
                                                    </div>
                                                    <div class="add-to-cart">
                                                        <a href="/erado/shop/?add-to-cart=371" data-quantity="1" data-product_id="371" data-product_sku="" class="button-cart button product_type_simple add_to_cart_button ajax_add_to_cart"><i class="fa fa-spinner spinner"></i><i class="fa fa-check"></i>Add to cart<span class="tooltip">Add to cart</span></a>
                                                    </div>
                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-371">
                                                        <div class="yith-wcwl-add-button pr show">
                                                            <a href="/erado/shop/?add_to_wishlist=371" data-product-id="371" data-product-type="simple" class="button add_to_wishlist" >
                                                                <span class="text-hidden">Add to wishlist</span>
                                                                <i class="fa-heart1"></i>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-remove-button pr yith-wcwl-wishlistaddedbrowse hide">
                                                            <a class="button" href="https://wp.jmstheme.com/erado/wishlist/" data-product-id="371">
                                                                <i class="fa-heart2"></i><span class="text-hidden">Remove from Wishlist</span>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-wishlistaddresponse"></div>
                                                    </div>
                                                    <div class="woocommerce product compare-button"><a href="https://wp.jmstheme.com/erado?action=yith-woocompare-add-product&id=371" class="compare button" data-product_id="371" rel="nofollow">Compare</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item mb_20 col-lg-3 col-md-3 col-sm-4 col-xs-6 cat-slug-decor">
                                <div class="item-animation">
                                    <div class="product-box">
                                        <div class="style-2 ">
                                            <div class="product-thumb pr oh">
                                                <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="360" src="/client/uploads/2018/04/24.1-300x360.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="/client/uploads/2018/04/24.1-300x360.jpg 300w, /client/uploads/2018/04/24.1.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /><img width="300" height="360" src="/client/uploads/2018/04/24.5-300x360.jpg" class="secondary-image attachment-shop-catalog" alt="" srcset="/client/uploads/2018/04/24.5-300x360.jpg 300w, /client/uploads/2018/04/24.5.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="title-product middle-xs between-xs">
                                                    <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Erat accumsan lorem</a>
                                                </div>
                                                <span class="product-cat db mb_3"> <a href="javascript:;" data-filter="light/" rel="tag">Light</a></span>
                                                <div class="middle-xs price-box">
                                                    <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>90.00</span></span>
                                                </div>
                                                <div class="product-extra">
                                                    <div class="woocommerce-product-details__short-description">
                                                        <p>Proin accumsan porta miac maximus. In dapibus mollis risus, nec mattis sem gravida quam volutpat. Donec pellentesque vulputat. Aliquam vitae felis leo sodales tincidunt lectus.</p>
                                                    </div>
                                                    <div class="add-to-cart">
                                                        <a href="/erado/shop/?add-to-cart=370" data-quantity="1" data-product_id="370" data-product_sku="" class="button-cart button product_type_simple add_to_cart_button ajax_add_to_cart"><i class="fa fa-spinner spinner"></i><i class="fa fa-check"></i>Add to cart<span class="tooltip">Add to cart</span></a>
                                                    </div>
                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-370">
                                                        <div class="yith-wcwl-add-button pr show">
                                                            <a href="/erado/shop/?add_to_wishlist=370" data-product-id="370" data-product-type="simple" class="button add_to_wishlist" >
                                                                <span class="text-hidden">Add to wishlist</span>
                                                                <i class="fa-heart1"></i>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-remove-button pr yith-wcwl-wishlistaddedbrowse hide">
                                                            <a class="button" href="https://wp.jmstheme.com/erado/wishlist/" data-product-id="370">
                                                                <i class="fa-heart2"></i><span class="text-hidden">Remove from Wishlist</span>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-wishlistaddresponse"></div>
                                                    </div>
                                                    <div class="woocommerce product compare-button"><a href="https://wp.jmstheme.com/erado?action=yith-woocompare-add-product&id=370" class="compare button" data-product_id="370" rel="nofollow">Compare</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item mb_20 col-lg-3 col-md-3 col-sm-4 col-xs-6 cat-slug-essentials">
                                <div class="item-animation">
                                    <div class="product-box">
                                        <div class="style-2 product-sold-out">
                                            <div class="product-thumb pr oh">
                                 <span class="badge pa tc dib">
                                 <span class="sold-out">Sold Out</span> </span>
                                                <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="360" src="/client/uploads/2018/04/13-300x360.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="/client/uploads/2018/04/13-300x360.jpg 300w, /client/uploads/2018/04/13.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /><img width="300" height="360" src="/client/uploads/2018/04/14-300x360.jpg" class="secondary-image attachment-shop-catalog" alt="" srcset="/client/uploads/2018/04/14-300x360.jpg 300w, /client/uploads/2018/04/14.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="title-product middle-xs between-xs">
                                                    <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Feugiat Gravida Odio</a>
                                                </div>
                                                <span class="product-cat db mb_3"> <a href="javascript:;" data-filter="essentials/" rel="tag">Essentials</a></span>
                                                <div class="middle-xs price-box">
                                                    <span class="price"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>250.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>150.00</span></ins></span>
                                                </div>
                                                <div class="product-extra">
                                                    <div class="woocommerce-product-details__short-description">
                                                        <p>Proin accumsan porta miac maximus. In dapibus mollis risus, nec mattis sem gravida quam volutpat. Donec pellentesque vulputat. Aliquam vitae felis leo sodales tincidunt lectus.</p>
                                                    </div>
                                                    <div class="add-to-cart">
                                                        <a href="http://bi-acaovo.net/erado/single-product.php" data-quantity="1" data-product_id="382" data-product_sku="15" class="button-cart button product_type_simple ajax_add_to_cart"><i class="fa fa-spinner spinner"></i><i class="fa fa-check"></i>Read more<span class="tooltip">Read more</span></a>
                                                    </div>
                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-382">
                                                        <div class="yith-wcwl-add-button pr show">
                                                            <a href="/erado/shop/?add_to_wishlist=382" data-product-id="382" data-product-type="simple" class="button add_to_wishlist" >
                                                                <span class="text-hidden">Add to wishlist</span>
                                                                <i class="fa-heart1"></i>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-remove-button pr yith-wcwl-wishlistaddedbrowse hide">
                                                            <a class="button" href="https://wp.jmstheme.com/erado/wishlist/" data-product-id="382">
                                                                <i class="fa-heart2"></i><span class="text-hidden">Remove from Wishlist</span>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-wishlistaddresponse"></div>
                                                    </div>
                                                    <div class="woocommerce product compare-button"><a href="https://wp.jmstheme.com/erado?action=yith-woocompare-add-product&id=382" class="compare button" data-product_id="382" rel="nofollow">Compare</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item mb_20 col-lg-3 col-md-3 col-sm-4 col-xs-6 cat-slug-essentials">
                                <div class="item-animation">
                                    <div class="product-box">
                                        <div class="style-2 ">
                                            <div class="product-thumb pr oh">
                                                <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="360" src="/client/uploads/2018/04/12.1-300x360.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="/client/uploads/2018/04/12.1-300x360.jpg 300w, /client/uploads/2018/04/12.1.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /><img width="300" height="360" src="/client/uploads/2018/04/12.3-300x360.jpg" class="secondary-image attachment-shop-catalog" alt="" srcset="/client/uploads/2018/04/12.3-300x360.jpg 300w, /client/uploads/2018/04/12.3.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="title-product middle-xs between-xs">
                                                    <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Gravida Quam Volutpat</a>
                                                </div>
                                                <span class="product-cat db mb_3"> <a href="javascript:;" data-filter="office-collection/" rel="tag">Office</a></span>
                                                <div class="middle-xs price-box">
                                                    <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>94.00</span></span>
                                                </div>
                                                <div class="product-extra">
                                                    <div class="woocommerce-product-details__short-description">
                                                        <p>Proin accumsan porta miac maximus. In dapibus mollis risus, nec mattis sem gravida quam volutpat. Donec pellentesque vulputat. Aliquam vitae felis leo sodales tincidunt lectus.</p>
                                                    </div>
                                                    <div class="add-to-cart">
                                                        <a href="/erado/shop/?add-to-cart=383" data-quantity="1" data-product_id="383" data-product_sku="29" class="button-cart button product_type_simple add_to_cart_button ajax_add_to_cart"><i class="fa fa-spinner spinner"></i><i class="fa fa-check"></i>Add to cart<span class="tooltip">Add to cart</span></a>
                                                    </div>
                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-383">
                                                        <div class="yith-wcwl-add-button pr show">
                                                            <a href="/erado/shop/?add_to_wishlist=383" data-product-id="383" data-product-type="simple" class="button add_to_wishlist" >
                                                                <span class="text-hidden">Add to wishlist</span>
                                                                <i class="fa-heart1"></i>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-remove-button pr yith-wcwl-wishlistaddedbrowse hide">
                                                            <a class="button" href="https://wp.jmstheme.com/erado/wishlist/" data-product-id="383">
                                                                <i class="fa-heart2"></i><span class="text-hidden">Remove from Wishlist</span>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-wishlistaddresponse"></div>
                                                    </div>
                                                    <div class="woocommerce product compare-button"><a href="https://wp.jmstheme.com/erado?action=yith-woocompare-add-product&id=383" class="compare button" data-product_id="383" rel="nofollow">Compare</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item mb_20 col-lg-3 col-md-3 col-sm-4 col-xs-6 cat-slug-essentials">
                                <div class="item-animation">
                                    <div class="product-box">
                                        <div class="style-2 ">
                                            <div class="product-thumb pr oh">
                                                <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="360" src="/client/uploads/2018/04/22-300x360.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="/client/uploads/2018/04/22-300x360.jpg 300w, /client/uploads/2018/04/22.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /><img width="300" height="360" src="/client/uploads/2018/04/20-300x360.jpg" class="secondary-image attachment-shop-catalog" alt="" srcset="/client/uploads/2018/04/20-300x360.jpg 300w, /client/uploads/2018/04/20.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="title-product middle-xs between-xs">
                                                    <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Hella Jongerius Special</a>
                                                </div>
                                                <span class="product-cat db mb_3"> <a href="javascript:;" data-filter="light/" rel="tag">Light</a></span>
                                                <div class="middle-xs price-box">
                                                    <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>90.00</span></span>
                                                </div>
                                                <div class="product-extra">
                                                    <div class="woocommerce-product-details__short-description">
                                                        <p>Vestibulum curae torquent diam diam commodo parturient penatibus nunc dui adipiscing.</p>
                                                    </div>
                                                    <div class="add-to-cart">
                                                        <a href="/erado/shop/?add-to-cart=372" data-quantity="1" data-product_id="372" data-product_sku="" class="button-cart button product_type_simple add_to_cart_button ajax_add_to_cart"><i class="fa fa-spinner spinner"></i><i class="fa fa-check"></i>Add to cart<span class="tooltip">Add to cart</span></a>
                                                    </div>
                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-372">
                                                        <div class="yith-wcwl-add-button pr show">
                                                            <a href="/erado/shop/?add_to_wishlist=372" data-product-id="372" data-product-type="simple" class="button add_to_wishlist" >
                                                                <span class="text-hidden">Add to wishlist</span>
                                                                <i class="fa-heart1"></i>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-remove-button pr yith-wcwl-wishlistaddedbrowse hide">
                                                            <a class="button" href="https://wp.jmstheme.com/erado/wishlist/" data-product-id="372">
                                                                <i class="fa-heart2"></i><span class="text-hidden">Remove from Wishlist</span>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-wishlistaddresponse"></div>
                                                    </div>
                                                    <div class="woocommerce product compare-button"><a href="https://wp.jmstheme.com/erado?action=yith-woocompare-add-product&id=372" class="compare button" data-product_id="372" rel="nofollow">Compare</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item mb_20 col-lg-3 col-md-3 col-sm-4 col-xs-6 cat-slug-essentials">
                                <div class="item-animation">
                                    <div class="product-box">
                                        <div class="style-2 ">
                                            <div class="product-thumb pr oh">
                                                <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="360" src="/client/uploads/2018/04/19-300x360.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="/client/uploads/2018/04/19-300x360.jpg 300w, /client/uploads/2018/04/19.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /><img width="300" height="360" src="/client/uploads/2018/04/20-300x360.jpg" class="secondary-image attachment-shop-catalog" alt="" srcset="/client/uploads/2018/04/20-300x360.jpg 300w, /client/uploads/2018/04/20.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="title-product middle-xs between-xs">
                                                    <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Lorem Erat Accumsan</a>
                                                </div>
                                                <span class="product-cat db mb_3"> <a href="javascript:;" data-filter="light/" rel="tag">Light</a></span>
                                                <div class="middle-xs price-box">
                                                    <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>60.00</span></span>
                                                </div>
                                                <div class="product-extra">
                                                    <div class="woocommerce-product-details__short-description">
                                                        <p>Proin accumsan porta miac maximus. In dapibus mollis risus, nec mattis sem gravida quam volutpat. Donec pellentesque vulputat. Aliquam vitae felis leo sodales tincidunt lectus.</p>
                                                    </div>
                                                    <div class="add-to-cart">
                                                        <a href="/erado/shop/?add-to-cart=377" data-quantity="1" data-product_id="377" data-product_sku="" class="button-cart button product_type_simple add_to_cart_button ajax_add_to_cart"><i class="fa fa-spinner spinner"></i><i class="fa fa-check"></i>Add to cart<span class="tooltip">Add to cart</span></a>
                                                    </div>
                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-377">
                                                        <div class="yith-wcwl-add-button pr show">
                                                            <a href="/erado/shop/?add_to_wishlist=377" data-product-id="377" data-product-type="simple" class="button add_to_wishlist" >
                                                                <span class="text-hidden">Add to wishlist</span>
                                                                <i class="fa-heart1"></i>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-remove-button pr yith-wcwl-wishlistaddedbrowse hide">
                                                            <a class="button" href="https://wp.jmstheme.com/erado/wishlist/" data-product-id="377">
                                                                <i class="fa-heart2"></i><span class="text-hidden">Remove from Wishlist</span>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-wishlistaddresponse"></div>
                                                    </div>
                                                    <div class="woocommerce product compare-button"><a href="https://wp.jmstheme.com/erado?action=yith-woocompare-add-product&id=377" class="compare button" data-product_id="377" rel="nofollow">Compare</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item mb_20 col-lg-3 col-md-3 col-sm-4 col-xs-6 cat-slug-dining-chair-collection">
                                <div class="item-animation">
                                    <div class="product-box">
                                        <div class="style-2 ">
                                            <div class="product-thumb pr oh">
                                                <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="360" src="/client/uploads/2018/04/4.1-300x360.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="/client/uploads/2018/04/4.1-300x360.jpg 300w, /client/uploads/2018/04/4.1.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /><img width="300" height="360" src="/client/uploads/2018/04/4.2-300x360.jpg" class="secondary-image attachment-shop-catalog" alt="" srcset="/client/uploads/2018/04/4.2-300x360.jpg 300w, /client/uploads/2018/04/4.2.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /></a>
                                                <div class="imageswatch-list-variations">
                                                    <ul>
                                                        <li class="imageswatch-product-variation"><a href="http://bi-acaovo.net/erado/single-product.php?attribute_pa_color=orange" data-thumb="" class="swatch-variation" style="background-image: url(/client/uploads/2018/03/orange.jpg); background-size: cover; text-indent: -999em;">orange</a></li>
                                                        <li class="imageswatch-product-variation"><a href="http://bi-acaovo.net/erado/single-product.php?attribute_pa_color=red" data-thumb="" class="swatch-variation" style="background-image: url(/client/uploads/2018/03/litle_red.jpg); background-size: cover; text-indent: -999em;">red</a></li>
                                                        <li class="imageswatch-product-variation"><a href="http://bi-acaovo.net/erado/single-product.php?attribute_pa_color=teal" data-thumb="" class="swatch-variation" style="background-image: url(/client/uploads/2018/03/teal-1.jpg); background-size: cover; text-indent: -999em;">teal</a></li>
                                                        <li class="imageswatch-product-variation"><a href="http://bi-acaovo.net/erado/single-product.php?attribute_pa_color=yellow" data-thumb="" class="swatch-variation" style="background-image: url(/client/uploads/2018/03/yellow.jpg); background-size: cover; text-indent: -999em;">yellow</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="product-info">
                                                <div class="title-product middle-xs between-xs">
                                                    <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Mauris Vestibulum</a>
                                                </div>
                                                <span class="product-cat db mb_3"> <a href="javascript:;" data-filter="light/" rel="tag">Light</a></span>
                                                <div class="middle-xs price-box">
                                                    <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>80.00</span> &ndash; <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>100.00</span></span>
                                                </div>
                                                <div class="product-extra">
                                                    <div class="woocommerce-product-details__short-description">
                                                        <p>Proin accumsan porta miac maximus. In dapibus mollis risus, nec mattis sem gravida quam volutpat. Donec pellentesque vulputat. Aliquam vitae felis leo sodales tincidunt lectus.</p>
                                                        <p>Sed magna ipsum, posuere lectus, feugiat gravida odio. Sed pellentesque magna congue tincidunt</p>
                                                    </div>
                                                    <div class="add-to-cart">
                                                        <a href="http://bi-acaovo.net/erado/single-product.php" data-quantity="1" data-product_id="391" data-product_sku="" class="button-cart button product_type_variable add_to_cart_button"><i class="fa fa-spinner spinner"></i><i class="fa fa-check"></i>Select options<span class="tooltip">Select options</span></a>
                                                    </div>
                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-391">
                                                        <div class="yith-wcwl-add-button pr hide">
                                                            <a href="/erado/shop/?add_to_wishlist=391" data-product-id="391" data-product-type="variable" class="button add_to_wishlist" >
                                                                <span class="text-hidden">Add to wishlist</span>
                                                                <i class="fa-heart1"></i>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-remove-button pr yith-wcwl-wishlistaddedbrowse show">
                                                            <a class="button" href="https://wp.jmstheme.com/erado/wishlist/" data-product-id="391">
                                                                <i class="fa-heart2"></i><span class="text-hidden">Remove from Wishlist</span>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-wishlistaddresponse"></div>
                                                    </div>
                                                    <div class="woocommerce product compare-button"><a href="https://wp.jmstheme.com/erado?action=yith-woocompare-add-product&id=391" class="compare button" data-product_id="391" rel="nofollow">Compare</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item mb_20 col-lg-3 col-md-3 col-sm-4 col-xs-6 cat-slug-dining-chair-collection">
                                <div class="item-animation">
                                    <div class="product-box">
                                        <div class="style-2 ">
                                            <div class="product-thumb pr oh">
                                 <span class="badge pa tc dib">
                                 <span class="onsale">Sale</span> </span>
                                                <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="360" src="/client/uploads/2018/04/21-300x360.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="/client/uploads/2018/04/21-300x360.jpg 300w, /client/uploads/2018/04/21.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /><img width="300" height="360" src="/client/uploads/2018/04/14-300x360.jpg" class="secondary-image attachment-shop-catalog" alt="" srcset="/client/uploads/2018/04/14-300x360.jpg 300w, /client/uploads/2018/04/23.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="title-product middle-xs between-xs">
                                                    <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Normann Copenhagen</a>
                                                </div>
                                                <span class="product-cat db mb_3"> <a href="javascript:;" data-filter="light/" rel="tag">Light</a></span>
                                                <div class="middle-xs price-box">
                                                    <span class="price"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>99.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>90.00</span></ins></span>
                                                </div>
                                                <div class="product-extra">
                                                    <div class="woocommerce-product-details__short-description">
                                                        <p>Proin accumsan porta miac maximus. In dapibus mollis risus, nec mattis sem gravida quam volutpat. Donec pellentesque vulputat. Aliquam vitae felis leo sodales tincidunt lectus.</p>
                                                    </div>
                                                    <div class="add-to-cart">
                                                        <a href="https://themeforest.net/user/joommasters" data-quantity="1" data-product_id="375" data-product_sku="" class="button-cart button product_type_external"><i class="fa fa-spinner spinner"></i><i class="fa fa-check"></i>shop now<span class="tooltip">shop now</span></a>
                                                    </div>
                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-375">
                                                        <div class="yith-wcwl-add-button pr show">
                                                            <a href="/erado/shop/?add_to_wishlist=375" data-product-id="375" data-product-type="external" class="button add_to_wishlist" >
                                                                <span class="text-hidden">Add to wishlist</span>
                                                                <i class="fa-heart1"></i>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-remove-button pr yith-wcwl-wishlistaddedbrowse hide">
                                                            <a class="button" href="https://wp.jmstheme.com/erado/wishlist/" data-product-id="375">
                                                                <i class="fa-heart2"></i><span class="text-hidden">Remove from Wishlist</span>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-wishlistaddresponse"></div>
                                                    </div>
                                                    <div class="woocommerce product compare-button"><a href="https://wp.jmstheme.com/erado?action=yith-woocompare-add-product&id=375" class="compare button" data-product_id="375" rel="nofollow">Compare</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item mb_20 col-lg-3 col-md-3 col-sm-4 col-xs-6 cat-slug-dining-chair-collection">
                                <div class="item-animation">
                                    <div class="product-box">
                                        <div class="style-2 product-sold-out">
                                            <div class="product-thumb pr oh">
                                 <span class="badge pa tc dib">
                                 <span class="sold-out">Sold Out</span> </span>
                                                <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="360" src="/client/uploads/2018/04/25-300x360.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="/client/uploads/2018/04/25-300x360.jpg 300w, /client/uploads/2018/04/25.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /><img width="300" height="360" src="/client/uploads/2018/04/25-300x360.jpg" class="secondary-image attachment-shop-catalog" alt="" srcset="/client/uploads/2018/04/25-300x360.jpg 300w, /client/uploads/2018/04/25.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="title-product middle-xs between-xs">
                                                    <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Pellentesque magna</a>
                                                </div>
                                                <span class="product-cat db mb_3"> <a href="javascript:;" data-filter="light/" rel="tag">Light</a></span>
                                                <div class="middle-xs price-box">
                                                    <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>300.00</span></span>
                                                </div>
                                                <div class="product-extra">
                                                    <div class="woocommerce-product-details__short-description">
                                                        <p>Proin accumsan porta miac maximus. In dapibus mollis risus, nec mattis sem gravida quam volutpat. Donec pellentesque vulputat. Aliquam vitae felis leo sodales tincidunt lectus.</p>
                                                    </div>
                                                    <div class="add-to-cart">
                                                        <a href="http://bi-acaovo.net/erado/single-product.php" data-quantity="1" data-product_id="369" data-product_sku="2-1-4" class="button-cart button product_type_simple ajax_add_to_cart"><i class="fa fa-spinner spinner"></i><i class="fa fa-check"></i>Read more<span class="tooltip">Read more</span></a>
                                                    </div>
                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-369">
                                                        <div class="yith-wcwl-add-button pr show">
                                                            <a href="/erado/shop/?add_to_wishlist=369" data-product-id="369" data-product-type="simple" class="button add_to_wishlist" >
                                                                <span class="text-hidden">Add to wishlist</span>
                                                                <i class="fa-heart1"></i>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-remove-button pr yith-wcwl-wishlistaddedbrowse hide">
                                                            <a class="button" href="https://wp.jmstheme.com/erado/wishlist/" data-product-id="369">
                                                                <i class="fa-heart2"></i><span class="text-hidden">Remove from Wishlist</span>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-wishlistaddresponse"></div>
                                                    </div>
                                                    <div class="woocommerce product compare-button"><a href="https://wp.jmstheme.com/erado?action=yith-woocompare-add-product&id=369" class="compare button" data-product_id="369" rel="nofollow">Compare</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item mb_20 col-lg-3 col-md-3 col-sm-4 col-xs-6 cat-slug-dining-chair-collection">
                                <div class="item-animation">
                                    <div class="product-box">
                                        <div class="style-2 product-sold-out">
                                            <div class="product-thumb pr oh">
                                 <span class="badge pa tc dib">
                                 <span class="sold-out">Sold Out</span> </span>
                                                <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="360" src="/client/uploads/2018/04/11-300x360.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="/client/uploads/2018/04/11-300x360.jpg 300w, /client/uploads/2018/04/11.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /><img width="300" height="360" src="/client/uploads/2018/04/10-300x360.jpg" class="secondary-image attachment-shop-catalog" alt="" srcset="/client/uploads/2018/04/10-300x360.jpg 300w, /client/uploads/2018/04/10.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /></a>
                                            </div>
                                            <div class="product-info">
                                                <div class="title-product middle-xs between-xs">
                                                    <a href="http://bi-acaovo.net/erado/single-product.php" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Pellentesque Mattis Purus</a>
                                                </div>
                                                <span class="product-cat db mb_3"> <a href="javascript:;" data-filter="decor/" rel="tag">Decor</a></span>
                                                <div class="middle-xs price-box">
                                                    <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>99.00</span></span>
                                                </div>
                                                <div class="product-extra">
                                                    <div class="woocommerce-product-details__short-description">
                                                        <p>Proin accumsan porta miac maximus. In dapibus mollis risus, nec mattis sem gravida quam volutpat. Donec pellentesque vulputat. Aliquam vitae felis leo sodales tincidunt lectus.</p>
                                                    </div>
                                                    <div class="add-to-cart">
                                                        <a href="http://bi-acaovo.net/erado/single-product.php" data-quantity="1" data-product_id="384" data-product_sku="" class="button-cart button product_type_simple ajax_add_to_cart"><i class="fa fa-spinner spinner"></i><i class="fa fa-check"></i>Read more<span class="tooltip">Read more</span></a>
                                                    </div>
                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-384">
                                                        <div class="yith-wcwl-add-button pr show">
                                                            <a href="/erado/shop/?add_to_wishlist=384" data-product-id="384" data-product-type="simple" class="button add_to_wishlist" >
                                                                <span class="text-hidden">Add to wishlist</span>
                                                                <i class="fa-heart1"></i>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-remove-button pr yith-wcwl-wishlistaddedbrowse hide">
                                                            <a class="button" href="https://wp.jmstheme.com/erado/wishlist/" data-product-id="384">
                                                                <i class="fa-heart2"></i><span class="text-hidden">Remove from Wishlist</span>
                                                            </a>
                                                            <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                        </div>
                                                        <div class="yith-wcwl-wishlistaddresponse"></div>
                                                    </div>
                                                    <div class="woocommerce product compare-button"><a href="https://wp.jmstheme.com/erado?action=yith-woocompare-add-product&id=384" class="compare button" data-product_id="384" rel="nofollow">Compare</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="shop-action-bottom pt_20 clearfix">
                        <p class="woocommerce-result-count">
                            Showing 1&ndash;12 of 26 results
                        </p>
                        <nav class="woocommerce-pagination">
                            <ul class='page-numbers'>
                                <li><span aria-current='page' class='page-numbers current'>1</span></li>
                                <li><a class='page-numbers' href='https://wp.jmstheme.com/erado/shop/page/2/'>2</a></li>
                                <li><a class='page-numbers' href='https://wp.jmstheme.com/erado/shop/page/3/'>3</a></li>
                                <li><a class="next page-numbers" href="https://wp.jmstheme.com/erado/shop/page/2/">&rarr;</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- end content -->
            </div>
        </div>
    </div>

@endsection

@section ('script')
    <script>
        var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
        var htmlDivCss = "";
        if (htmlDiv) {
            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
        } else {
            var htmlDiv = document.createElement("div");
            htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
            document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
        }
    </script>
    <script>
        var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
        var htmlDivCss = "";
        if (htmlDiv) {
            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
        } else {
            var htmlDiv = document.createElement("div");
            htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
            document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
        }
    </script>
    <script type="text/javascript">
        if (setREVStartSize !== undefined) setREVStartSize({
            c: '#rev_slider_1_1',
            responsiveLevels: [1240, 1240, 778, 480],
            gridwidth: [1720, 1720, 778, 480],
            gridheight: [750, 750, 450, 350],
            sliderLayout: 'auto'
        });

        var revapi1,
            tpj;
        (function() {
            if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded", onLoad);
            else onLoad();

            function onLoad() {
                if (tpj === undefined) {
                    tpj = jQuery;
                    if ("off" == "on") tpj.noConflict();
                }
                if (tpj("#rev_slider_1_1").revolution == undefined) {
                    revslider_showDoubleJqueryError("#rev_slider_1_1");
                } else {
                    revapi1 = tpj("#rev_slider_1_1").show().revolution({
                        sliderType: "standard",
                        jsFileLocation: "https://eradocdn-cff8.kxcdn.com/wp-content/plugins/revslider/public/assets/js/",
                        sliderLayout: "auto",
                        dottedOverlay: "none",
                        delay: 9000,
                        navigation: {
                            keyboardNavigation: "off",
                            keyboard_direction: "horizontal",
                            mouseScrollNavigation: "off",
                            mouseScrollReverse: "default",
                            onHoverStop: "off",
                            bullets: {
                                enable: true,
                                hide_onmobile: false,
                                style: "erado_bullet",
                                hide_onleave: false,
                                direction: "horizontal",
                                h_align: "center",
                                v_align: "bottom",
                                h_offset: 0,
                                v_offset: 20,
                                space: 5,
                                tmp: '<span class="tp-bullet-inner"></span>'
                            }
                        },
                        responsiveLevels: [1240, 1240, 778, 480],
                        visibilityLevels: [1240, 1240, 778, 480],
                        gridwidth: [1720, 1720, 778, 480],
                        gridheight: [750, 750, 450, 350],
                        lazyType: "none",
                        shadow: 0,
                        spinner: "spinner0",
                        stopLoop: "off",
                        stopAfterLoops: -1,
                        stopAtSlide: -1,
                        shuffle: "off",
                        autoHeight: "off",
                        disableProgressBar: "on",
                        hideThumbsOnMobile: "off",
                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        debugMode: false,
                        fallbacks: {
                            simplifyAll: "off",
                            nextSlideOnWindowFocus: "off",
                            disableFocusListener: false,
                        }
                    });
                }; /* END OF revapi call */

            }; /* END OF ON LOAD FUNCTION */
        }()); /* END OF WRAPPING FUNCTION */
    </script>
    <script>
        var htmlDivCss = unescape("%23rev_slider_1_1%20.erado_bullet%20.tp-bullet%7B%0A%20%20border-radius%3A%2050%25%3B%0A%20%20background%3Atransparent%3B%0A%20%20width%3A14px%3B%0A%20%20height%3A14px%3B%0A%7D%0A%23rev_slider_1_1%20.erado_bullet%20.tp-bullet%3Abefore%7B%0A%20%20%20%20content%3A%20%27%27%3B%0A%20%20%20%20position%3A%20absolute%3B%0A%20%20%20%20width%3A%208px%3B%0A%20%20%20%20height%3A%208px%3B%0A%20%20%20%20top%3A%2050%25%3B%0A%20%20%20%20left%3A%2050%25%3B%0A%20%20%20%20margin-top%3A%20-4px%3B%0A%20%20%20%20margin-left%3A%20-4px%3B%0A%20%20%20%20display%3A%20inline-block%3B%0A%20%20%20%20border-radius%3A%20100%25%3B%0A%20%20%20%20background-color%3A%20rgb%28254%2C%2079%2C%2024%29%3B%0A%7D%0A%23rev_slider_1_1%20.erado_bullet%20.tp-bullet.selected%2C%0A%23rev_slider_1_1%20.erado_bullet%20.tp-bullet%3Ahover%20%7B%0A%20%20box-shadow%3A%200%200%200%202px%20rgba%28254%2C%2079%2C%2024%2C1%29%3B%0A%20%20border%3Anone%3B%0A%20%20border-radius%3A%2050%25%3B%0A%20%20background%3Atransparent%3B%0A%7D%0A%0A%23rev_slider_1_1%20.erado_bullet%20.tp-bullet.selected%20.tp-bullet-inner%2C%0A%23rev_slider_1_1%20.erado_bullet%20.tp-bullet%3Ahover%20.tp-bullet-inner%7B%0A%20transform%3A%20scale%280.4%29%3B%0A%20-webkit-transform%3A%20scale%280.4%29%3B%0A%20background-color%3Argb%28254%2C%2079%2C%2024%29%3B%0A%7D%0A");
        var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
        if (htmlDiv) {
            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
        } else {
            var htmlDiv = document.createElement('div');
            htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
            document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
        }
    </script>
@endsection