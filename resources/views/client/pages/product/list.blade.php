@extends('client.layouts.main')

@section('style')
    <style type="text/css" id="kc-css-general">
        .kc-off-notice {
            display: inline-block !important;
        }
        .kc-container {
            max-width:1222px;
        }
    </style>
    <style type="text/css" id="kc-css-render">
        body.kc-css-system .kc-css-394364 ,body.kc-css-system .kc-css-394364 p {
            color:#949494;
            font-size:40px;
            line-height:30px;
        }
        body.kc-css-system .kc-css-191211.kc_title,body.kc-css-system .kc-css-191211 .kc_title,body.kc-css-system .kc-css-191211 .kc_title a.kc_title_link {
            text-transform:uppercase;
            text-align:center;
            margin-top:0px;
        }
    </style>
@endsection

@section('content')
    <input type="hidden" name="lang" value="{{$lang}}">
    <div class="fl-page-content">
        <div class="page-heading pt_50 pb_50">
            <div class="container">
                <div class="row">
                    <div class="page-heading-position tc">
                        <h2 class="page-title">Sản phẩm</h2>
                    </div>
                    <div class="page-heading-position tc">
                        <div class="breadcrumb"><a href="https://wp.jmstheme.com/erado" title="Home">Home</a> <span></span> Sản phẩm</div>
                    </div>
                </div>
            </div>
        </div>
        <header class="woocommerce-products-header">
        </header>
        <div class="clearfix"></div>
        <div class="container mt_80 mb_70">
            <div class="row ">
                <div id="main-content" class="fullwidth">
                    <div class="kc_clfw"></div>
                    <div class="shop-filter-cat">
                        <ul class="product-categories filter-grid">
                            <li class="cat-item cat-item-0"><a href="javascript:;" data-filter="all">All</a></li>
                            @foreach($categorys as $category)
                            <li class="cat-item cat-item-35"><a href="javascript:;" data-filter="{{$category->cat_slug}}">{{$category->name}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                    {{--<div class="shop-action">
                        <div class="shop-action-inner flex between-xs pb_10 ">
                            <div class="wc-switch clearfix hidden-xs">
                                <span>View mode:</span>
                                <a href="javascript:void(0)" class="active view-grid" title="Grid"><i class="icon-grid"></i></a>
                                <a href="javascript:void(0)" class="view-list" title="List"><i class="icon-list"></i></a>
                            </div>
                            <div>
                            </div>
                        </div>
                        <input type="hidden" name="current_url" id="current_url" value="" />
                    </div>--}}
                    <div class="clearfix"></div>
                    <div class="product-layout-wrapper">
                        <div class="wc-loading hide"></div>
                        <div class="products product-layout product-grid product-spacing-30 product-columns-4" >

                            <div id="product_html" ></div>

                        </div>
                    </div>
                    <div class="shop-action-bottom pt_20 clearfix">
                        <p class="woocommerce-result-count">
                            Showing 1&ndash;12 of 26 results
                        </p>
                        <nav class="woocommerce-pagination">
                            <ul class='page-numbers'>
                                <li><span aria-current='page' class='page-numbers current'>1</span></li>
                                <li><a class='page-numbers' href='https://wp.jmstheme.com/erado/shop/page/2/'>2</a></li>
                                <li><a class='page-numbers' href='https://wp.jmstheme.com/erado/shop/page/3/'>3</a></li>
                                <li><a class="next page-numbers" href="https://wp.jmstheme.com/erado/shop/page/2/">&rarr;</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('script')
    <script type="text/javascript">

        var cat_product = $('.cat-item a').attr('data-filter');
        var lang = $('input[name=lang]').val();

        $(document).ready(function(){
            getItem(cat_product, lang);
            $('.cat-item a').click(function(e){
                e.preventDefault();
                var cat_product = $(this).attr('data-filter');
                var lang = $('input[name=lang]').val();

                getItem(cat_product, lang);

            });
        });

        function getItem(cat_product, lang) {
            $('#product_html').html('')
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "post",
                url: "{{ route('client.getproductByCat') }}",
                data: {
                    cat_product: cat_product,
                    lang: lang
                }
            }).done(function (data) {

                $('#product_html').html(data.data.product_html);
            });
        }
    </script>
@endsection