@foreach($products as $product)
    <div class="item mb_20 col-lg-3 col-md-3 col-sm-4 col-xs-6 cat-slug-decor">
        <div class="item-animation">
            <div class="product-box">
                <div class="style-2 ">
                    <div class="product-thumb pr oh">
                        <a href="{{route('client.product_detail', ['cat_new' => $product->slug])}}" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="360" src="/{{$images->path}}" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="/{{$images->path}} 300w, /{{$images->path}} 600w" sizes="(max-width: 300px) 100vw, 300px" /><img width="300" height="360" src="/{{$images->path}}" class="secondary-image attachment-shop-catalog" alt="" srcset="/{{$images->path}} 300w, /{{$images->path}} 600w" sizes="(max-width: 300px) 100vw, 300px" /></a>
                    </div>
                    <div class="product-info">
                        <div class="title-product middle-xs between-xs">
                            <a href="{{route('client.product_detail', ['cat_new' => $product->slug])}}" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">{{$product->name}}</a>
                        </div>
                        <div class="middle-xs price-box">
                            <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"></span>{{$product->min_price}} {{$product->unit}}  - {{$product->max_price}} {{$product->unit}}</span></span>
                        </div>
                        <div class="product-extra">
                            <div class="woocommerce-product-details__short-description">
                                <p>Proin accumsan porta miac maximus. In dapibus mollis risus, nec mattis sem gravida quam volutpat. Donec pellentesque vulputat. Aliquam vitae felis leo sodales tincidunt lectus.</p>
                            </div>
                            <div class="add-to-cart">
                                <a href="" data-quantity="1" data-product_id="393" data-product_sku="" class="button-cart button product_type_simple add_to_cart_button ajax_add_to_cart"><i class="fa fa-spinner spinner"></i><i class="fa fa-check"></i>Add to cart<span class="tooltip">Add to cart</span></a>
                            </div>
                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-393">
                                <div class="yith-wcwl-add-button pr show">
                                    <a href="" data-product-id="393" data-product-type="simple" class="button add_to_wishlist" >
                                        <span class="text-hidden">Add to wishlist</span>
                                        <i class="fa-heart1"></i>
                                    </a>
                                    <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                </div>
                                <div class="yith-wcwl-remove-button pr yith-wcwl-wishlistaddedbrowse hide">
                                    <a class="button" href="https://wp.jmstheme.com/erado/wishlist/" data-product-id="393">
                                        <i class="fa-heart2"></i><span class="text-hidden">Remove from Wishlist</span>
                                    </a>
                                    <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                </div>
                                <div class="yith-wcwl-wishlistaddresponse"></div>
                            </div>
                            <div class="woocommerce product compare-button"><a href="https://wp.jmstheme.com/erado?action=yith-woocompare-add-product&id=393" class="compare button" data-product_id="393" rel="nofollow">Compare</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
