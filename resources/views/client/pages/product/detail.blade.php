@extends('client.layouts.main')

@section ('content')
<div class="fl-page-content">
    <div class="wc-product-detail-layout-1">
        <div class="container mt_85 mb_20 ">
            <div class="row">
                <div id="main-content" class="col-md-12 col-sm-12 col-xs-12">
                    <div class="products-links">
                        <a href="" class="previous-product"><i class="pe-7s-angle-left"></i></a><a href="single-product.php?p=posuare-condimentum/" class="next-product"><i class="pe-7s-angle-right"></i></a>
                    </div>
                    <div id="product-391" class="post-391 product type-product status-publish has-post-thumbnail product_cat-light product_tag-decor product_tag-erado product_tag-furniture product_tag-joommasters product_tag-office pa_color-orange pa_color-red pa_color-teal pa_color-yellow first instock featured shipping-taxable purchasable product-type-variable has-default-attributes">
                        <div class="wc-product-top wc-single-product-style-1 row mb_75">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 column-left">
                                <div class="single-product-thumbnail pr clearfix bottom">
                                    <div class="p-thumb images thumbnail-slider" data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "asNavFor": ".p-nav", "fade":true}'>
                                        @foreach ($images as $image)
                                            <div class="p-item woocommerce-product-gallery__image">
                                                <a href="{{ asset($image->path) }}" class="zoom" data-rel="prettyPhoto[product-gallery]">
                                                    <img width="600" height="720" 
                                                        src="{{ asset($image->path) }}" 
                                                        class="attachment-shop_single size-shop_single wp-post-image" 
                                                        alt="" title=""
                                                        data-src="{{ asset($image->path) }}" 
                                                        data-large_image="{{ asset($image->path) }}" 
                                                        data-zoom-image="{{ asset($image->path) }}" 
                                                        data-large_image_width="600" data-large_image_height="720" 
                                                        srcset="{{ asset($image->path) }} 600w, {{ asset($image->path) }} 300w" 
                                                        sizes="(max-width: 600px) 100vw, 600px" />
                                                </a>
                                            </div>
                                        @endforeach    
                                    </div>
                                    <div class="p-nav oh thumbnail-slider" data-slick='{"slidesToShow": 4,"slidesToScroll": 1,"asNavFor": ".p-thumb","arrows": false, "focusOnSelect": true,  "responsive":[{"breakpoint": 736,"settings":{"slidesToShow": 4, "vertical":false}}]}'>
                                        @foreach ($images as $image)
                                            <div>
                                                <img width="300" height="360" 
                                                    src="{{ asset($image->path) }}" 
                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" 
                                                    alt="" title="4.1" 
                                                    srcset="{{ asset($image->path) }} 300w, {{ asset($image->path) }} 600w" 
                                                    sizes="(max-width: 300px) 100vw, 300px" />
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 column-right">
                                <div class="summary entry-summary">
                                    <h1 class="product_title entry-title">
                                        @if (Session::get('locale') == 'vn')
                                            {{ $product->name_vn }}
                                        @else
                                            {{ $product->name_en }}
                                        @endif
                                    </h1>
                                    <p class="price">
                                        @if (Session::get('locale') == 'vn')
                                            <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol">&#36;</span>
                                                {{ $product->min_price_vn }}
                                            </span> 
                                                &ndash;
                                            <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol">&#36;</span>
                                                {{ $product->max_price_vn }}
                                            </span>
                                        @else
                                            <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol">&#36;</span>
                                                {{ $product->min_price_en }}
                                            </span> 
                                                &ndash;
                                            <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol">&#36;</span>
                                                {{ $product->max_price_en }}
                                            </span>
                                        @endif
                                    </p>
                                    <div class="woocommerce-product-details__short-description">
                                        @if (Session::get('locale') == 'vn')
                                            <p>{{ $product->sapo_vn }}</p>
                                        @else
                                            <p>{{ $product->sapo_en }}</p>
                                        @endif
                                    </div>
                                    <form class="variations_form cart" method="post" enctype='multipart/form-data' data-product_id="391" data-product_variations="[{&quot;attributes&quot;:{&quot;attribute_pa_color&quot;:&quot;orange&quot;},&quot;availability_html&quot;:&quot;&quot;,&quot;backorders_allowed&quot;:false,&quot;dimensions&quot;:{&quot;length&quot;:&quot;&quot;,&quot;width&quot;:&quot;&quot;,&quot;height&quot;:&quot;&quot;},&quot;dimensions_html&quot;:&quot;N\/A&quot;,&quot;display_price&quot;:100,&quot;display_regular_price&quot;:100,&quot;image&quot;:{&quot;title&quot;:&quot;4&quot;,&quot;caption&quot;:&quot;&quot;,&quot;url&quot;:&quot;https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4.jpg&quot;,&quot;alt&quot;:&quot;&quot;,&quot;src&quot;:&quot;https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4-600x720.jpg&quot;,&quot;srcset&quot;:&quot;https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4.jpg 600w, https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4-300x360.jpg 300w&quot;,&quot;sizes&quot;:&quot;(max-width: 600px) 100vw, 600px&quot;,&quot;full_src&quot;:&quot;https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4.jpg&quot;,&quot;full_src_w&quot;:600,&quot;full_src_h&quot;:720,&quot;gallery_thumbnail_src&quot;:&quot;https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4-100x100.jpg&quot;,&quot;gallery_thumbnail_src_w&quot;:100,&quot;gallery_thumbnail_src_h&quot;:100,&quot;thumb_src&quot;:&quot;https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4-300x360.jpg&quot;,&quot;thumb_src_w&quot;:300,&quot;thumb_src_h&quot;:360,&quot;src_w&quot;:600,&quot;src_h&quot;:720},&quot;image_id&quot;:&quot;1705&quot;,&quot;is_downloadable&quot;:false,&quot;is_in_stock&quot;:true,&quot;is_purchasable&quot;:true,&quot;is_sold_individually&quot;:&quot;no&quot;,&quot;is_virtual&quot;:false,&quot;max_qty&quot;:&quot;&quot;,&quot;min_qty&quot;:1,&quot;price_html&quot;:&quot;&lt;span class=\&quot;price\&quot;&gt;&lt;span class=\&quot;woocommerce-Price-amount amount\&quot;&gt;&lt;span class=\&quot;woocommerce-Price-currencySymbol\&quot;&gt;&#036;&lt;\/span&gt;100.00&lt;\/span&gt;&lt;\/span&gt;&quot;,&quot;sku&quot;:&quot;&quot;,&quot;variation_description&quot;:&quot;&quot;,&quot;variation_id&quot;:1775,&quot;variation_is_active&quot;:true,&quot;variation_is_visible&quot;:true,&quot;weight&quot;:&quot;&quot;,&quot;weight_html&quot;:&quot;N\/A&quot;},{&quot;attributes&quot;:{&quot;attribute_pa_color&quot;:&quot;red&quot;},&quot;availability_html&quot;:&quot;&quot;,&quot;backorders_allowed&quot;:false,&quot;dimensions&quot;:{&quot;length&quot;:&quot;&quot;,&quot;width&quot;:&quot;&quot;,&quot;height&quot;:&quot;&quot;},&quot;dimensions_html&quot;:&quot;N\/A&quot;,&quot;display_price&quot;:90,&quot;display_regular_price&quot;:90,&quot;image&quot;:{&quot;title&quot;:&quot;4.3&quot;,&quot;caption&quot;:&quot;&quot;,&quot;url&quot;:&quot;https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4.3.jpg&quot;,&quot;alt&quot;:&quot;&quot;,&quot;src&quot;:&quot;https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4.3-600x720.jpg&quot;,&quot;srcset&quot;:&quot;https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4.3.jpg 600w, https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4.3-300x360.jpg 300w&quot;,&quot;sizes&quot;:&quot;(max-width: 600px) 100vw, 600px&quot;,&quot;full_src&quot;:&quot;https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4.3.jpg&quot;,&quot;full_src_w&quot;:600,&quot;full_src_h&quot;:720,&quot;gallery_thumbnail_src&quot;:&quot;https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4.3-100x100.jpg&quot;,&quot;gallery_thumbnail_src_w&quot;:100,&quot;gallery_thumbnail_src_h&quot;:100,&quot;thumb_src&quot;:&quot;https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4.3-300x360.jpg&quot;,&quot;thumb_src_w&quot;:300,&quot;thumb_src_h&quot;:360,&quot;src_w&quot;:600,&quot;src_h&quot;:720},&quot;image_id&quot;:&quot;1704&quot;,&quot;is_downloadable&quot;:false,&quot;is_in_stock&quot;:true,&quot;is_purchasable&quot;:true,&quot;is_sold_individually&quot;:&quot;no&quot;,&quot;is_virtual&quot;:false,&quot;max_qty&quot;:&quot;&quot;,&quot;min_qty&quot;:1,&quot;price_html&quot;:&quot;&lt;span class=\&quot;price\&quot;&gt;&lt;span class=\&quot;woocommerce-Price-amount amount\&quot;&gt;&lt;span class=\&quot;woocommerce-Price-currencySymbol\&quot;&gt;&#036;&lt;\/span&gt;90.00&lt;\/span&gt;&lt;\/span&gt;&quot;,&quot;sku&quot;:&quot;&quot;,&quot;variation_description&quot;:&quot;&quot;,&quot;variation_id&quot;:1256,&quot;variation_is_active&quot;:true,&quot;variation_is_visible&quot;:true,&quot;weight&quot;:&quot;&quot;,&quot;weight_html&quot;:&quot;N\/A&quot;},{&quot;attributes&quot;:{&quot;attribute_pa_color&quot;:&quot;teal&quot;},&quot;availability_html&quot;:&quot;&quot;,&quot;backorders_allowed&quot;:false,&quot;dimensions&quot;:{&quot;length&quot;:&quot;&quot;,&quot;width&quot;:&quot;&quot;,&quot;height&quot;:&quot;&quot;},&quot;dimensions_html&quot;:&quot;N\/A&quot;,&quot;display_price&quot;:80,&quot;display_regular_price&quot;:80,&quot;image&quot;:{&quot;title&quot;:&quot;4.1&quot;,&quot;caption&quot;:&quot;&quot;,&quot;url&quot;:&quot;https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4.1.jpg&quot;,&quot;alt&quot;:&quot;&quot;,&quot;src&quot;:&quot;https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4.1-600x720.jpg&quot;,&quot;srcset&quot;:&quot;https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4.1.jpg 600w, https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4.1-300x360.jpg 300w&quot;,&quot;sizes&quot;:&quot;(max-width: 600px) 100vw, 600px&quot;,&quot;full_src&quot;:&quot;https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4.1.jpg&quot;,&quot;full_src_w&quot;:600,&quot;full_src_h&quot;:720,&quot;gallery_thumbnail_src&quot;:&quot;https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4.1-100x100.jpg&quot;,&quot;gallery_thumbnail_src_w&quot;:100,&quot;gallery_thumbnail_src_h&quot;:100,&quot;thumb_src&quot;:&quot;https:\/\/wp.jmstheme.com\/erado\/wp-content\/uploads\/2018\/04\/4.1-300x360.jpg&quot;,&quot;thumb_src_w&quot;:300,&quot;thumb_src_h&quot;:360,&quot;src_w&quot;:600,&quot;src_h&quot;:720},&quot;image_id&quot;:&quot;1702&quot;,&quot;is_downloadable&quot;:false,&quot;is_in_stock&quot;:true,&quot;is_purchasable&quot;:true,&quot;is_sold_individually&quot;:&quot;no&quot;,&quot;is_virtual&quot;:false,&quot;max_qty&quot;:&quot;&quot;,&quot;min_qty&quot;:1,&quot;price_html&quot;:&quot;&lt;span class=\&quot;price\&quot;&gt;&lt;span class=\&quot;woocommerce-Price-amount amount\&quot;&gt;&lt;span class=\&quot;woocommerce-Price-currencySymbol\&quot;&gt;&#036;&lt;\/span&gt;80.00&lt;\/span&gt;&lt;\/span&gt;&quot;,&quot;sku&quot;:&quot;&quot;,&quot;variation_description&quot;:&quot;&quot;,&quot;variation_id&quot;:1257,&quot;variation_is_active&quot;:true,&quot;variation_is_visible&quot;:true,&quot;weight&quot;:&quot;&quot;,&quot;weight_html&quot;:&quot;N\/A&quot;}]">
                                        <div class="single_variation_wrap" >
                                            <div class="woocommerce-variation single_variation"></div>
                                            <div class="woocommerce-variation-add-to-cart variations_button">
                                            <button type="submit" class="single_add_to_cart_button button alt">Đặt hàng</button>
                                            <input type="hidden" name="add-to-cart" value="391" />
                                            <input type="hidden" name="product_id" value="391" />
                                            <input type="hidden" name="variation_id" class="variation_id" value="0" />
                                            </div>
                                        </div>
                                        <ul class="social-ct">
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                        </ul>
                                        {{--<p class="deli-ct">
                                        <i class="fa fa-check-square-o" aria-hidden="true"></i> Security policy (edit with Customer reassurance module)<br />
                                        <i class="fa fa-truck" aria-hidden="true"></i> Delivery policy (edit with Customer reassurance module)<br />
                                        <i class="fa fa-exchange" aria-hidden="true"></i> Return policy (edit with Customer reassurance module)
                                        </p>--}}
                                        <script type="text/javascript">
                                            (function($) {
                                                "use strict";
                                            function loadImages(productId,option) {
        
                                            }
                                            $(document).ready(function(){
                                                $('.tabs.wc-tabs li').click(function() {
                                                var $indexLi = $(this).index() + 1;
                                                $('.tabs.wc-tabs li').removeClass('active');
                                                $(this).addClass('active');
                                                $('.tab-content > div').hide();
                                                $('.tab-content > div:nth-child('+$indexLi+')').show();
                                                });
                                                            var attributes = [ 'pa_color', ];
                                                    var $variation_form = $('.variations_form');
                                                    var $product_variations = $variation_form.data( 'product_variations' );
                                                    $('a.imageswatch-variation').on('click touchstart',function(event){
                                                        if($(this).hasClass('disable')) {
                                                            return;
                                                        }
                                                    var current = $(this);
                                                        if(!current.hasClass('selected')) {
                                                            var value = current.attr('option-value');
                                                            var selector_name = current.closest('div').attr('id');
                                                            if(selector_name == attributes[0]) {
                                                                $('div.imageswatch-variations a').each(function(){
                                                                    $(this).removeClass('selected');
                                                                });
                                                            }
                                                            if($("select#"+selector_name).find('option[value="'+value+'"]').length > 0) {
                                                                $(this).closest('div').children('a').each(function(){
                                                                    $(this).removeClass('selected');
                                                            $(this).removeClass('disable');
                                                                });
                                                        if(!$(this).hasClass('selected')) {
                                                                    current.addClass('selected');
        
                                                                    $("select#"+selector_name).val(value);
                                                                    $variation_form.trigger( 'wc_variation_form' );
                                                                    $variation_form.trigger( 'check_variations' );
        
                                                                }
                                                            } else {
                                                                current.addClass('disable');
                                                            }
                                                        }
        
                                                    });
                                                    $variation_form.on('wc_variation_form', function() {
                                                        $( this ).on( 'click', '.reset_variations', function( event ) {
                                                            $('div.imageswatch-variations a').each(function(){
                                                                $(this).removeClass('selected');
                                                            });
                                                            loadImages('391','null');
                                                        });
                                                    });
                                                    $variation_form.on('reset_data',function(){
                                                        $variation_form.find( '.variations select').each(function(){
                                                            if($(this).val() == '') {
                                                                var id = $(this).attr('id');
                                                                $('div#'+id+' a').removeClass('selected');
                                                            }
                                                        });
                                                    });
                                                                        $('[data-toggle="tooltip"]').tooltip();
                                                        });
                                                    } )( jQuery );
                                        </script>
                                    </form>
                                    <ul class="product_meta">
                                        {{--<li class="sku_wrapper">SKU: <span class="sku">N/A</span></li>
                                        <li class="posted_in">Category: <a href="https://wp.jmstheme.com/erado/product-category/light/" rel="tag">Light</a></li>--}}
                                        <li class="tagged_as">Tags:
                                            @foreach($product_tag as $item)
                                            <a href="" rel="tag">{{$item->name}}</a>,
                                            @endforeach
                                    </ul>
                                    <div class="addthis_inline_share_toolbox_abpr"></div>
                                </div>
                                <!-- .summary -->
                            </div>
                        </div>
                        <div class="woocommerce-tabs wc-tabs-wrapper clearfix">
                            <ul class="tabs wc-tabs" role="tablist">
                                <li class="description_tab active" id="tab-title-description" role="tab" aria-controls="tab-description">
                                    <a href="#tab-description">Description</a>
                                </li>
                                <li class="reviews_tab" id="tab-title-reviews" role="tab" aria-controls="tab-reviews">
                                    <a href="#tab-reviews">Reviews (0)</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--description entry-content wc-tab" id="tab-description" role="tabpanel" aria-labelledby="tab-title-description" style="display: block;">
                                    @if (Session::get('locale') == 'vn')
                                        {!! $product->description_vn !!}
                                    @else
                                        {!! $product->description_en !!}
                                    @endif
                                </div>
                                <div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--reviews entry-content wc-tab" id="tab-reviews" role="tabpanel" aria-labelledby="tab-title-reviews" style="display: none;">
                                    <div id="reviews" class="woocommerce-Reviews">
                                        <div id="comments">
                                        <h2 class="woocommerce-Reviews-title">Reviews</h2>
                                        <p class="woocommerce-noreviews">There are no reviews yet.</p>
                                        </div>
                                        <div id="review_form_wrapper">
                                        <div id="review_form">
                                            <div id="respond" class="comment-respond">
                                                <span id="reply-title" class="comment-reply-title">
                                                    Be the first to review &ldquo;
                                                    @if (Session::get('locale') == 'vn')
                                                        {{ $product->name_vn }}
                                                    @else 
                                                        {{ $product->name_en }}
                                                    @endif
                                                    &rdquo;
                                                </span>
                                                <form action="https://wp.jmstheme.com/erado/wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate>
                                                    <p class="comment-notes"><span id="email-notes">Your email address will not be published.</span> Required fields are marked <span class="required">*</span></p>
                                                    <div class="comment-form-rating">
                                                    <label for="rating">Your rating</label>
                                                    <select name="rating" id="rating" required>
                                                        <option value="">Rate&hellip;</option>
                                                        <option value="5">Perfect</option>
                                                        <option value="4">Good</option>
                                                        <option value="3">Average</option>
                                                        <option value="2">Not that bad</option>
                                                        <option value="1">Very poor</option>
                                                    </select>
                                                    </div>
                                                    <p class="comment-form-comment"><textarea id="comment" name="comment" cols="45" rows="8" required placeholder=" Your review * "></textarea></p>
                                                    <p class="comment-form-author"><label for="author">Name <span class="required">*</span></label> <input id="author" name="author" type="text" value="" size="30" required /></p>
                                                    <p class="comment-form-email"><label for="email">Email <span class="required">*</span></label> <input id="email" name="email" type="email" value="" size="30" required /></p>
                                                    <p class="form-submit"><input name="submit" type="submit" id="submit" class="submit" value="Submit" /> <input type='hidden' name='comment_post_ID' value='391' id='comment_post_ID' />
                                                    <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
                                                    </p>
                                                </form>
                                            </div>
                                            <!-- #respond -->
                                        </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            {{--<div class="related-product jmsproduct-box">
                            <div class="addon-title mb_40">
                                <h3>Related products</h3>
                            </div>
                            <div class="related-carousel owl-theme owl-carousel" data-carousel='{"selector":".related-carousel", "itemDesktop": 4, "itemMedium": 3, "itemTablet": 2, "itemMobile": 1, "margin":30 , "nav":true}'>

                                <div class="item">
                                    <div class="product-box">
                                        <div class="style-2 ">
                                        <div class="product-thumb pr oh">
                                            <span class="badge pa tc dib">
                                            <span class="onsale">Sale</span> </span>
                                            <a href="single-product.php?p=normann-copenhagen/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link"><img width="300" height="360" src="uploads/2018/04/21-300x360.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" srcset="uploads/2018/04/21-300x360.jpg 300w, uploads/2018/04/21.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /><img width="300" height="360" src="uploads/2018/04/23-300x360.jpg" class="secondary-image attachment-shop-catalog" alt="" srcset="uploads/2018/04/23-300x360.jpg 300w, uploads/2018/04/23.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" /></a>
                                        </div>
                                        <div class="product-info">
                                            <div class="title-product middle-xs between-xs">
                                                <a href="single-product.php?p=normann-copenhagen/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Normann Copenhagen</a>
                                            </div>
                                            <span class="product-cat db mb_3"> <a href="https://wp.jmstheme.com/erado/product-category/light/" rel="tag">Light</a></span>
                                            <div class="middle-xs price-box">
                                                <span class="price"><del><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>99.00</span></del> <ins><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>90.00</span></ins></span>
                                            </div>
                                            <div class="product-extra">
                                                <div class="woocommerce-product-details__short-description">
                                                    <p>Proin accumsan porta miac maximus. In dapibus mollis risus, nec mattis sem gravida quam volutpat. Donec pellentesque vulputat. Aliquam vitae felis leo sodales tincidunt lectus.</p>
                                                </div>
                                                <div class="add-to-cart">
                                                    <a href="https://themeforest.net/user/joommasters" data-quantity="1" data-product_id="375" data-product_sku="" class="button-cart button product_type_external"><i class="fa fa-spinner spinner"></i><i class="fa fa-check"></i>shop now<span class="tooltip">shop now</span></a>
                                                </div>
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-375">
                                                    <div class="yith-wcwl-add-button pr show">
                                                    <a href="/erado/product/mauris-vestibulum/?add_to_wishlist=375" data-product-id="375" data-product-type="external" class="button add_to_wishlist" >
                                                    <span class="text-hidden">Add to wishlist</span>
                                                    <i class="fa-heart1"></i>
                                                    </a>
                                                    <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                    </div>
                                                    <div class="yith-wcwl-remove-button pr yith-wcwl-wishlistaddedbrowse hide">
                                                    <a class="button" href="https://wp.jmstheme.com/erado/wishlist/" data-product-id="375">
                                                    <i class="fa-heart2"></i><span class="text-hidden">Remove from Wishlist</span>
                                                    </a>
                                                    <span class="ajax-loading visible-hide"><i class="fa fa-spinner spinner"></i></span>
                                                    </div>
                                                    <div class="yith-wcwl-wishlistaddresponse"></div>
                                                </div>
                                                <div class="woocommerce product compare-button"><a href="https://wp.jmstheme.com/erado?action=yith-woocompare-add-product&id=375" class="compare button" data-product_id="375" rel="nofollow">Compare</a></div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection