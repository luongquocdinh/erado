@extends('client.layouts.main')
@section('content')
    <div class="fl-page-content" style="transform: none;">
        <div class="page-heading pt_28 pb_28">
            <div class="container">
                <div class="breadcrumb"><a href="https://wp.jmstheme.com/erado" title="Home">@lang('client.home')</a> <span></span> <a href="{{route('client.recruitment')}}" title="@lang('client.recruitment')">@lang('client.recruitment')</a> <span></span>@lang('client.detail')</div>
            </div>
        </div>
        <div class="container mt_85 mb_75" style="transform: none;">
            <div class="fl-row row left-sidebar" style="transform: none;">
                <div id="main-content" class="col-xs-12">
                    <article id="post-327" class="single-post post-327 post type-post status-publish format-standard has-post-thumbnail hentry category-architecture category-furniture category-interior category-stationary category-uncategorized tag-content tag-erado tag-furniture">
                        <h1 class="post-title entry-title">{{$recruitment->title}}</h1>
                        <div class="social-meta mb_20">
                            <div class="post-meta"><span class="author">By <a href="https://wp.jmstheme.com/erado/author/admin/">{{$recruitment->author}}</a></span><span class="time updated"><a href="single-blog.php?p=template-paginated-2/">{{$recruitment->created_at}}</a></span><span class="comment-number"><a href="single-blog.php?p=template-paginated-2/#respond">0 Comments</a></span></div>
                        </div>
                        {{trim(strip_tags($recruitment->content))}}
                    </article>
                    <!-- #post-# -->
                    <div class="clearfix"></div>


                </div>
            </div>
        </div>
    </div>
@endsection