@foreach($news as $key => $new)
    <article class="item col-md-4 col-sm-6 col-xs-12 mb_40 post-{{$key}} post type-post status-publish format-standard has-post-thumbnail hentry category-architecture category-furniture category-interior category-stationary category-uncategorized tag-content tag-erado tag-furniture">
        <div class="post-inner item-animation" data-sr-id="2" style="; visibility: visible;  -webkit-transform: translateY(0) scale(1); opacity: 1;transform: translateY(0) scale(1); opacity: 1;-webkit-transition: -webkit-transform 0.7s cubic-bezier(0.6, 0.2, 0.1, 1) 0s, opacity 0.7s cubic-bezier(0.6, 0.2, 0.1, 1) 0s; transition: transform 0.7s cubic-bezier(0.6, 0.2, 0.1, 1) 0s, opacity 0.7s cubic-bezier(0.6, 0.2, 0.1, 1) 0s; ">
            <div class="post-thumbnail pr">
                <a href="{{route('client.news_detail', ['cat_new' => $new->slug])}}" class="db mb_20" rel="bookmark" title="Fusce Congue Vestibulum Tortor Suscipit">
                    <img width="1300" height="677" src="/{{$new->image}}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="Fusce Congue Vestibulum Tortor Suscipit" sizes="(max-width: 1300px) 100vw, 1300px">
                </a>
            </div>
            <div class="post-content">
                <h2 class="post-title entry-title mb_7"><a class="chp" href="" rel="bookmark">{{$new->title}}</a></h2>
                <div class="post-meta"><span class="author">By <a href="{{route('client.news_detail', ['cat_new' => $new->slug])}}">{{$new->author}}</a></span><span class="time updated"><a href="{{route('client.news_detail', ['cat_new' => $new->slug])}}">{{$new->created_at}}</a></span><span class="comment-number"><a href="single-blog.php?p=template-paginated-2/#respond">0 Comments</a></span></div>
            </div>
            <!-- .post-content -->
            <div class="post-excerpt">
                {{ $new->sapo }}
                <p class="read-more-section"><a class="btn-read-more more-link" href="{{route('client.news_detail', ['cat_new' => $new->slug])}}">@lang('client.read_more')</a></p>
            </div>
        </div>
    </article>
@endforeach