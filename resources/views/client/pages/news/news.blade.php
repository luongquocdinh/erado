@extends('client.layouts.main')

@section ('style')
    <style type="text/css" id="kc-css-general">
        .kc-off-notice{display: inline-block !important;}
        .kc-container{max-width:1222px;}
    </style>
    <link rel='stylesheet'href='{{ asset('client') }}/css/style_home.css' type='text/css' media='all' />
@endsection

@section('content')
    <input type="hidden" name="lang" value="{{$lang}}">
    <div class="fl-page-content">
        <div class="page-heading pt_50 pb_50">
            <div class="container">
                <div class="row">
                    <div class="page-heading-position tc">
                        <h2 class="page-title">@lang('client.news')</h2>
                    </div>
                    <div class="page-heading-position tc">
                        <div class="breadcrumb"><a href="https://wp.jmstheme.com/erado" title="@lang('client.news')">@lang('client.home')</a><span></span>@lang('client.news')</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container mt_85 mb_75">
            <div class="kc_clfw"></div>
            <div class="shop-filter-cat">
                <ul class="product-categories filter-grid">
                    <li class="cat-item"><a href="javascript:;" data-filter="all">All</a></li>
                    @foreach($categorys as $category)
                    <li class="cat-item"><a href="javascript:;" data-filter="{{$category->cat_slug}}">{{$category->name}}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="fl-row row ">
                <div id="main-content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="blog-layout row erado-masonry" data-masonry="{&quot;selector&quot;:&quot;.item&quot;,&quot;layoutMode&quot;:&quot;masonry&quot;,&quot;columnWidth&quot;:&quot;.item&quot;}" style="position: relative; height: 2322px;">
                        {{--post--}}

                        <div id="news_html" style="position: absolute; left: 0%; top: 0px;" ></div>
                        {{--end post--}}


                    </div>
                    </div>
                    <div class="clearfix"></div>
                    <nav class="pagination-block tc">
                        <ul class="page-numbers">
                            <li><span aria-current="page" class="page-numbers current">1</span></li>
                            <li><a class="page-numbers" href="">2</a></li>
                            <li><a class="next page-numbers" href=""><i class="fa fa-angle-right"></i></a></li>
                        </ul>
                    </nav>
                    <!-- .page-nav -->
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
<script type="text/javascript">

   $(document).ready(function(){
       var language = $('input[name=lang]').val();
       var cat_news = $('.cat-item a').attr('data-filter');
       getItem(language, cat_news);
        $('.cat-item a').click(function(e){
            e.preventDefault();
            var language = $('input[name=lang]').val();
            var cat_news = $(this).attr('data-filter');
            getItem(language, cat_news);
        });
   });
   
   function getItem(language, cat_news) {
       $('#news_html').html('')
       $.ajax({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           },
           type: "post",
           url: "{{ route('client.getNewsByCat') }}",
           data: {
               cat_news: cat_news,
               lang: language
           }
       }).done(function (data) {

           $('#news_html').html(data.data.news_html);
       });
   }
    </script>
@endsection
