@extends('client.layouts.main')

@section ('style')
    <style type="text/css" id="kc-css-general">
        .kc-off-notice{display: inline-block !important;}
        .kc-container{max-width:1222px;}
    </style>
    <link rel='stylesheet'href='{{ asset('client') }}/css/style_home.css' type='text/css' media='all' />
@endsection

@section('content')
    <div class="fl-page-content">
        <div class="page-heading pt_50 pb_50">
            <div class="container">
                <div class="row">
                    <div class="page-heading-position tc">
                        <h2 class="page-title"></h2>
                    </div>
                    <div class="page-heading-position tc">
                        <div class="breadcrumb"><a href="" title="@lang('client.about_us')">@lang('client.home')</a><span></span>@lang('client.about_us')</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container mt_85 mb_75">
            <div class="fl-row row ">
                <div id="main-content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="blog-layout row erado-masonry" data-masonry="{&quot;selector&quot;:&quot;.item&quot;,&quot;layoutMode&quot;:&quot;masonry&quot;,&quot;columnWidth&quot;:&quot;.item&quot;}" style="position: relative; height: 2322px;">
                        {{--post--}}

                        @foreach($abouts as $key => $about)
                            <article id="post-327" class="item col-md-4 col-sm-6 col-xs-12 mb_40 post-327 post type-post status-publish format-standard has-post-thumbnail hentry category-architecture category-furniture category-interior category-stationary category-uncategorized tag-content tag-erado tag-furniture" style="position: absolute; left: 0%; top: 0px;">
                                <div class="post-inner item-animation" data-sr-id="2" style="; visibility: visible;  -webkit-transform: translateY(0) scale(1); opacity: 1;transform: translateY(0) scale(1); opacity: 1;-webkit-transition: -webkit-transform 0.7s cubic-bezier(0.6, 0.2, 0.1, 1) 0s, opacity 0.7s cubic-bezier(0.6, 0.2, 0.1, 1) 0s; transition: transform 0.7s cubic-bezier(0.6, 0.2, 0.1, 1) 0s, opacity 0.7s cubic-bezier(0.6, 0.2, 0.1, 1) 0s; ">
                                    <div class="post-thumbnail pr">
                                        <a href="{{route('client.about_us_detail', ['cat_new' => $about->slug])}}" class="db mb_20" rel="bookmark" title="Fusce Congue Vestibulum Tortor Suscipit">
                                            <img width="1300" height="677" src="/{{$about->image}}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="Fusce Congue Vestibulum Tortor Suscipit" srcset="/{{$about->image}} 1300w, /{{$about->image}} 300w, /{{$about->image}} 768w, / {{$about->image}} 600w" sizes="(max-width: 1300px) 100vw, 1300px">            </a>
                                    </div>
                                    <div class="post-content">
                                        <h2 class="post-title entry-title mb_7"><a class="chp" href="{{route('client.about_us_detail', ['cat_new' => $about->slug])}}" rel="bookmark">{{$about->title}}</a></h2>
                                        <div class="post-meta"><span class="author">By <a href="{{route('client.about_us_detail', ['cat_new' => $about->slug])}}">{{$about->author}}</a></span><span class="time updated"><a href="{{route('client.about_us_detail', ['cat_new' => $about->slug])}}">{{$about->created_at}}</a></span><span class="comment-number"><a href="single-blog.php?p=template-paginated-2/#respond">0 Comments</a></span></div>
                                    </div>
                                    <!-- .post-content -->
                                    <div class="post-excerpt">
                                        {{ $about->sapo }}
                                        <p class="read-more-section"><a class="btn-read-more more-link" href="{{route('client.about_us_detail', ['cat_new' => $about->slug])}}">@lang('client.read_more')</a></p>
                                    </div>
                                </div>
                            </article>
                        @endforeach
                    {{--end post--}}
                    </div>
                    </div>
                    <div class="clearfix"></div>
                    <nav class="pagination-block tc">
                        <ul class="page-numbers">
                            <li><span aria-current="page" class="page-numbers current">1</span></li>
                            <li><a class="page-numbers" href="blog.php?page/2/?style=masonry&amp;sidebar=no&amp;column=3#038;sidebar=no&amp;column=3">2</a></li>
                            <li><a class="next page-numbers" href="blog.php?page/2/?style=masonry&amp;sidebar=no&amp;column=3#038;sidebar=no&amp;column=3"><i class="fa fa-angle-right"></i></a></li>
                        </ul>
                    </nav>
                    <!-- .page-nav -->
                </div>
            </div>
        </div>
    </div>

@endsection

