@extends('client.layouts.main')
@section('content')
    <div class="fl-page-content" style="transform: none;">
        <div class="page-heading pt_28 pb_28">
            <div class="container">
                <div class="breadcrumb"><a href="" title="Home">@lang('client.home')</a> <span></span> <a href="{{route('client.about_us')}}" title="@lang('client.about_us')">@lang('client.about_us')</a> <span></span>@lang('client.detail')</div>
            </div>
        </div>
        <div class="container mt_85 mb_75" style="transform: none;">
            <div class="fl-row row left-sidebar" style="transform: none;">
                <div id="main-content" class="col-xs-12">
                    <article id="post-327" class="single-post post-327 post type-post status-publish format-standard has-post-thumbnail hentry category-architecture category-furniture category-interior category-stationary category-uncategorized tag-content tag-erado tag-furniture">
                        <h1 class="post-title entry-title">{{$about->title}}</h1>
                        <div class="social-meta mb_20">
                            <div class="post-meta"><span class="author">By <a href="">{{$about->author}}</a></span><span class="time updated"><a href="">{{$about->created_at}}</a></span><span class="comment-number"><a href="">0 Comments</a></span></div>
                        </div>
                        {{trim(strip_tags($about->content))}}
                    </article>
                    <!-- #post-# -->
                    <div class="clearfix"></div>


                </div>
            </div>
        </div>
    </div>
@endsection