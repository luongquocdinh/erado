@extends('client.layouts.main')

@section ('style')
    <style type="text/css" id="kc-css-general">
        .kc-off-notice{display: inline-block !important;}
        .kc-container{max-width:1222px;}
        .attachment-woocommerce_thumbnail {
            max-height: 320px;
            max-width: 300px;
        }

        .owl-carousel .owl-stage {
            display: flex;
        }

        .image_product {
            max-width: 283px;
            max-height: 340px;
        }
        
    </style>
    <link rel='stylesheet'href='{{ asset('client') }}/css/style_home.css' type='text/css' media='all' />
@endsection

@section('content')
    <div class="page-content-inner ">
        <div class="page-container container">
           <div class="row fl-page ">
                <div id="main-content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt_75 mb_75">
                    <div class="kc_clfw"></div>

                    {{-- TOP PRODUCT TYPE 1 --}}
                    <section class="kc-elm kc-css-608181 kc_row">
                        <div class="kc-row-container">
                            <div class="kc-wrap-columns">
                                <div class="kc-elm kc-css-753646 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class="kc-col-container">
                                        <div class="widget widget_revslider kc-elm kc-css-605854">
                                            <link href="https://fonts.googleapis.com/css?family=Poppins:600%2C400%2C500" rel="stylesheet" property="stylesheet" type="text/css" media="all">
                                            <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
                                                <!-- START REVOLUTION SLIDER 5.4.7.4 auto mode -->
                                                <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.7.4">
                                                    <ul>
                                                        <!-- SLIDE  -->
                                                        <li data-index="rs-2" data-transition="fade,parallaxhorizontal,parallaxvertical,curtain-2,curtain-3" data-slotamount="default,default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default,default,default,default,default" data-easeout="default,default,default,default,default" data-masterspeed="300,default,default,default,default" data-thumb="uploads/2018/04/slideshow1-2-100x50.jpg" data-delay="5990" data-rotate="0,0,0,0,0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                            <!-- MAIN IMAGE -->
                                                            <img src="{{ asset('client') }}/uploads/2018/04/slideshow1-2.jpg" alt="" title="slideshow1-2" width="1720" height="750" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                            <!-- LAYERS -->

                                                            <!-- LAYER NR. 1 -->
                                                            <div class="tp-caption   tp-resizeme" id="slide-2-layer-1" data-x="['left','left','center','center']" data-hoffset="['888','888','0','-1']" data-y="['top','top','top','top']" data-voffset="['224','224','86','86']" data-fontsize="['48','48','30','25']" data-lineheight="['48','48','30','25']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":590,"speed":1000,"frame":"0","from":"z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":310,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 48px; line-height: 48px; font-weight: 600; color: #fe4f18; letter-spacing: 0px;font-family:Poppins;text-transform:capitalize;">Top Featured </div>

                                                            <!-- LAYER NR. 2 -->
                                                            <div class="tp-caption   tp-resizeme" id="slide-2-layer-2" data-x="['left','left','center','center']" data-hoffset="['888','888','0','-8']" data-y="['top','top','top','top']" data-voffset="['304','304','142','141']" data-fontsize="['50','50','35','27']" data-lineheight="['50','50','35','27']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1070,"split":"chars","splitdelay":0.05,"speed":1090,"split_direction":"forward","frame":"0","from":"x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 50px; line-height: 50px; font-weight: 600; color: #222222; letter-spacing: 0px;font-family:Poppins;">BEST WOOD CHAIR DESIGN </div>

                                                            <!-- LAYER NR. 3 -->
                                                            <div class="tp-caption   tp-resizeme" id="slide-2-layer-3" data-x="['left','left','center','center']" data-hoffset="['888','888','0','0']" data-y="['top','top','top','top']" data-voffset="['379','379','200','158']" data-fontsize="['24','24','20','16']" data-lineheight="['27','27','20','16']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','on','on','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1620,"speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"rZ:0deg;sX:0.7;sY:0.7;opacity:0;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Back.easeIn"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 24px; line-height: 27px; font-weight: 400; color: #333333; letter-spacing: 0px;font-family:Poppins;">Creative Design Everyone Wants From ERADO Store </div>

                                                            <!-- LAYER NR. 4 -->
                                                            <div class="tp-caption rev-btn " id="slide-2-layer-4" data-x="['left','left','center','center']" data-hoffset="['888','888','0','0']" data-y="['top','top','top','top']" data-voffset="['450','450','270','200']" data-fontsize="['17','17','17','12']" data-lineheight="['17','17','17','12']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":2070,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(216,52,1);bw:0px 0px 2px 0;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[13,13,12,12]" data-paddingright="[35,35,35,25]" data-paddingbottom="[11,11,12,12]" data-paddingleft="[35,35,35,25]" style="z-index: 8; white-space: nowrap; font-size: 17px; line-height: 17px; font-weight: 500; color: rgba(255,255,255,1); letter-spacing: 0px;font-family:Poppins;background-color:rgb(254,79,24);border-color:rgb(216,52,1);border-style:solid;border-width:0px 0px 2px 0px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">SHOP NOW </div>
                                                        </li>
                                                        <!-- SLIDE  -->
                                                        <li data-index="rs-3" data-transition="fade,parallaxhorizontal,parallaxvertical,curtain-2,curtain-3" data-slotamount="default,default,default,default,default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default,default,default,default,default" data-easeout="default,default,default,default,default" data-masterspeed="300,default,default,default,default" data-thumb="uploads/2018/03/slideshow1-1-100x50.jpg" data-rotate="0,0,0,0,0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                            <!-- MAIN IMAGE -->
                                                            <img src="{{ asset('client') }}/uploads/2018/03/slideshow1-1.jpg" alt="" title="slideshow1-1" width="1720" height="750" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                            <!-- LAYERS -->

                                                            <!-- LAYER NR. 5 -->
                                                            <div class="tp-caption   tp-resizeme" id="slide-3-layer-1" data-x="['left','left','center','center']" data-hoffset="['888','888','0','0']" data-y="['top','top','top','top']" data-voffset="['224','224','86','86']" data-fontsize="['48','48','30','25']" data-lineheight="['48','48','30','25']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":590,"speed":1000,"frame":"0","from":"z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power2.easeIn"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 48px; line-height: 48px; font-weight: 600; color: #fe4f18; letter-spacing: 0px;font-family:Poppins;text-transform:capitalize;">New Arrivals </div>

                                                            <!-- LAYER NR. 6 -->
                                                            <div class="tp-caption   tp-resizeme" id="slide-3-layer-2" data-x="['left','left','center','center']" data-hoffset="['888','888','0','0']" data-y="['top','top','top','top']" data-voffset="['304','304','142','135']" data-fontsize="['50','50','35','27']" data-lineheight="['50','50','35','27']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1150,"split":"chars","splitdelay":0.05,"speed":1440,"split_direction":"forward","frame":"0","from":"x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 50px; line-height: 50px; font-weight: 600; color: #222222; letter-spacing: 0px;font-family:Poppins;">BEST WOOD CHAIR DESIGN </div>

                                                            <!-- LAYER NR. 7 -->
                                                            <div class="tp-caption   tp-resizeme" id="slide-3-layer-3" data-x="['left','left','center','center']" data-hoffset="['888','888','0','0']" data-y="['top','top','top','top']" data-voffset="['386','386','200','158']" data-fontsize="['24','24','20','16']" data-lineheight="['27','27','20','16']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','on','on','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1620,"speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"rZ:0deg;sX:0.7;sY:0.7;opacity:0;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Back.easeIn"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 24px; line-height: 27px; font-weight: 400; color: #333333; letter-spacing: 0px;font-family:Poppins;">Creative Design Everyone Wants From ERADO Store </div>

                                                            <!-- LAYER NR. 8 -->
                                                            <div class="tp-caption rev-btn " id="slide-3-layer-4" data-x="['left','left','center','center']" data-hoffset="['888','888','0','0']" data-y="['top','top','top','top']" data-voffset="['450','450','270','189']" data-fontsize="['17','17','17','12']" data-lineheight="['17','17','17','12']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":2070,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(216,52,1);bw:0 0px 2px 0;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[13,13,12,12]" data-paddingright="[35,35,35,25]" data-paddingbottom="[11,11,12,12]" data-paddingleft="[35,35,35,25]" style="z-index: 8; white-space: nowrap; font-size: 17px; line-height: 17px; font-weight: 500; color: rgba(255,255,255,1); letter-spacing: 0px;font-family:Poppins;background-color:rgb(254,79,24);border-color:rgb(216,52,1);border-style:solid;border-width:0px 0px 2px 0px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">SHOP NOW </div>
                                                        </li>
                                                    </ul>
                                                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    {{-- END TOP PRODUCT TYPE 1 --}}

                    {{-- SERVICE --}}
                    <section class="kc-elm kc-css-641475 kc_row">
                        <div class="kc-row-container">
                                <div class="kc-wrap-columns">
                                    <div class="kc-elm kc-css-630117 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class="kc-col-container">
                                            <div class="kc-elm kc-css-541074" style="height: 85px; clear: both; width:100%;"></div>
                                    </div>
                                    </div>
                                </div>
                        </div>
                    </section>

                    <section class="kc-elm kc-css-663768 kc_row 2">
                        <div class="kc-row-container  kc-container  1">
                            <div class="kc-wrap-columns">
                                <div class="kc-elm kc-css-26906 kc_col-sm-4 kc_column kc_col-sm-4">
                                <div class="kc-col-container">
                                        <div class="kc-elm kc-css-271789 kc-feature-boxes kc-fb-layout-4 shipping-box">
                                            <figure class="content-image"><img src="{{ asset('client') }}/uploads/2018/03/introduct1.png" alt=""></figure>
                                            <div class="box-right">
                                            <div class="content-title">Free Shipping</div>
                                            <div class="content-desc">For on order over $199.00</div>
                                            </div>
                                        </div>
                                </div>
                                </div>
                                <div class="kc-elm kc-css-487005 kc_col-sm-4 kc_column kc_col-sm-4">
                                <div class="kc-col-container">
                                        <div class="kc-elm kc-css-588931 kc-feature-boxes kc-fb-layout-4 money-box">
                                            <figure class="content-image"><img src="{{ asset('client') }}/uploads/2018/03/introduct2.png" alt=""></figure>
                                            <div class="box-right">
                                            <div class="content-title">RETURN EXCHANGE</div>
                                            <div class="content-desc">100% money back guarantee</div>
                                            </div>
                                        </div>
                                </div>
                                </div>
                                <div class="kc-elm kc-css-439358 kc_col-sm-4 kc_column kc_col-sm-4">
                                <div class="kc-col-container">
                                        <div class="kc-elm kc-css-797632 kc-feature-boxes kc-fb-layout-4 support-box">
                                            <figure class="content-image"><img src="{{ asset('client') }}/uploads/2018/03/introduct3.png" alt=""></figure>
                                            <div class="box-right">
                                            <div class="content-title">Quality Support</div>
                                            <div class="content-desc">Alway online feedback 24/7</div>
                                            </div>
                                        </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    {{-- END SERVICE --}}

                    {{-- PRODUCT TYPE 1 --}}
                    <section class="kc-elm kc-css-570074 kc_row">
                        <div class="kc-row-container  kc-container">
                            <div class="kc-wrap-columns">
                                <div class="kc-elm kc-css-643423 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class="kc-col-container">
                                        <div class="kc-elm kc-css-655212" style="height: 75px; clear: both; width:100%;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="kc-elm kc-css-932238 kc_row">
                        <div class="kc-row-container  kc-container">
                            <div class="kc-wrap-columns">
                                <div class="kc-elm kc-css-764683 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class="kc-col-container">
                                        <div class="kc-elm kc-css-327530 jmsproduct-box effect-product-1">
                                            <div class="addon-title mb_40">
                                                <h3>Nhóm sản phẩm 1</h3>
                                                <p class="description">Include all items updated are the most customer choice of the week!</p>
                                            </div>
                                        </div> 
                                    </div>
                                </div>

                                <div class="product-carousel-80435038 owl-carousel owl-theme ">                                    
                                    @foreach ($products as $product_1)
                                        @if ($product_1->type == 1)                                                
                                            <div class="item-wrap">
                                                <div class="item product-style-1">
                                                    <div class="product-box">
                                                        <div class="style-2 ">
                                                            <div class="product-thumb pr oh">
                                                                @if (count($product_1->images))                
                                                                    <a href="single-product.php?p=condimentum-libero-dolor/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                                        <img src="{{ $product_1->images }}" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image image_product" alt="" srcset="{{ $product_1->images }} 300w, {{ $product_1->images }} 600w" sizes="(max-width: 300px) 100vw, 300px" />
                                                                        <img src="{{ $product_1->images }}" class="secondary-image attachment-shop-catalog image_product" alt="" srcset="{{ $product_1->images }} 300w, {{ $product_1->images }} 600w" sizes="(max-width: 300px) 100vw, 300px" />
                                                                    </a>            
                                                                @else
                                                                    <a href="single-product.php?p=condimentum-libero-dolor/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                                        <img src="{{ asset('client') }}/uploads/product_no_image.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image image_product" alt="" srcset="{{ asset('client') }}/uploads/product_no_image.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" />
                                                                        <img src="{{ asset('client') }}/uploads/product_no_image.jpg" class="secondary-image attachment-shop-catalog image_product" alt="" srcset="{{ asset('client') }}/uploads/product_no_image.jpg 300w, {{ asset('client') }}/uploads/product_no_image.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" />
                                                                    </a>
                                                                @endif
                                                            </div>
                                                            <div class="product-info">
                                                                <div class="title-product middle-xs between-xs">
                                                                    <a href="single-product.php?p=condimentum-libero-dolor/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                                        @if (Session::get('locale') == 'vn')
                                                                            {{ $product_1->name_vn }}
                                                                        @else
                                                                            {{ $product_1->name_en }}
                                                                        @endif
                                                                    </a>
                                                                </div>
                                                                <span class="product-cat db mb_3"> 
                                                                    <a href="https://wp.jmstheme.com/erado/product-category/furniture/" rel="tag">
                                                                        @foreach ($product_1->categories as $category)
                                                                            @if (Session::get('locale') == 'vn')
                                                                                {{ $product_1->name_vn }}
                                                                            @else
                                                                                {{ $product_1->name_en }}
                                                                            @endif
                                                                        @endforeach
                                                                    </a>
                                                                </span>
                                                                <div class="middle-xs price-box">
                                                                    <span class="price">
                                                                        <span class="woocommerce-Price-amount amount">
                                                                            <span class="woocommerce-Price-currencySymbol">&#36;</span>
                                                                            @if (Session::get('locale') == 'vn')
                                                                                {{ $product_1->min_price_vn }} - {{ $product_1->max_price_vn }} ({{ $product_1->unit_vn }})
                                                                            @else
                                                                                {{ $product_1->min_price_en }} - {{ $product_1->max_price_en }} ({{ $product_1->unit_en }})
                                                                            @endif
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                                
                                                                <div class="woocommerce-product-details__short-description">
                                                                    @if (Session::get('locale') == 'vn')
                                                                        <p>{{ $product_1->sapo_vn }}</p>
                                                                    @else
                                                                        <p>{{ $product_1->sapo_en }}</p>
                                                                    @endif
                                                                </div>                                                 
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                                
                                        @endif
                                    @endforeach                               
                                </div>
                            </div>
                            {{-- JS LIST PRODUCT 1 --}}
                            <script type="text/javascript">
                                jQuery(document).ready(function($) {
                                    var owl_product = $('.product-carousel-80435038');
                                    var rtl = false;
                                    if ($('body').hasClass('rtl')) rtl = true;

                                    owl_product.owlCarousel({
                                        responsive: {
                                        320: {
                                            items: 1,
                                            margin: 30
                                        },
                                        480: {
                                            items: 2,
                                            margin: 30
                                        },
                                        768: {
                                            items: 3,
                                            margin: 30
                                        },
                                        991: {
                                            items: 3,
                                            margin: 30
                                        },
                                        1199: {
                                            items: 4,
                                        }
                                        },
                                        rtl: rtl,
                                        margin: 20,
                                        dots: false,
                                        nav: true,
                                        autoplay: false,
                                        loop: false,
                                        autoplayTimeout: 5000,
                                        smartSpeed: 1000,
                                        navText: ['<i class="icon-arrow prev"></i>', '<i class="icon-arrow next"></i>']
                                    });
                                    $(".loadmore div.morebox").css("display", "none");
                                    $(".loadmore div.morebox").slice(0, 4).show();
                                    $("#loadMore").on('click', function(e) {
                                        e.preventDefault();
                                        $(".loadmore div.morebox:hidden").slice(0, 4).slideDown();
                                        if ($(".loadmore div.morebox:hidden").length == 0) {
                                            $("#load").fadeOut('slow');
                                        }
                                        $('html,body').animate({
                                            scrollTop: $(this).offset().top
                                        }, 1500);
                                    });
                                });
                            </script>
                            {{-- END JS LIST PRODUCT 1 --}}
                        </div>
                    </section>
                    {{-- END PRODUCT TYPE 1 --}}

                    {{------------------------------------------------------}}

                    {{-- TOP PRODUCT TYPE 2 --}}
                    <section class="kc-elm kc-elm kc-css-861768 kc_row kc_row">
                        <div class="kc-row-container">
                            <div class="kc-wrap-columns">
                                <div class="kc-elm kc-css-753646 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class="kc-col-container">
                                        <div class="widget widget_revslider kc-elm kc-css-605854">
                                            <link href="https://fonts.googleapis.com/css?family=Poppins:600%2C400%2C500" rel="stylesheet" property="stylesheet" type="text/css" media="all">
                                            <div id="rev_slider_2_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
                                                <!-- START REVOLUTION SLIDER 5.4.7.4 auto mode -->
                                                <div id="rev_slider_2_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.7.4">
                                                    <ul>
                                                        <!-- SLIDE  -->
                                                        <li data-index="rs-4" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="uploads/2018/04/slideshow3-1-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                            <!-- MAIN IMAGE -->
                                                            <img src="{{ asset('client') }}/uploads/2018/04/slideshow3-1.jpg" alt="" title="slideshow3-1" width="1920" height="950" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                            <!-- LAYERS -->
                                                            <!-- LAYER NR. 1 -->
                                                            <div class="tp-caption   tp-resizeme" id="slide-4-layer-1" data-x="['left','left','left','left']" data-hoffset="['245','245','51','24']" data-y="['top','top','top','top']" data-voffset="['328','328','123','50']" data-fontsize="['42','42','25','25']" data-lineheight="['52','52','30','25']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":540,"split":"chars","splitdelay":0.05,"speed":2000,"split_direction":"forward","frame":"0","from":"y:[-100%];z:0;rZ:35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 42px; line-height: 52px; font-weight: 600; color: #fe4f18; letter-spacing: 0px;font-family:Poppins;">Top Featured </div>
                                                            <!-- LAYER NR. 2 -->
                                                            <div class="tp-caption   tp-resizeme" id="slide-4-layer-2" data-x="['left','left','left','left']" data-hoffset="['248','248','49','24']" data-y="['top','top','top','top']" data-voffset="['412','412','169','97']" data-fontsize="['50','50','27','27']" data-lineheight="['60','60','35','27']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":780,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 50px; line-height: 60px; font-weight: 600; color: #222222; letter-spacing: 0px;font-family:Poppins;">BEST LEG ARMCHAIR DESIGN </div>
                                                            <!-- LAYER NR. 3 -->
                                                            <div class="tp-caption   tp-resizeme" id="slide-4-layer-3" data-x="['left','left','left','left']" data-hoffset="['250','250','51','27']" data-y="['top','top','top','top']" data-voffset="['505','505','215','115']" data-fontsize="['24','24','14','12']" data-lineheight="['34','34','24','20']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','on','on','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1040,"speed":1500,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 24px; line-height: 34px; font-weight: 400; color: #333333; letter-spacing: 0px;font-family:Open Sans;">Creative Design Everyone Wants From ERADO Store </div>
                                                            <!-- LAYER NR. 4 -->
                                                            <div class="tp-caption rev-btn " id="slide-4-layer-4" data-x="['left','left','left','left']" data-hoffset="['252','252','50','24']" data-y="['top','top','top','top']" data-voffset="['590','590','259','150']" data-fontsize="['17','17','13','12']" data-lineheight="['17','17','13','12']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":1310,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(216,52,1);bw:0 0 2px 0;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[13,13,12,12]" data-paddingright="[35,35,35,25]" data-paddingbottom="[11,11,12,12]" data-paddingleft="[35,35,35,25]" style="z-index: 8; white-space: nowrap; font-size: 17px; line-height: 17px; font-weight: 600; color: rgba(255,255,255,1); letter-spacing: px;font-family:Poppins;background-color:rgb(254,79,24);border-color:rgb(216,52,1);border-style:solid;border-width:0px 0px 2px 0px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">SHOP NOW </div>
                                                        </li>
                                                        <!-- SLIDE  -->
                                                        <li data-index="rs-5" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="uploads/2018/04/slideshow3-2-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                                            <!-- MAIN IMAGE -->
                                                            <img src="{{ asset('client') }}/uploads/2018/04/slideshow3-2.jpg" alt="" title="slideshow3-2" width="1920" height="950" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                            <!-- LAYERS -->
                                                            <!-- LAYER NR. 5 -->
                                                            <div class="tp-caption   tp-resizeme" id="slide-5-layer-1" data-x="['left','left','left','center']" data-hoffset="['952','952','390','0']" data-y="['top','top','top','top']" data-voffset="['246','246','108','62']" data-fontsize="['42','42','25','25']" data-lineheight="['52','52','30','25']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":540,"split":"chars","splitdelay":0.05,"speed":2000,"split_direction":"forward","frame":"0","from":"y:[-100%];z:0;rZ:35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 42px; line-height: 52px; font-weight: 600; color: #fe4f18; letter-spacing: 0px;font-family:Poppins;">New Arrivals </div>
                                                            <!-- LAYER NR. 6 -->
                                                            <div class="tp-caption   tp-resizeme" id="slide-5-layer-2" data-x="['left','left','left','center']" data-hoffset="['950','950','393','0']" data-y="['top','top','top','top']" data-voffset="['327','327','156','114']" data-fontsize="['50','50','27','27']" data-lineheight="['60','60','35','28']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":780,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 50px; line-height: 60px; font-weight: 600; color: #222222; letter-spacing: 0px;font-family:Poppins;">MAKE MORE WONDERFUL </div>
                                                            <!-- LAYER NR. 7 -->
                                                            <div class="tp-caption   tp-resizeme" id="slide-5-layer-3" data-x="['left','left','left','left']" data-hoffset="['952','952','393','164']" data-y="['top','top','top','top']" data-voffset="['416','416','206','122']" data-fontsize="['24','24','14','12']" data-lineheight="['34','34','24','20']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','on','on','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1040,"speed":1500,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 24px; line-height: 34px; font-weight: 400; color: #333333; letter-spacing: 0px;font-family:Open Sans;">Creative Design Everyone Wants From ERADO Store </div>
                                                            <!-- LAYER NR. 8 -->
                                                            <div class="tp-caption rev-btn " id="slide-5-layer-4" data-x="['left','left','left','center']" data-hoffset="['954','954','391','0']" data-y="['top','top','top','top']" data-voffset="['495','495','252','166']" data-fontsize="['17','17','13','12']" data-lineheight="['17','17','13','12']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":1310,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(216,52,1);bw:0 0px 2px 0;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[13,13,12,12]" data-paddingright="[35,35,35,25]" data-paddingbottom="[11,11,12,12]" data-paddingleft="[35,35,35,25]" style="z-index: 8; white-space: nowrap; font-size: 17px; line-height: 17px; font-weight: 600; color: rgba(255,255,255,1); letter-spacing: px;font-family:Poppins;background-color:rgb(254,79,24);border-color:rgb(216,52,1);border-style:solid;border-width:0px 0px 2px 0px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">SHOP NOW </div>
                                                        </li>
                                                    </ul>
                                                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    {{-- END TOP PRODUCT TYPE 2 --}}

                    {{-- PRODUCT TYPE 2 --}}
                    <section class="kc-elm kc-css-570074 kc_row">
                        <div class="kc-row-container  kc-container">
                            <div class="kc-wrap-columns">
                                <div class="kc-elm kc-css-643423 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class="kc-col-container">
                                        <div class="kc-elm kc-css-655212" style="height: 75px; clear: both; width:100%;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="kc-elm kc-css-932238 kc_row">
                        <div class="kc-row-container  kc-container">
                            <div class="kc-wrap-columns">
                                <div class="kc-elm kc-css-764683 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class="kc-col-container">
                                        <div class="kc-elm kc-css-327530 jmsproduct-box effect-product-1">
                                            <div class="addon-title mb_40">
                                                <h3>Nhóm sản phẩm 2</h3>
                                                <p class="description">Include all items updated are the most customer choice of the week!</p>
                                            </div>
                                        </div> 
                                    </div>
                                </div>

                                <div class="product-carousel-2 owl-carousel owl-theme ">
                                    @foreach ($products as $product_2)
                                        @if ($product_2->type == 2)
                                            <div class="item-wrap">
                                                <div class="item product-style-1">
                                                    <div class="product-box">
                                                        <div class="style-2 ">
                                                            <div class="product-thumb pr oh">
                                                                @if (count($product_2->images))                
                                                                    <a href="single-product.php?p=condimentum-libero-dolor/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                                        <img src="{{ $product_2->images }}" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image image_product" alt="" srcset="{{ $product_2->images }} 300w, {{ $product_2->images }} 600w" sizes="(max-width: 300px) 100vw, 300px" />
                                                                        <img src="{{ $product_2->images }}" class="secondary-image attachment-shop-catalog image_product" alt="" srcset="{{ $product_2->images }} 300w, {{ $product_2->images }} 600w" sizes="(max-width: 300px) 100vw, 300px" />
                                                                    </a>           
                                                                @else
                                                                    <a href="single-product.php?p=condimentum-libero-dolor/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                                        <img src="{{ asset('client') }}/uploads/product_no_image.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image image_product" alt="" srcset="{{ asset('client') }}/uploads/product_no_image.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" />
                                                                    </a>
                                                                @endif
                                                            </div>
                                                            <div class="product-info">
                                                                <div class="title-product middle-xs between-xs">
                                                                    <a href="single-product.php?p=condimentum-libero-dolor/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                                        @if (Session::get('locale') == 'vn')
                                                                            {{ $product_2->name_vn }}
                                                                        @else
                                                                            {{ $product_2->name_en }}
                                                                        @endif
                                                                    </a>
                                                                </div>
                                                                <span class="product-cat db mb_3"> 
                                                                    <a href="https://wp.jmstheme.com/erado/product-category/furniture/" rel="tag">
                                                                        @foreach ($product_2->categories as $category)
                                                                            @if (Session::get('locale') == 'vn')
                                                                                {{ $product_2->name_vn }}
                                                                            @else
                                                                                {{ $product_2->name_en }}
                                                                            @endif
                                                                        @endforeach
                                                                    </a>
                                                                </span>
                                                                <div class="middle-xs price-box">
                                                                    <span class="price">
                                                                        <span class="woocommerce-Price-amount amount">
                                                                            <span class="woocommerce-Price-currencySymbol">&#36;</span>
                                                                            @if (Session::get('locale') == 'vn')
                                                                                {{ $product_2->min_price_vn }} - {{ $product_2->max_price_vn }} ({{ $product_2->unit_vn }})
                                                                            @else
                                                                                {{ $product_2->min_price_en }} - {{ $product_2->max_price_en }} ({{ $product_2->unit_en }})
                                                                            @endif
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                                
                                                                <div class="woocommerce-product-details__short-description">
                                                                    @if (Session::get('locale') == 'vn')
                                                                        <p>{{ $product_2->sapo_vn }}</p>
                                                                    @else
                                                                        <p>{{ $product_2->sapo_en }}</p>
                                                                    @endif
                                                                </div>                                                 
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>    
                                        @endif
                                    @endforeach 
                                </div>
                            </div>
                        </div>
                    </section>
                    {{-- END PRODUCT TYPE 2 --}}

                    {{------------------------------------------------------}}

                    {{-- TOP PRODUCT TYPE 3 --}}
                    <section class="kc-elm kc-css-861768 kc_row">
                        <div id="rev_slider_7_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
                            <!-- START REVOLUTION SLIDER 5.4.7.4 auto mode -->
                            <div id="rev_slider_7_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.7.4">
                                <ul>
                                    <!-- SLIDE  -->
                                    <li data-index="rs-16" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="https://eradocdn-cff8.kxcdn.com/wp-content/uploads/2018/04/slideshow8-1-1-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                        <!-- MAIN IMAGE -->
                                        <img src="https://eradocdn-cff8.kxcdn.com/wp-content/uploads/2018/04/slideshow8-1-1.jpg" alt="" title="slideshow8-1" width="1920" height="880" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                        <!-- LAYERS -->
                        
                                        <!-- LAYER NR. 1 -->
                                        <div class="tp-caption   tp-resizeme" id="slide-16-layer-1" data-x="['center','center','center','center']" data-hoffset="['150','150','70','-3']" data-y="['top','top','top','top']" data-voffset="['254','254','114','86']" data-fontsize="['56','56','25','20']" data-lineheight="['60','60','30','27']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":540,"split":"chars","splitdelay":0.05,"speed":2000,"split_direction":"forward","frame":"0","from":"y:[-100%];z:0;rZ:35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 56px; line-height: 60px; font-weight: 600; color: #ffffff; letter-spacing: 0px;font-family:Poppins;">New Arrivals </div>
                        
                                        <!-- LAYER NR. 2 -->
                                        <div class="tp-caption   tp-resizeme" id="slide-16-layer-2" data-x="['center','center','center','center']" data-hoffset="['150','150','70','-2']" data-y="['middle','middle','top','top']" data-voffset="['-61','-61','163','131']" data-fontsize="['68','68','27','22']" data-lineheight="['80','80','35','28']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":780,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 68px; line-height: 80px; font-weight: 600; color: #ffffff; letter-spacing: 0px;font-family:Poppins;">MAKE MORE WONDERFUL </div>
                        
                                        <!-- LAYER NR. 3 -->
                                        <div class="tp-caption   tp-resizeme" id="slide-16-layer-3" data-x="['center','center','center','center']" data-hoffset="['150','150','70','0']" data-y="['top','top','top','top']" data-voffset="['439','439','213','152']" data-fontsize="['30','30','14','12']" data-lineheight="['40','40','24','20']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','on','on','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1040,"speed":1500,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 30px; line-height: 40px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Open Sans;">Creative Design Everyone Wants From ERADO Store </div>
                        
                                        <!-- LAYER NR. 4 -->
                                        <div class="tp-caption rev-btn " id="slide-16-layer-4" data-x="['center','center','center','center']" data-hoffset="['150','150','70','-2']" data-y="['top','top','top','top']" data-voffset="['517','517','260','179']" data-fontsize="['17','17','13','12']" data-lineheight="['17','17','13','12']" data-color="['rgba(255,255,255,1)','rgba(255,255,255,1)','rgba(255,255,255,1)','rgb(255,255,255)']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":1310,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(216,52,1);bw:0 0 2px 0;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[13,13,12,12]" data-paddingright="[25,25,25,25]" data-paddingbottom="[11,11,12,12]" data-paddingleft="[25,25,25,25]" style="z-index: 8; white-space: nowrap; font-size: 17px; line-height: 17px; font-weight: 600; color: rgba(255,255,255,1); letter-spacing: px;font-family:Poppins;background-color:rgb(254,79,24);border-color:rgb(216,52,1);border-style:solid;border-width:0px 0px 2px 0px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">SHOP NOW </div>
                                    </li>
                                    <!-- SLIDE  -->
                                    <li data-index="rs-18" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="https://eradocdn-cff8.kxcdn.com/wp-content/uploads/2018/04/slideshow8-2-1-100x50.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                        <!-- MAIN IMAGE -->
                                        <img src="https://eradocdn-cff8.kxcdn.com/wp-content/uploads/2018/04/slideshow8-2-1.jpg" alt="" title="slideshow8-2" width="1920" height="880" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                        <!-- LAYERS -->
                        
                                        <!-- LAYER NR. 5 -->
                                        <div class="tp-caption   tp-resizeme" id="slide-18-layer-1" data-x="['center','center','center','center']" data-hoffset="['-350','-350','-170','0']" data-y="['top','top','top','top']" data-voffset="['254','254','114','68']" data-fontsize="['56','56','25','20']" data-lineheight="['60','60','30','27']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":540,"split":"chars","splitdelay":0.05,"speed":2000,"split_direction":"forward","frame":"0","from":"y:[-100%];z:0;rZ:35deg;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 56px; line-height: 60px; font-weight: 600; color: #ffffff; letter-spacing: 0px;font-family:Poppins;">Top Featured </div>
                        
                                        <!-- LAYER NR. 6 -->
                                        <div class="tp-caption   tp-resizeme" id="slide-18-layer-2" data-x="['center','center','center','center']" data-hoffset="['-350','-350','-170','-1']" data-y="['middle','middle','top','top']" data-voffset="['-61','-61','163','117']" data-fontsize="['68','68','27','22']" data-lineheight="['80','80','35','28']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":780,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6; white-space: nowrap; font-size: 68px; line-height: 80px; font-weight: 600; color: #ffffff; letter-spacing: 0px;font-family:Poppins;">BEST LEG ARMCHAIR DESIGN </div>
                        
                                        <!-- LAYER NR. 7 -->
                                        <div class="tp-caption   tp-resizeme" id="slide-18-layer-3" data-x="['center','center','center','center']" data-hoffset="['-350','-350','-170','0']" data-y="['top','top','top','top']" data-voffset="['439','439','213','144']" data-fontsize="['30','30','14','12']" data-lineheight="['40','40','24','20']" data-width="none" data-height="none" data-whitespace="nowrap" data-visibility="['on','on','on','off']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":1040,"speed":1500,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 30px; line-height: 40px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Open Sans;">Creative Design Everyone Wants From ERADO Store </div>
                        
                                        <!-- LAYER NR. 8 -->
                                        <div class="tp-caption rev-btn " id="slide-18-layer-4" data-x="['center','center','center','center']" data-hoffset="['-350','-350','-170','0']" data-y="['top','top','top','top']" data-voffset="['517','517','260','180']" data-fontsize="['17','17','13','12']" data-lineheight="['17','17','13','12']" data-color="['rgba(255,255,255,1)','rgba(255,255,255,1)','rgba(255,255,255,1)','rgb(255,255,255)']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="button" data-responsive_offset="on" data-responsive="off" data-frames='[{"delay":1310,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(216,52,1);bw:0 0 2px 0;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[13,13,12,12]" data-paddingright="[25,25,25,25]" data-paddingbottom="[11,11,12,12]" data-paddingleft="[25,25,25,25]" style="z-index: 8; white-space: nowrap; font-size: 17px; line-height: 17px; font-weight: 600; color: rgba(255,255,255,1); letter-spacing: px;font-family:Poppins;background-color:rgb(254,79,24);border-color:rgb(216,52,1);border-style:solid;border-width:0px 0px 2px 0px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">SHOP NOW </div>
                                    </li>
                                </ul>
                                <script>
                                    var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                    var htmlDivCss = "";
                                    if (htmlDiv) {
                                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                    } else {
                                        var htmlDiv = document.createElement("div");
                                        htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                        document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                    }
                                </script>
                                <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                            </div>
                            <script>
                                var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                var htmlDivCss = "";
                                if (htmlDiv) {
                                    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                } else {
                                    var htmlDiv = document.createElement("div");
                                    htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                    document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                }
                            </script>
                            <script type="text/javascript">
                                if (setREVStartSize !== undefined) setREVStartSize({
                                    c: '#rev_slider_7_1',
                                    responsiveLevels: [1240, 1240, 778, 480],
                                    gridwidth: [1920, 1920, 778, 480],
                                    gridheight: [880, 880, 400, 320],
                                    sliderLayout: 'auto'
                                });
                        
                                var revapi7,
                                    tpj;
                                (function() {
                                    if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded", onLoad);
                                    else onLoad();
                        
                                    function onLoad() {
                                        if (tpj === undefined) {
                                            tpj = jQuery;
                                            if ("off" == "on") tpj.noConflict();
                                        }
                                        if (tpj("#rev_slider_7_1").revolution == undefined) {
                                            revslider_showDoubleJqueryError("#rev_slider_7_1");
                                        } else {
                                            revapi7 = tpj("#rev_slider_7_1").show().revolution({
                                                sliderType: "standard",
                                                jsFileLocation: "https://eradocdn-cff8.kxcdn.com/wp-content/plugins/revslider/public/assets/js/",
                                                sliderLayout: "auto",
                                                dottedOverlay: "none",
                                                delay: 9000,
                                                navigation: {
                                                    keyboardNavigation: "off",
                                                    keyboard_direction: "horizontal",
                                                    mouseScrollNavigation: "off",
                                                    mouseScrollReverse: "default",
                                                    onHoverStop: "off",
                                                    bullets: {
                                                        enable: true,
                                                        hide_onmobile: false,
                                                        style: "erado_bullet",
                                                        hide_onleave: false,
                                                        direction: "horizontal",
                                                        h_align: "center",
                                                        v_align: "bottom",
                                                        h_offset: 0,
                                                        v_offset: 20,
                                                        space: 5,
                                                        tmp: '<span class="tp-bullet-inner"></span>'
                                                    }
                                                },
                                                responsiveLevels: [1240, 1240, 778, 480],
                                                visibilityLevels: [1240, 1240, 778, 480],
                                                gridwidth: [1920, 1920, 778, 480],
                                                gridheight: [880, 880, 400, 320],
                                                lazyType: "none",
                                                shadow: 0,
                                                spinner: "spinner0",
                                                stopLoop: "off",
                                                stopAfterLoops: -1,
                                                stopAtSlide: -1,
                                                shuffle: "off",
                                                autoHeight: "off",
                                                disableProgressBar: "on",
                                                hideThumbsOnMobile: "off",
                                                hideSliderAtLimit: 0,
                                                hideCaptionAtLimit: 0,
                                                hideAllCaptionAtLilmit: 0,
                                                debugMode: false,
                                                fallbacks: {
                                                    simplifyAll: "off",
                                                    nextSlideOnWindowFocus: "off",
                                                    disableFocusListener: false,
                                                }
                                            });
                                        }; /* END OF revapi call */
                        
                                    }; /* END OF ON LOAD FUNCTION */
                                }()); /* END OF WRAPPING FUNCTION */
                            </script>
                            <script>
                                var htmlDivCss = unescape("%23rev_slider_7_1%20.erado_bullet%20.tp-bullet%7B%0A%20%20border-radius%3A%2050%25%3B%0A%20%20background%3Atransparent%3B%0A%20%20width%3A14px%3B%0A%20%20height%3A14px%3B%0A%7D%0A%23rev_slider_7_1%20.erado_bullet%20.tp-bullet%3Abefore%7B%0A%20%20%20%20content%3A%20%27%27%3B%0A%20%20%20%20position%3A%20absolute%3B%0A%20%20%20%20width%3A%208px%3B%0A%20%20%20%20height%3A%208px%3B%0A%20%20%20%20top%3A%2050%25%3B%0A%20%20%20%20left%3A%2050%25%3B%0A%20%20%20%20margin-top%3A%20-4px%3B%0A%20%20%20%20margin-left%3A%20-4px%3B%0A%20%20%20%20display%3A%20inline-block%3B%0A%20%20%20%20border-radius%3A%20100%25%3B%0A%20%20%20%20background-color%3A%20rgb%28254%2C%2079%2C%2024%29%3B%0A%7D%0A%23rev_slider_7_1%20.erado_bullet%20.tp-bullet.selected%2C%0A%23rev_slider_7_1%20.erado_bullet%20.tp-bullet%3Ahover%20%7B%0A%20%20box-shadow%3A%200%200%200%202px%20rgba%28254%2C%2079%2C%2024%2C1%29%3B%0A%20%20border%3Anone%3B%0A%20%20border-radius%3A%2050%25%3B%0A%20%20background%3Atransparent%3B%0A%7D%0A%0A%23rev_slider_7_1%20.erado_bullet%20.tp-bullet.selected%20.tp-bullet-inner%2C%0A%23rev_slider_7_1%20.erado_bullet%20.tp-bullet%3Ahover%20.tp-bullet-inner%7B%0A%20transform%3A%20scale%280.4%29%3B%0A%20-webkit-transform%3A%20scale%280.4%29%3B%0A%20background-color%3Argb%28254%2C%2079%2C%2024%29%3B%0A%7D%0A");
                                var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                if (htmlDiv) {
                                    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                } else {
                                    var htmlDiv = document.createElement('div');
                                    htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                    document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                }
                            </script>
                        </div><!-- END REVOLUTION SLIDER --> 
                    </section>
                    {{-- END TOP PRODUCT TYPE 3 --}}

                    {{-- PRODUCT TYPE 3 --}}
                    <section class="kc-elm kc-css-252514 kc_row">
                        <div class="kc-row-container  kc-container">
                                <div class="kc-wrap-columns">
                                    <div class="kc-elm kc-css-663311 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class="kc-col-container">
                                            <div class="kc-elm kc-css-940748" style="height: 15px; clear: both; width:100%;"></div>
                                    </div>
                                    </div>
                                </div>
                        </div>
                    </section>

                    <section class="kc-elm kc-css-512235 kc_row">
                        <div class="kc-row-container  kc-container">
                            <div class="kc-wrap-columns">
                                {{-- <div class="kc-elm kc-css-987848 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class="kc-col-container"> --}}
                                        <div class="kc-elm kc-css-574328 jmsproduct-box effect-product-14">
                                            <div class="addon-title mb_40">
                                                <h3>Nhóm sản phẩm 3</h3>
                                                <p class="description">Our collection hover on a look you like to shop the items featured</p>
                                            </div>
                                        </div>

                                        <div class="product-carousel-2 owl-carousel owl-theme ">                                          
                                            @foreach ($products as $product_3)
                                                @if ($product_3->type == 3)
                                                    <div class="item-wrap">
                                                        <div class="item product-style-1">
                                                            <div class="product-box">
                                                                <div class="style-2 ">
                                                                    <div class="product-thumb pr oh">
                                                                        @if (count($product_3->images))                
                                                                            <a href="single-product.php?p=condimentum-libero-dolor/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                                                <img src="{{ $product_3->images }}" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image image_product" alt="" srcset="{{ $product_3->images }} 300w, {{ $product_3->images }} 600w" sizes="(max-width: 300px) 100vw, 300px" />
                                                                                <img src="{{ $product_3->images }}" class="secondary-image attachment-shop-catalog image_product" alt="" srcset="{{ $product_3->images }} 300w, {{ $product_3->images }} 600w" sizes="(max-width: 300px) 100vw, 300px" />
                                                                            </a>           
                                                                        @else
                                                                            <a href="single-product.php?p=condimentum-libero-dolor/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                                                <img src="{{ asset('client') }}/uploads/product_no_image.jpg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image image_product" alt="" srcset="{{ asset('client') }}/uploads/product_no_image.jpg 600w" sizes="(max-width: 300px) 100vw, 300px" />
                                                                            </a>
                                                                        @endif
                                                                    </div>
                                                                    <div class="product-info">
                                                                        <div class="title-product middle-xs between-xs">
                                                                            <a href="single-product.php?p=condimentum-libero-dolor/" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                                                                @if (Session::get('locale') == 'vn')
                                                                                    {{ $product_3->name_vn }}
                                                                                @else
                                                                                    {{ $product_3->name_en }}
                                                                                @endif
                                                                            </a>
                                                                        </div>
                                                                        <span class="product-cat db mb_3"> 
                                                                            <a href="https://wp.jmstheme.com/erado/product-category/furniture/" rel="tag">
                                                                                @foreach ($product_3->categories as $category)
                                                                                    @if (Session::get('locale') == 'vn')
                                                                                        {{ $product_3->name_vn }}
                                                                                    @else
                                                                                        {{ $product_3->name_en }}
                                                                                    @endif
                                                                                @endforeach
                                                                            </a>
                                                                        </span>
                                                                        <div class="middle-xs price-box">
                                                                            <span class="price">
                                                                                <span class="woocommerce-Price-amount amount">
                                                                                    <span class="woocommerce-Price-currencySymbol">&#36;</span>
                                                                                    @if (Session::get('locale') == 'vn')
                                                                                        {{ $product_3->min_price_vn }} - {{ $product_3->max_price_vn }} ({{ $product_3->unit_vn }})
                                                                                    @else
                                                                                        {{ $product_3->min_price_en }} - {{ $product_3->max_price_en }} ({{ $product_3->unit_en }})
                                                                                    @endif
                                                                                </span>
                                                                            </span>
                                                                        </div>
                                                                        
                                                                        <div class="woocommerce-product-details__short-description">
                                                                            @if (Session::get('locale') == 'vn')
                                                                                <p>{{ $product_3->sapo_vn }}</p>
                                                                            @else
                                                                                <p>{{ $product_3->sapo_en }}</p>
                                                                            @endif
                                                                        </div>                                                 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach   
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        jQuery(document).ready(function($) {
                                            var owl_product = $('.product-carousel-2');
                                            var rtl = false;
                                            if ($('body').hasClass('rtl')) rtl = true;

                                            owl_product.owlCarousel({
                                                    responsive: {
                                                        320: {
                                                        items: 1,
                                                        margin: 30
                                                        },
                                                        480: {
                                                        items: 2,
                                                        margin: 30
                                                        },
                                                        768: {
                                                        items: 2,
                                                        margin: 30
                                                        },
                                                        991: {
                                                        items: 3,
                                                        margin: 30
                                                        },
                                                        1199: {
                                                        items: 4,
                                                        }
                                                    },
                                                    rtl: rtl,
                                                    margin: 20,
                                                    dots: false,
                                                    nav: true,
                                                    autoplay: false,
                                                    loop: false,
                                                    autoplayTimeout: 5000,
                                                    smartSpeed: 1000,
                                                    navText: ['<i class="icon-arrow prev"></i>', '<i class="icon-arrow next"></i>']
                                            });
                                            $(".loadmore div.morebox").css("display", "none");
                                            $(".loadmore div.morebox").slice(0, 4).show();
                                            $("#loadMore").on('click', function(e) {
                                                    e.preventDefault();
                                                    $(".loadmore div.morebox:hidden").slice(0, 4).slideDown();
                                                    if ($(".loadmore div.morebox:hidden").length == 0) {
                                                        $("#load").fadeOut('slow');
                                                    }
                                                    $('html,body').animate({
                                                        scrollTop: $(this).offset().top
                                                    }, 1500);
                                            });
                                        });
                                    </script>
                                {{-- </div>
                            </div> --}}
                        </div>
                    </section>
                    {{-- END PRODUCT TYPE 3 --}}

                    {{-- PARTNER --}}
                    <section class="kc-elm kc-css-327484 kc_row">
                        <div class="kc-row-container  kc-container">
                            <div class="kc-wrap-columns">
                                <div class="kc-elm kc-css-146750 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class="kc-col-container">
                                        <div id="jmsbrand-box" class="kc-elm kc-css-32123">
                                            @foreach ($partner as $brand)
                                                <div class="owl-item cloned" style="width: 173.667px; margin-right: 30px;">
                                                    <div class="item tc">
                                                        <img src="{{ $brand->icon }}" alt="{{ $brand->name }}" class="img-responsive"/>
                                                    </div>         
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="kc-elm kc-css-235494 kc_row">
                        <div class="kc-row-container  kc-container">
                                <div class="kc-wrap-columns">
                                    <div class="kc-elm kc-css-247386 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class="kc-col-container">
                                            <div class="kc-elm kc-css-539532" style="height: 75px; clear: both; width:100%;"></div>
                                    </div>
                                    </div>
                                </div>
                        </div>
                    </section>
                    {{-- END PARTNER --}}

                    {{-- NEWS --}}
                    <section class="kc-elm kc-css-114363 kc_row">
                        <div class="kc-row-container  kc-container">
                            <div class="kc-wrap-columns">
                                <div class="kc-elm kc-css-525995 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class="kc-col-container">
                                        <div id="jmsblog-box" class="kc-elm kc-css-663070 template-1">
                                            <div class="addon-title mb_40">
                                                <h3>Tin Tức</h3>
                                                <p class="description">All our post are the latest updates from furniture and decor world</p>
                                            </div>
                                            <div class="blog-carousel-1199695738 owl-carousel owl-theme">
                                                @foreach ($news as $article)
                                                    <div class="item">
                                                        <div class="blog-box">
                                                            <div class="post-thumbnail pr oh effect-1"> 
                                                                <a href="#">
                                                                    <img src="{{ $article->images }}" style="max-height: 220px;" width="550"/>
                                                                </a>
                                                            </div>

                                                            <div class="post-info">
                                                                <h2 class="post-title">
                                                                    <a href="#" rel="bookmark">
                                                                        @if (Session::get('locate') == 'vn')
                                                                            {{ $article->title_vn }}
                                                                        @else
                                                                            {{ $article->title_en }}
                                                                        @endif
                                                                    </a>
                                                                </h2>

                                                                <ul class="post-meta">
                                                                    <li><i class="blog-calendar"></i><span>{{ date('F j, Y', $article->created_at) }}</span></li>
                                                                    <li>
                                                                        <i class="blog-eye"></i>
                                                                        <a href="#">
                                                                            {{ $article->number_comment }} comments 
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>

                                        </div>
                                        <script type="text/javascript">
                                            jQuery(document).ready(function($) {
                                                var rtl = false;
                                                if ($('body').hasClass('rtl')) rtl = true;

                                                $('.blog-carousel-1199695738').owlCarousel({
                                                    responsive: {
                                                        320: {
                                                            items: 1,
                                                            margin: 30
                                                        },
                                                        480: {
                                                            items: 1,
                                                            margin: 30
                                                        },
                                                        768: {
                                                            items: 2,
                                                            margin: 30
                                                        },
                                                        991: {
                                                            items: 3,
                                                            margin: 30
                                                        },
                                                        1199: {
                                                            items: 3,
                                                        }
                                                    },
                                                    rtl: rtl,
                                                    margin: 20,
                                                    dots: false,
                                                    nav: true,
                                                    autoplay: false,
                                                    loop: false,
                                                    autoplayTimeout: 5000,
                                                    smartSpeed: 1000,
                                                    navText: ['<i class="icon-arrow prev"></i>', '<i class="icon-arrow next"></i>']
                                                });
                                            });
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="kc-elm kc-css-235494 kc_row">
                        <div class="kc-row-container  kc-container">
                            <div class="kc-wrap-columns">
                                <div class="kc-elm kc-css-247386 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class="kc-col-container">
                                        <div class="kc-elm kc-css-539532" style="height: 75px; clear: both; width:100%;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="kc-elm kc-css-474368 kc_row">
                        <div class="kc-row-container  kc-container">
                            <div class="kc-wrap-columns">
                                <div class="kc-elm kc-css-443240 kc_col-sm-12 kc_column kc_col-sm-12">
                                    <div class="kc-col-container"></div>
                                </div>
                            </div>
                        </div>
                    </section>
                    {{-- STOP NEWS --}}
                </div>
           </div>
        </div>
    </div>
@endsection

@section ('script')
    <script>
        var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
        var htmlDivCss = "";
        if (htmlDiv) {
            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
        } else {
            var htmlDiv = document.createElement("div");
            htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
            document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
        }
    </script>
    <script>
        var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
        var htmlDivCss = "";
        if (htmlDiv) {
            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
        } else {
            var htmlDiv = document.createElement("div");
            htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
            document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
        }
    </script>
    <script type="text/javascript">
        if (setREVStartSize !== undefined) setREVStartSize({
            c: '#rev_slider_1_1',
            responsiveLevels: [1240, 1240, 778, 480],
            gridwidth: [1720, 1720, 778, 480],
            gridheight: [750, 750, 450, 350],
            sliderLayout: 'auto'
        });

        var revapi1,
            tpj;
        (function() {
            if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded", onLoad);
            else onLoad();

            function onLoad() {
                if (tpj === undefined) {
                    tpj = jQuery;
                    if ("off" == "on") tpj.noConflict();
                }
                if (tpj("#rev_slider_1_1").revolution == undefined) {
                    revslider_showDoubleJqueryError("#rev_slider_1_1");
                } else {
                    revapi1 = tpj("#rev_slider_1_1").show().revolution({
                        sliderType: "standard",
                        jsFileLocation: "https://eradocdn-cff8.kxcdn.com/wp-content/plugins/revslider/public/assets/js/",
                        sliderLayout: "auto",
                        dottedOverlay: "none",
                        delay: 9000,
                        navigation: {
                            keyboardNavigation: "off",
                            keyboard_direction: "horizontal",
                            mouseScrollNavigation: "off",
                            mouseScrollReverse: "default",
                            onHoverStop: "off",
                            bullets: {
                                enable: true,
                                hide_onmobile: false,
                                style: "erado_bullet",
                                hide_onleave: false,
                                direction: "horizontal",
                                h_align: "center",
                                v_align: "bottom",
                                h_offset: 0,
                                v_offset: 20,
                                space: 5,
                                tmp: '<span class="tp-bullet-inner"></span>'
                            }
                        },
                        responsiveLevels: [1240, 1240, 778, 480],
                        visibilityLevels: [1240, 1240, 778, 480],
                        gridwidth: [1720, 1720, 778, 480],
                        gridheight: [750, 750, 450, 350],
                        lazyType: "none",
                        shadow: 0,
                        spinner: "spinner0",
                        stopLoop: "off",
                        stopAfterLoops: -1,
                        stopAtSlide: -1,
                        shuffle: "off",
                        autoHeight: "off",
                        disableProgressBar: "on",
                        hideThumbsOnMobile: "off",
                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        debugMode: false,
                        fallbacks: {
                            simplifyAll: "off",
                            nextSlideOnWindowFocus: "off",
                            disableFocusListener: false,
                        }
                    });
                }; /* END OF revapi call */

            }; /* END OF ON LOAD FUNCTION */
        }()); /* END OF WRAPPING FUNCTION */
    </script>
    <script>
        var htmlDivCss = unescape("%23rev_slider_1_1%20.erado_bullet%20.tp-bullet%7B%0A%20%20border-radius%3A%2050%25%3B%0A%20%20background%3Atransparent%3B%0A%20%20width%3A14px%3B%0A%20%20height%3A14px%3B%0A%7D%0A%23rev_slider_1_1%20.erado_bullet%20.tp-bullet%3Abefore%7B%0A%20%20%20%20content%3A%20%27%27%3B%0A%20%20%20%20position%3A%20absolute%3B%0A%20%20%20%20width%3A%208px%3B%0A%20%20%20%20height%3A%208px%3B%0A%20%20%20%20top%3A%2050%25%3B%0A%20%20%20%20left%3A%2050%25%3B%0A%20%20%20%20margin-top%3A%20-4px%3B%0A%20%20%20%20margin-left%3A%20-4px%3B%0A%20%20%20%20display%3A%20inline-block%3B%0A%20%20%20%20border-radius%3A%20100%25%3B%0A%20%20%20%20background-color%3A%20rgb%28254%2C%2079%2C%2024%29%3B%0A%7D%0A%23rev_slider_1_1%20.erado_bullet%20.tp-bullet.selected%2C%0A%23rev_slider_1_1%20.erado_bullet%20.tp-bullet%3Ahover%20%7B%0A%20%20box-shadow%3A%200%200%200%202px%20rgba%28254%2C%2079%2C%2024%2C1%29%3B%0A%20%20border%3Anone%3B%0A%20%20border-radius%3A%2050%25%3B%0A%20%20background%3Atransparent%3B%0A%7D%0A%0A%23rev_slider_1_1%20.erado_bullet%20.tp-bullet.selected%20.tp-bullet-inner%2C%0A%23rev_slider_1_1%20.erado_bullet%20.tp-bullet%3Ahover%20.tp-bullet-inner%7B%0A%20transform%3A%20scale%280.4%29%3B%0A%20-webkit-transform%3A%20scale%280.4%29%3B%0A%20background-color%3Argb%28254%2C%2079%2C%2024%29%3B%0A%7D%0A");
        var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
        if (htmlDiv) {
            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
        } else {
            var htmlDiv = document.createElement('div');
            htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
            document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
        }
    </script>

    <script type="text/javascript">
        if (setREVStartSize !== undefined) setREVStartSize({
            c: '#rev_slider_2_1',
            responsiveLevels: [1240, 1240, 778, 480],
            gridwidth: [1920, 1920, 778, 480],
            gridheight: [950, 950, 400, 320],
            sliderLayout: 'auto'
        });

        var revapi2,
            tpj;
        (function() {
            if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded", onLoad);
            else onLoad();

            function onLoad() {
                if (tpj === undefined) {
                    tpj = jQuery;
                    if ("off" == "on") tpj.noConflict();
                }
                if (tpj("#rev_slider_2_1").revolution == undefined) {
                    revslider_showDoubleJqueryError("#rev_slider_2_1");
                } else {
                    revapi2 = tpj("#rev_slider_2_1").show().revolution({
                        sliderType: "standard",
                        jsFileLocation: "https://eradocdn-cff8.kxcdn.com/wp-content/plugins/revslider/public/assets/js/",
                        sliderLayout: "auto",
                        dottedOverlay: "none",
                        delay: 9000,
                        navigation: {
                            keyboardNavigation: "off",
                            keyboard_direction: "horizontal",
                            mouseScrollNavigation: "off",
                            mouseScrollReverse: "default",
                            onHoverStop: "off",
                            bullets: {
                                enable: true,
                                hide_onmobile: false,
                                style: "erado_bullet",
                                hide_onleave: false,
                                direction: "horizontal",
                                h_align: "center",
                                v_align: "bottom",
                                h_offset: 0,
                                v_offset: 20,
                                space: 5,
                                tmp: '<span class="tp-bullet-inner"></span>'
                            }
                        },
                        responsiveLevels: [1240, 1240, 778, 480],
                        visibilityLevels: [1240, 1240, 778, 480],
                        gridwidth: [1920, 1920, 778, 480],
                        gridheight: [950, 950, 400, 320],
                        lazyType: "none",
                        shadow: 0,
                        spinner: "spinner0",
                        stopLoop: "off",
                        stopAfterLoops: -1,
                        stopAtSlide: -1,
                        shuffle: "off",
                        autoHeight: "off",
                        disableProgressBar: "on",
                        hideThumbsOnMobile: "off",
                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        debugMode: false,
                        fallbacks: {
                            simplifyAll: "off",
                            nextSlideOnWindowFocus: "off",
                            disableFocusListener: false,
                        }
                    });
                }; /* END OF revapi call */

            }; /* END OF ON LOAD FUNCTION */
        }()); /* END OF WRAPPING FUNCTION */
    </script>
    <script>
        var htmlDivCss = unescape("%23rev_slider_2_1%20.erado_bullet%20.tp-bullet%7B%0A%20%20border-radius%3A%2050%25%3B%0A%20%20background%3Atransparent%3B%0A%20%20width%3A14px%3B%0A%20%20height%3A14px%3B%0A%7D%0A%23rev_slider_2_1%20.erado_bullet%20.tp-bullet%3Abefore%7B%0A%20%20%20%20content%3A%20%27%27%3B%0A%20%20%20%20position%3A%20absolute%3B%0A%20%20%20%20width%3A%208px%3B%0A%20%20%20%20height%3A%208px%3B%0A%20%20%20%20top%3A%2050%25%3B%0A%20%20%20%20left%3A%2050%25%3B%0A%20%20%20%20margin-top%3A%20-4px%3B%0A%20%20%20%20margin-left%3A%20-4px%3B%0A%20%20%20%20display%3A%20inline-block%3B%0A%20%20%20%20border-radius%3A%20100%25%3B%0A%20%20%20%20background-color%3A%20rgb%28254%2C%2079%2C%2024%29%3B%0A%7D%0A%23rev_slider_2_1%20.erado_bullet%20.tp-bullet.selected%2C%0A%23rev_slider_2_1%20.erado_bullet%20.tp-bullet%3Ahover%20%7B%0A%20%20box-shadow%3A%200%200%200%202px%20rgba%28254%2C%2079%2C%2024%2C1%29%3B%0A%20%20border%3Anone%3B%0A%20%20border-radius%3A%2050%25%3B%0A%20%20background%3Atransparent%3B%0A%7D%0A%0A%23rev_slider_2_1%20.erado_bullet%20.tp-bullet.selected%20.tp-bullet-inner%2C%0A%23rev_slider_2_1%20.erado_bullet%20.tp-bullet%3Ahover%20.tp-bullet-inner%7B%0A%20transform%3A%20scale%280.4%29%3B%0A%20-webkit-transform%3A%20scale%280.4%29%3B%0A%20background-color%3Argb%28254%2C%2079%2C%2024%29%3B%0A%7D%0A");
        var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
        if (htmlDiv) {
            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
        } else {
            var htmlDiv = document.createElement('div');
            htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
            document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
        }
    </script>
@endsection