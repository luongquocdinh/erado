<?php

namespace App\Business;

use App\Helpers\Business;
use App\Models\v1\Recruitment;
use App\Models\v1\Recruitments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BusinessRecruitment implements BusinessInterface
{
    static protected $_instance = NULL;

    /**
     * Use singleton pattern
     *
     * @return BusinessRecruitment object
     */
    static public function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * get model instance
     *
     * @param mixed $data
     *
     * @return Recruitment Object
     */
    public static function getModel($data = array())
    {
        $model = Recruitment::getInstance();

        if ($data) {
            if (is_numeric($data)) {
                $model = $model->find($data);
            } else {
                return $model;
            }
        }

        return $model;
    }

    /**
     * get list Recruitments
     * 
     * @param mixed $request
     * 
     * @return Recruitment $model
     */
    public function list ()
    {
        return $this->getModel()->orderBy('updated_at', 'DESC')->paginate(Business::PER_PAGE);
    }

    /** create data with request
     *
     * @param mixed $request
     *
     * @return Recruitment $model->id
     */
    public function create($data)
    {
        $id = $this->getModel()->create($data)->id;

        return $id;
    }

    /**
     * update data with request
     *
     * @param       $id
     * @param mixed $request
     *
     * @return boolean
     */
    public function update($id, $data = [])
    {
        $model = $this->getModel($id);
        if (!$model) {
            return false;
        }
        
        $model->update($data);

        return $this->getModel($id);
    }

    /**
     * find One data with request
     * 
     * @param       $data
     * @param mixed $request
     * 
     * @return boolean
     */

    public function find($id)
    {
        $model = $this->getModel()->where('id', $id)->first();
        if (!$model) {
            return false;
        }

        return $model;
    }

    public function findByCondition($condition = [])
    {
        return $this->getModel()->where($condition)->get();
    }
}