<?php

namespace App\Business;

use App\Models\v1\Image;
use App\Helpers\Business;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BusinessImage implements BusinessInterface
{
    static protected $_instance = NULL;

    /**
     * Use singleton pattern
     *
     * @return BusinessImage object
     */
    static public function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * get model instance
     *
     * @param mixed $data
     *
     * @return Image Object
     */
    public static function getModel($data = array())
    {
        $model = Image::getInstance();
        if ($data) {
            if (is_numeric($data)) {
                $model = $model->find($data);
            } else {
                return $model;
            }
        }

        return $model;
    }

    /**
     * get list product
     * 
     * @param mixed $request
     * 
     * @return Image $model
     */
    public function list ()
    {
        return $this->getModel()->paginate(Business::PER_PAGE);
    }

    public function getImageByType($type, $link_from_id)
    {
        return $this->getModel()
                    ->where("type", $type)
                    ->where('link_from_id', $link_from_id)
                    ->get();
    }

    /** create data with request
     *
     * @param mixed $request
     *
     * @return Image $model->id
     */
    public function create($data)
    {
        $id = $this->getModel()->create($data)->id;

        return $this->getModel($id);
    }

    /**
     * update data with request
     *
     * @param       $id
     * @param mixed $request
     *
     * @return boolean
     */
    public function update($id, $data = [])
    {
        $model = $this->getModel($id);
        if (!$model) {
            return false;
        }
        
        $model->update($data);

        return $this->getModel($id);
    }

    /**
     * find One data with request
     * 
     * @param       $data
     * @param mixed $request
     * 
     * @return boolean
     */
    public function find($id)
    {
        $model = $this->getModel()->where('id', $id)->first();
        if (!$model) {
            return false;
        }

        return $model;
    }

    public function destroy($id)
    {
        return $this->getModel()->where('id', $id)->delete();
    }

    public function remove($type, $id)
    {
        return $this->getModel()->where([
            'type' => $type,
            'link_from_id' => $id
        ])->delete();
    }
}