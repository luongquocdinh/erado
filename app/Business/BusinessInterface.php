<?php

namespace App\Business;

interface BusinessInterface
{
    static public function getInstance();
}