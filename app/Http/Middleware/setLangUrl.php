<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class setLangUrl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = empty($request->input('lang')) ? 'vn' : $request->input('lang');
        App::setLocale($locale);
        Session::put('locale', $locale);
        URL::defaults(['lang' => $locale]);
        return $next($request);
    }
}
