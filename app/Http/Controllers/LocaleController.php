<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;
use Redirect;

class LocaleController extends Controller
{
    public function change (Request $request)
    {
        if ($request->ajax()){
            $request->session()->put('locale', $request->locales);
            $request->session()->flash('alert-success', ('app.Locale_Change_Success'));
        }
    }
}