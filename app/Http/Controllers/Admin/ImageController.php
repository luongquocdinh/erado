<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\IndexController;
use App\Business\BusinessImage;
use App\Helpers\Business;
use Session;

class ImageController extends IndexController
{
    public function delete($id)
    {
        $image = BusinessImage::getInstance()->destroy($id);

        return $image;
    }
}