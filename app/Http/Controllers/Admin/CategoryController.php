<?php

namespace App\Http\Controllers\Admin;

use App\Business\BusinessImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\IndexController;
use App\Helpers\Business;
use Session;
use App\Business\BusinessCategory;

class CategoryController extends IndexController
{
    public function list()
    {
        $data = BusinessCategory::getInstance()->list();
        
        return view('admin.pages.category.list')->with('data', $data);
    }

    public function create ()
    {
        return view('admin.pages.category.post');
    }

    public function store(Request $request)
    {
        $validator = $this->validateData($this->rule_category(), $request);
        if ($validator->fails()) {
            Session::flash('error', $validator->messages()->first());
            return redirect()->back()->withInput();
        }

        $data = [
            'name_vn'  => $request->name_vn,
            'name_en'  => $request->name_en,
            'description_vn' => $request->description_vn,
            'description_en' => $request->description_en,
            'type' => $request->type,
            'cat_slug_vn' => $this->to_slug($request->name_vn),
            'cat_slug_en' => $this->to_slug($request->name_en),
            'title_vn' => $request->title_vn,
            'title_en' => $request->title_en,
            'meta_vn' => $request->meta_vn,
            'meta_en' => $request->meta_en,
        ];

        $id = BusinessCategory::getInstance()->create($data);

        if(!empty($request->file('images'))){
            $files = $request->file('images');
            $path = public_path('storage');

            foreach ($files as $file) {
                $name = time() . '_' . $file->getClientOriginalName();
                $file->move($path, $name);
                $image = [
                    'path' => 'storage/' . $name,
                    'name' => $name,
                    'type' => Business::IMAGE_TYPE_CATEGORY_PRODUCT,
                    'link_from_id' => $id
                ];
                BusinessImage::getInstance()->create($image);
            }
        }

        Session::flash('success', trans('alert.create') . trans('admin.category') . ' ' . $request->name . ' ' . trans('alert.success'));
        return redirect()->route('admin.category.get');
    }

    public function edit($id)
    {
        $detail = BusinessCategory::getInstance()->find($id);
        $images = BusinessImage::getInstance()->getImageByType(Business::IMAGE_TYPE_CATEGORY_PRODUCT, $id);

        return view('admin.pages.category.post')->with([
            'detail' => $detail,
            'images' => $images
        ]);
    }

    public function update($id, Request $request)
    {
        $validator = $this->validateData($this->rule_category(), $request);
        if ($validator->fails()) {
            Session::flash('error', $validator->messages()->first());
            return redirect()->back()->withInput();
        }

        $data = [
            'name_vn'  => $request->name_vn,
            'name_en'  => $request->name_en,
            'description_vn' => $request->description_vn,
            'description_en' => $request->description_en,
            'type' => $request->type,
            'title_vn' => $request->title_vn,
            'title_en' => $request->title_en,
            'cat_slug_vn' => $this->to_slug($request->name_vn),
            'cat_slug_en' => $this->to_slug($request->name_en),
            'meta_vn' => $request->meta_vn,
            'meta_en' => $request->meta_en,
        ];
        BusinessCategory::getInstance()->update($id, $data);

        Session::flash('success', trans('alert.update') . trans('admin.category') . ' ' . $request->name . ' ' . trans('alert.success'));
        return redirect()->route('admin.category.get');
    }

    public function changeStatus($id)
    {
        $category = BusinessCategory::getInstance()->find($id);
        $data = [
            'is_active' => !$category->is_active
        ];

        $update = BusinessCategory::getInstance()->update($id, $data);

        return response()->json([
            'meta' => 200,
            'success' => true,
            'data' => $update
        ]);
    }

    private function rule_category()
    {
        return [
            'name_vn' => 'required',
            'name_en' => 'required',
            'description_vn' => 'required',
            'description_en' => 'required'
        ];
    }
}