<?php

namespace App\Http\Controllers\Admin;

use App\Business\BusinessAbout;
use App\Business\BusinessImage;
use App\Helpers\Business;
use Illuminate\Http\Request;
use Session;

class AboutController extends IndexController
{

    public function list()
    {
        $data = BusinessAbout::getInstance()->list();

        return view('admin.pages.about.list')->with('data', $data);
    }

    public function create ()
    {

        return view('admin.pages.about.post');
    }

    public function store(Request $request)
    {
        $validator = $this->validateData($this->rule_about(), $request);
        if ($validator->fails()) {
            Session::flash('error', $validator->messages()->first());
            return redirect()->back()->withInput();
        }

        $data = [
            'title_vn'  => $request->title_vn,
            'title_en'  => $request->title_en,
            'content_vn' => $request->content_vn,
            'content_en' => $request->content_en,
            'slug_vn' => $this->to_slug($request->title_vn),
            'slug_en' => $this->to_slug($request->title_en),
            'author_vn' => $request->author_vn,
            'author_en' => $request->author_en,
            'sapo_vn' => $request->sapo_vn,
            'sapo_en' => $request->sapo_en,
        ];


        $id =  BusinessAbout::getInstance()->create($data);

        if($files=$request->file('images')){
            $path = public_path('storage');
            foreach ($files as $file) {
                $name = time() . '_' . $file->getClientOriginalName();
                $file->move($path, $name);
                $image = [
                    'path' => 'storage/' . $name,
                    'name' => $name,
                    'type' => Business::IMAGE_TYPE_ABOUT,
                    'link_from_id' => $id
                ];
                BusinessImage::getInstance()->create($image);
            }
        }
        Session::flash('success', trans('alert.create') . trans('admin.about_us') . ' ' . $request->title_vn . ' ' . trans('alert.success'));
        return redirect()->route('admin.about_us.get');
    }

    public function edit($id)
    {
        $detail = BusinessAbout::getInstance()->find($id);
        $images = BusinessImage::getInstance()->getImageByType(Business::IMAGE_TYPE_ABOUT, $id);

        return view('admin.pages.about.post')->with([
            'detail' => $detail,
            'images' => $images,
        ]);
    }

    public function update($id, Request $request)
    {
        $validator = $this->validateData($this->rule_about(), $request);
        if ($validator->fails()) {
            Session::flash('error', $validator->messages()->first());
            return redirect()->back()->withInput();
        }

        $data = [
            'title_vn'  => $request->title_vn,
            'title_en'  => $request->title_en,
            'author_vn'  => $request->author_vn,
            'author_en'  => $request->author_en,
            'content_vn' => $request->content_vn,
            'content_en' => $request->content_en,
            'slug_vn' => $this->to_slug($request->title_vn),
            'slug_en' => $this->to_slug($request->title_en),
            'sapo_vn' => $request->sapo_vn,
            'sapo_en' => $request->sapo_en,
        ];

        $about = BusinessAbout::getInstance()->update($id, $data);
        if($files=$request->file('images')){


            BusinessImage::getInstance()->remove(Business::IMAGE_TYPE_ABOUT,$id);
            $path = public_path('storage');
            foreach ($files as $file) {
                $name = time() . '_' . $file->getClientOriginalName();
                $file->move($path, $name);
                $image = [
                    'path' => 'storage/' . $name,
                    'name' => $name,
                    'type' => Business::IMAGE_TYPE_ABOUT,
                    'link_from_id' => $id
                ];
                BusinessImage::getInstance()->create($image);
            }
        }

        return redirect()->back()->withInput();
    }

    private function rule_about()
    {
        return [
            'title_vn' => 'required',
            'title_en' => 'required',
            'content_vn' => 'required',
            'content_en' => 'required',
            'author_vn' => 'required',
            'author_en' => 'required',
            'sapo_vn' => 'required',
            'sapo_en' => 'required',
        ];
    }
}