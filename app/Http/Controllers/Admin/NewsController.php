<?php

namespace App\Http\Controllers\Admin;

use App\Business\BusinessNews;
use App\Business\BusinessNewsTag;
use Illuminate\Http\Request;
use App\Business\BusinessImage;
use App\Business\BusinessProduct;
use App\Business\BusinessProductTag;
use App\Helpers\Business;
use Session;
use App\Business\BusinessCategory;

class NewsController extends IndexController
{
    public function list()
    {
        $data = BusinessNews::getInstance()->list();

        return view('admin.pages.news.list')->with('data', $data);
    }

    public function create ()
    {
        $categoies = BusinessCategory::getInstance()->findByCondition(['type' => Business::CATEGORY_NEWS]);
        return view('admin.pages.news.post')->with([
            'categories' => $categoies
        ]);
    }

    public function store(Request $request)
    {
        $validator = $this->validateData($this->rule_news(), $request);
        if ($validator->fails()) {
            Session::flash('error', $validator->messages()->first());
            return redirect()->back()->withInput();
        }

        $data = [
            'title_vn'  => $request->title_vn,
            'title_en'  => $request->title_en,
            'content_vn' => $request->content_vn,
            'content_en' => $request->content_en,
            'slug_vn' => $this->to_slug($request->title_vn),
            'slug_en' => $this->to_slug($request->title_en),
            'author_vn' => $request->author_vn,
            'author_en' => $request->author_en,
            'sapo_vn' => $request->sapo_vn,
            'sapo_en' => $request->sapo_en,
        ];
        $id = BusinessNews::getInstance()->create($data);
        $news = BusinessNews::getInstance()->find($id);
        if (isset($request->category)) {
            $news->categories()->attach($request->category);
        }

        if (isset($request->tag_vn)) {
            foreach ($request->tag_vn as $tag) {
                BusinessNewsTag::getInstance()->create([
                    'news_id' => $id,
                    'language' => 'vn',
                    'name' => $tag
                ]);
            }
        }

        if (isset($request->tag_en)) {
            foreach ($request->tag_en as $tag) {
                BusinessNewsTag::getInstance()->create([
                    'news_id' => $id,
                    'language' => 'en',
                    'name' => $tag
                ]);
            }
        }
        if($files=$request->file('images')){
            $path = public_path('storage');
            foreach ($files as $file) {
                $name = time() . '_' . $file->getClientOriginalName();
                $file->move($path, $name);
                $image = [
                    'path' => 'storage/' . $name,
                    'name' => $name,
                    'type' => Business::IMAGE_TYPE_NEWS,
                    'link_from_id' => $id
                ];
                BusinessImage::getInstance()->create($image);
            }
        }
        Session::flash('success', trans('alert.create') . trans('admin.news') . ' ' . $request->title_vn . ' ' . trans('alert.success'));
        return redirect()->route('admin.news.get');
    }

    public function edit($id)
    {
        $detail = BusinessNews::getInstance()->find($id);

        $news_category = [];
        foreach ($detail->categories as $category) {
            $news_category[] = $category->id;
        }

        $categoies = BusinessCategory::getInstance()->findByCondition(['type' => Business::CATEGORY_NEWS]);
        $images = BusinessImage::getInstance()->getImageByType(Business::IMAGE_TYPE_NEWS, $id);

        return view('admin.pages.news.post')->with([
            'detail' => $detail,
            'images' => $images,
            'categories' => $categoies,
            'news_category' => $news_category
        ]);
    }

    public function update($id, Request $request)
    {
        $validator = $this->validateData($this->rule_news(), $request);
        if ($validator->fails()) {
            Session::flash('error', $validator->messages()->first());
            return redirect()->back()->withInput();
        }

        $data = [
            'title_vn'  => $request->title_vn,
            'title_en'  => $request->title_en,
            'author_vn'  => $request->author_vn,
            'author_en'  => $request->author_en,
            'content_vn' => $request->content_vn,
            'content_en' => $request->content_en,
            'slug_vn' => $this->to_slug($request->title_vn),
            'slug_en' => $this->to_slug($request->title_en),
            'sapo_vn' => $request->sapo_vn,
            'sapo_en' => $request->sapo_en,
        ];
        $news = BusinessNews::getInstance()->update($id, $data);
        if (isset($request->category)) {
            $news->categories()->sync($request->category);
        }

        if (isset($request->tag_vn)) {
            BusinessNewsTag::getInstance()->deleteByNewsId($id, 'vn');
            foreach ($request->tag_vn as $tag) {
                BusinessNewsTag::getInstance()->create([
                    'news_id' => $id,
                    'language' => 'vn',
                    'name' => $tag
                ]);
            }
        }

        if (isset($request->tag_en)) {
            BusinessNewsTag::getInstance()->deleteByNewsId($id, 'en');
            foreach ($request->tag_en as $tag) {
                BusinessNewsTag::getInstance()->create([
                    'news_id' => $id,
                    'language' => 'en',
                    'name' => $tag
                ]);
            }
        }
        if($files=$request->file('images')){

            BusinessImage::getInstance()->remove(Business::IMAGE_TYPE_NEWS,$id);
            $path = public_path('storage');
            foreach ($files as $file) {
                $name = time() . '_' . $file->getClientOriginalName();
                $file->move($path, $name);
                $image = [
                    'path' => 'storage/' . $name,
                    'name' => $name,
                    'type' => Business::IMAGE_TYPE_NEWS,
                    'link_from_id' => $id
                ];
                BusinessImage::getInstance()->create($image);
            }
        }

        return redirect()->back()->withInput();
    }

    private function rule_news()
    {
        return [
            'title_vn' => 'required',
            'title_en' => 'required',
            'content_vn' => 'required',
            'content_en' => 'required',
            'author_vn' => 'required',
            'author_en' => 'required',
            'sapo_vn' => 'required',
            'sapo_en' => 'required',
        ];
    }
}