<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\IndexController;
use App\Helpers\Business;
use Session;
use App\Business\BusinessPartner;
use App\Business\BusinessImage;

class PartnerController extends IndexController
{
    public function list()
    {
        $data = BusinessPartner::getInstance()->list();
        
        return view('admin.pages.partner.list')->with('data', $data);
    }

    public function create ()
    {
        return view('admin.pages.partner.post');
    }

    public function store(Request $request)
    {
        $validator = $this->validateData($this->rule_partner(), $request);
        if ($validator->fails()) {
            Session::flash('error', $validator->messages()->first());
            return redirect()->back()->withInput();
        }
        $file = $request->file('icon');
        $path = public_path('storage');$name = time() . '_' . $file->getClientOriginalName();
        $file->move($path, $name);
        $icon = 'storage/' . $name;
        $data = [
            'name'  => $request->name,
            'link'  => $request->link,
            'icon' => $icon,
        ];
        BusinessPartner::getInstance()->create($data);

        Session::flash('success', trans('alert.create') . trans('admin.partner') . ' ' . $request->name . ' ' . trans('alert.success'));
        return redirect()->route('admin.partner.get');
    }

    public function edit($id)
    {
        $detail = BusinessPartner::getInstance()->find($id);

        return view('admin.pages.partner.post')->with([
            'detail' => $detail
        ]);
    }

    public function update($id, Request $request)
    {
        $validator = $this->validateData($this->rule_update_partner(), $request);
        if ($validator->fails()) {
            Session::flash('error', $validator->messages()->first());
            return redirect()->back()->withInput();
        }
        if ($request->file('icon')) {
            $file = $request->file('icon');
            $path = public_path('storage');
            $name = time() . '_' . $file->getClientOriginalName();
            $file->move($path, $name);
            $icon = 'storage/' . $name;
        }
        $partner = BusinessPartner::getInstance()->find($id);
        $data = [
            'name'  => $request->name,
            'link'  => $request->link,
            'icon' => isset($icon) ? $icon : $partner->icon,
        ];

        BusinessPartner::getInstance()->update($id, $data);

        Session::flash('success', trans('alert.update') . trans('admin.partner') . ' ' . $request->name . ' ' . trans('alert.success'));
        return redirect()->route('admin.partner.get');
    }

    public function  deleteIcon($id)
    {
        $data = [
            'icon' => ''
        ];

        BusinessPartner::getInstance()->update($id, $data);

        return response()->json([]);
    }

    public function changeStatus($id)
    {
        $partner = BusinessPartner::getInstance()->find($id);
        $data = [
            'is_active' => !$partner->is_active
        ];

        $update = BusinessPartner::getInstance()->update($id, $data);

        return response()->json([
            'meta' => 200,
            'success' => true,
            'data' => $update
        ]);
    }

    private function rule_partner()
    {
        return [
            'name' => 'required',
            'icon' => 'required',
        ];
    }

    private function rule_update_partner()
    {
        return [
            'name' => 'required',
        ];
    }
}