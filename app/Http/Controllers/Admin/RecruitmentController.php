<?php

namespace App\Http\Controllers\Admin;

use App\Business\BusinessImage;
use App\Business\BusinessRecruitment;
use App\Helpers\Business;
use App\Models\v1\Recruitments;
use Illuminate\Http\Request;
use Session;

class RecruitmentController extends IndexController
{

    public function list()
    {
        $data = BusinessRecruitment::getInstance()->list();

        return view('admin.pages.recruitment.list')->with('data', $data);
    }

    public function create ()
    {

        return view('admin.pages.recruitment.post');
    }

    public function store(Request $request)
    {
        $validator = $this->validateData($this->rule_recruitment(), $request);
        if ($validator->fails()) {
            Session::flash('error', $validator->messages()->first());
            return redirect()->back()->withInput();
        }

        $data = [
            'title_vn'  => $request->title_vn,
            'title_en'  => $request->title_en,
            'content_vn' => $request->content_vn,
            'content_en' => $request->content_en,
            'slug_vn' => $this->to_slug($request->title_vn),
            'slug_en' => $this->to_slug($request->title_en),
            'author_vn' => $request->author_vn,
            'author_en' => $request->author_en,
            'sapo_vn' => $request->sapo_vn,
            'sapo_en' => $request->sapo_en,
        ];


        $id =  BusinessRecruitment::getInstance()->create($data);

        if($files=$request->file('images')){
            $path = public_path('storage');
            foreach ($files as $file) {
                $name = time() . '_' . $file->getClientOriginalName();
                $file->move($path, $name);
                $image = [
                    'path' => 'storage/' . $name,
                    'name' => $name,
                    'type' => Business::IMAGE_TYPE_RECRUITMENT,
                    'link_from_id' => $id
                ];
                BusinessImage::getInstance()->create($image);
            }
        }
        Session::flash('success', trans('alert.create') . trans('admin.recruitment') . ' ' . $request->title_vn . ' ' . trans('alert.success'));
        return redirect()->route('admin.recruitment.get');
    }

    public function edit($id)
    {
        $detail = BusinessRecruitment::getInstance()->find($id);
        $images = BusinessImage::getInstance()->getImageByType(Business::IMAGE_TYPE_RECRUITMENT, $id);

        return view('admin.pages.recruitment.post')->with([
            'detail' => $detail,
            'images' => $images,
        ]);
    }

    public function update($id, Request $request)
    {
        $validator = $this->validateData($this->rule_recruitment(), $request);
        if ($validator->fails()) {
            Session::flash('error', $validator->messages()->first());
            return redirect()->back()->withInput();
        }

        $data = [
            'title_vn'  => $request->title_vn,
            'title_en'  => $request->title_en,
            'author_vn'  => $request->author_vn,
            'author_en'  => $request->author_en,
            'content_vn' => $request->content_vn,
            'content_en' => $request->content_en,
            'slug_vn' => $this->to_slug($request->title_vn),
            'slug_en' => $this->to_slug($request->title_en),
            'sapo_vn' => $request->sapo_vn,
            'sapo_en' => $request->sapo_en,
        ];

        $recruitment = BusinessRecruitment::getInstance()->update($id, $data);
        if($files=$request->file('images')){


            BusinessImage::getInstance()->remove(Business::IMAGE_TYPE_RECRUITMENT,$id);
            $path = public_path('storage');
            foreach ($files as $file) {
                $name = time() . '_' . $file->getClientOriginalName();
                $file->move($path, $name);
                $image = [
                    'path' => 'storage/' . $name,
                    'name' => $name,
                    'type' => Business::IMAGE_TYPE_RECRUITMENT,
                    'link_from_id' => $id
                ];
                BusinessImage::getInstance()->create($image);
            }
        }

        return redirect()->back()->withInput();
    }

    private function rule_recruitment()
    {
        return [
            'title_vn' => 'required',
            'title_en' => 'required',
            'content_vn' => 'required',
            'content_en' => 'required',
            'author_vn' => 'required',
            'author_en' => 'required',
            'sapo_vn' => 'required',
            'sapo_en' => 'required',
        ];
    }
}