<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\IndexController;
use App\Business\BusinessImage;
use App\Business\BusinessProduct;
use App\Business\BusinessProductTag;
use App\Helpers\Business;
use Session;
use App\Business\BusinessCategory;

class ProductController extends IndexController
{
    public function list()
    {
        $data = BusinessProduct::getInstance()->list();
        
        return view('admin.pages.product.list')->with('data', $data);
    }

    public function create ()
    {
        $categoies = BusinessCategory::getInstance()->findByCondition(['type' => Business::CATEGORY_PRODUCT]);
        return view('admin.pages.product.post')->with([
            'categories' => $categoies
        ]);
    }

    public function store(Request $request)
    {
        $validator = $this->validateData($this->rule_product(), $request);
        if ($validator->fails()) {
            Session::flash('error', $validator->messages()->first());
            return redirect()->back()->withInput();
        }

        $data = [
            'name_vn'  => $request->name_vn,
            'name_en'  => $request->name_en,
            'min_price_vn' => (float)str_replace(',', '',$request->min_price_vn),
            'max_price_vn' => (float)str_replace(',', '',$request->max_price_vn),
            'min_price_en' => (float)str_replace(',', '',$request->min_price_en),
            'max_price_en' => (float)str_replace(',', '',$request->max_price_en),
            'sku' => $request->sku,
            'slug_vn' => $this->to_slug($request->name_vn),
            'slug_en' => $this->to_slug($request->name_en),
            'type' => $request->type,
            'sapo_vn' => $request->sapo_vn,
            'sapo_en' => $request->sapo_en,
            'unit_vn' => $request->unit_vn,
            'unit_en' => $request->unit_en,
            'description_vn' => $request->description_vn,
            'description_en' => $request->description_en,
            'title_vn' => $request->title_vn,
            'title_en' => $request->title_en,
            'meta_vn' => $request->meta_vn,
            'meta_en' => $request->meta_en,
        ];
        $id = BusinessProduct::getInstance()->create($data);
        $product = BusinessProduct::getInstance()->find($id);
        if (isset($request->category)) {
            $product->categories()->attach($request->category);
        }

        if (isset($request->tag_vn)) {
            foreach ($request->tag_vn as $tag) {
                BusinessProductTag::getInstance()->create([
                    'product_id' => $id,
                    'language' => 'vn',
                    'name' => $tag
                ]);
            }
        }

        if (isset($request->tag_en)) {
            foreach ($request->tag_en as $tag) {
                BusinessProductTag::getInstance()->create([
                    'product_id' => $id,
                    'language' => 'en',
                    'name' => $tag
                ]);
            }
        }
        if($files=$request->file('images')){
            $path = public_path('storage');
            foreach ($files as $file) {
                $name = time() . '_' . $file->getClientOriginalName();
                $file->move($path, $name);
                $image = [
                    'path' => 'storage/' . $name,
                    'name' => $name,
                    'type' => Business::IMAGE_TYPE_PRODUCT,
                    'link_from_id' => $id
                ];
                BusinessImage::getInstance()->create($image);
            }
        }
        Session::flash('success', trans('alert.create') . trans('admin.product') . ' ' . $request->name . ' ' . trans('alert.success'));
        return redirect()->route('admin.product.get');
    }

    public function edit($id)
    {
        $detail = BusinessProduct::getInstance()->find($id);

        $product_category = [];
        foreach ($detail->categories as $category) {
            array_push($product_category, $category->id);
        }

        $categoies = BusinessCategory::getInstance()->findByCondition(['type' => Business::CATEGORY_PRODUCT]);
        $images = BusinessImage::getInstance()->getImageByType(Business::IMAGE_TYPE_PRODUCT, $id);

        return view('admin.pages.product.post')->with([
            'detail' => $detail,
            'images' => $images,
            'categories' => $categoies,
            'product_category' => $product_category
        ]);
    }

    public function update($id, Request $request)
    {
        $validator = $this->validateData($this->rule_product(), $request);
        if ($validator->fails()) {
            Session::flash('error', $validator->messages()->first());
            return redirect()->back()->withInput();
        }
        
        $data = [
            'name_vn'  => $request->name_vn,
            'name_en'  => $request->name_en,
            'min_price_vn' => (float)str_replace(',', '',$request->min_price_vn),
            'max_price_vn' => (float)str_replace(',', '',$request->max_price_vn),
            'min_price_en' => (float)str_replace(',', '',$request->min_price_en),
            'max_price_en' => (float)str_replace(',', '',$request->max_price_en),
            'sku' => $request->sku,
            'slug_vn' => $this->to_slug($request->name_vn),
            'slug_en' => $this->to_slug($request->name_en),
            'type' => $request->type,
            'sapo_vn' => $request->sapo_vn,
            'sapo_en' => $request->sapo_en,
            'unit_vn' => $request->unit_vn,
            'unit_en' => $request->unit_en,
            'description_vn' => $request->description_vn,
            'description_en' => $request->description_en,
            'title_vn' => $request->title_vn,
            'title_en' => $request->title_en,
            'meta_vn' => $request->meta_vn,
            'meta_en' => $request->meta_en,
        ];
        $product = BusinessProduct::getInstance()->update($id, $data);
        if (isset($request->category)) {
            $product->categories()->sync($request->category);
        }
        
        if (isset($request->tag_vn)) {
            BusinessProductTag::getInstance()->deleteByProductId($id, 'vn');
            foreach ($request->tag_vn as $tag) {
                BusinessProductTag::getInstance()->create([
                    'product_id' => $id,
                    'language' => 'vn',
                    'name' => $tag
                ]);
            }
        }

        if (isset($request->tag_en)) {
            BusinessProductTag::getInstance()->deleteByProductId($id, 'en');
            foreach ($request->tag_en as $tag) {
                BusinessProductTag::getInstance()->create([
                    'product_id' => $id,
                    'language' => 'en',
                    'name' => $tag
                ]);
            }
        }
        if($files=$request->file('images')){
            $path = public_path('storage');
            foreach ($files as $file) {
                $name = time() . '_' . $file->getClientOriginalName();
                $file->move($path, $name);
                $image = [
                    'path' => 'storage/' . $name,
                    'name' => $name,
                    'type' => Business::IMAGE_TYPE_PRODUCT,
                    'link_from_id' => $id
                ];
                BusinessImage::getInstance()->create($image);
            }
        }

        return redirect()->back()->withInput();
    }

    public function changeStatus($id)
    {
        $product = BusinessProduct::getInstance()->find($id);
        $data = [
            'is_active' => !$product->is_active
        ];

        $update = BusinessProduct::getInstance()->update($id, $data);

        return response()->json([
            'meta' => 200,
            'success' => true,
            'data' => $update
        ]);
    }

    private function rule_product()
    {
        return [
            'sku'  => 'required',
            'name_vn' => 'required',
            'min_price_vn' => 'required',
            'max_price_vn' => 'required',
            'sapo_vn' => 'required',
            'description_vn' => 'required',
            'name_en' => 'required',
            'min_price_en' => 'required',
            'max_price_en' => 'required',
            'sapo_en' => 'required',
            'description_en' => 'required',
            'unit_vn'   => 'required',
            'unit_en'   => 'required'
        ];
    }
}