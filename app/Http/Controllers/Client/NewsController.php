<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Business;
use App;

class NewsController extends Controller
{
    public function index (Request $request)
    {

//        $this->sendMail();
        $lang = isset($request->lang)?$request->lang:Business::LANG_DEFAULT ;
        $category = App\Models\v1\Category::getCatByType($lang, Business::CATEGORY_NEWS);
        $data['news'] = App\Models\v1\News::getAllNews($lang, 'all');

        $data['categorys'] = $category;
        $data['lang'] = $lang;
        return view('client.pages.news.news', $data);
    }

    public function detail (Request $request)
    {
        $lang = isset($request->lang)?$request->lang:Business::LANG_DEFAULT ;
        $slug = isset($request->slug)?$request->slug:'' ;
        $news = App\Models\v1\News::getNewsBySlug($lang, $slug);
        $data['news'] = $news;
        $data['news_tags'] = App\Models\v1\NewsTag::getNewsTagByNewID($lang, $news->id);

        return view('client.pages.news.news_detail', $data);
    }

    public function getNewsByCat(Request $request){

        $lang = isset($request->lang)?$request->lang:Business::LANG_DEFAULT ;
        $cat = isset($request->cat_news)?$request->cat_news:'all' ;
        $data['news'] = App\Models\v1\News::getAllNews($lang, $cat);
        $data['data']['news_html'] = view('client.pages.news.news_html', $data)->render();
        $data['data']['code'] = 200;

        return $data ;
    }

    public function sendMail()
    {
        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("test@example.com", "Example User");
        $email->setSubject("Sending with SendGrid is Fun");
        $email->addTo("minhloc1806@gmail.com", "Example User");
        $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
        $email->addContent(
            "text/html", "<strong>and easy to do anywhere, even with PHP</strong>"
        );
        $sendgrid = new \SendGrid('');
        try {
            $response = $sendgrid->send($email);
            print $response->statusCode() . "\n";
            print $response->body() . "\n";
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
    }
}