<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Business;
use App\Business\BusinessCategory;
use App;
use App\Business\BusinessProduct;
use App\Business\BusinessImage;
use Session;

class ProductController extends Controller
{
    public function index (Request $request)
    {
        /*$products = BusinessProduct::getInstance()->list();
        foreach ($products as $product) {
            $product->categories;
            $images = BusinessImage::getInstance()->getImageByType(Business::IMAGE_TYPE_PRODUCT, $product->id);
            $product->images = $images;
        }

        $categories = BusinessCategory::getInstance()->all();

        return view('client.pages.product.list', [
            'products' => $products,
            'categories' => $categories
        ]);*/


        $lang = isset($request->lang)?$request->lang:Business::LANG_DEFAULT ;
        $category = App\Models\v1\Category::getCatByType($lang, Business::CATEGORY_PRODUCT);
        $data['products'] = App\Models\v1\Product::getAllProduct($lang, 'all');

        $data['categorys'] = $category;
        $data['lang'] = $lang;
        return view('client.pages.product.list', $data);
    }

    public function detail(Request $request)
    {
       /* $lang = isset($request->lang)?$request->lang:Business::LANG_DEFAULT ;
        $slug = isset($request->slug)?$request->slug:'' ;
        $news = App\Models\v1\Product::getProductBySlug($lang, $slug);
        $data['products'] = $news;
        $data['products_tags'] = App\Models\v1\ProductTag::getProductByProductTagId($lang, $news->id);*/

        $lang = isset($request->lang)?$request->lang:Business::LANG_DEFAULT ;
        $slug = isset($request->slug)?$request->slug:'' ;

        $product = BusinessProduct::getInstance()->findByCondition(['slug_' . $lang => $slug]);
        $images = BusinessImage::getInstance()->getImageByType(Business::IMAGE_TYPE_PRODUCT, $product->id);
        $product_tag = App\Models\v1\ProductTag::getProductByProductTagId($lang, $product->id);

        return view('client.pages.product.detail', [
            'product' => $product,
            'images' => $images,
            'product_tag' => $product_tag
        ]);

    }

    public function getProductByCat(Request $request){

        $lang = isset($request->lang)?$request->lang:Business::LANG_DEFAULT ;
        $cat = isset($request->cat_product)?$request->cat_product:'all' ;
        $data['products'] = App\Models\v1\Product::getAllProduct($lang, $cat);

        $images = App\Models\v1\Image::getImages();
        $data['images'] = $images[0];

        $data['data']['product_html'] = view('client.pages.product.product_html', $data)->render();
        $data['data']['code'] = 200;

        return $data ;
    }
}