<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Business;
use App;

class AboutController extends Controller
{
    public function index (Request $request)
    {
        $lang = isset($request->lang)?$request->lang:Business::LANG_DEFAULT ;
        $data['abouts'] = App\Models\v1\About::getAllAbout($lang);

        return view('client.pages.about.about', $data);
    }

    public function detail (Request $request)
    {
        $lang = isset($request->lang)?$request->lang:Business::LANG_DEFAULT ;
        $slug = isset($request->slug)?$request->slug:'' ;
        $about = App\Models\v1\About::getAboutBySlug($lang, $slug);
        $data['about'] = $about;

        return view('client.pages.about.about_detail', $data);
    }
}