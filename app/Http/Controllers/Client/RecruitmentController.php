<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Business;
use App;

class RecruitmentController extends Controller
{
    public function index (Request $request)
    {
        $lang = isset($request->lang)?$request->lang:Business::LANG_DEFAULT ;
        $data['recruitments'] = App\Models\v1\Recruitment::getAllRecruitment($lang);

        return view('client.pages.recruitment.recruitment', $data);
    }

    public function detail (Request $request)
    {
        $lang = isset($request->lang)?$request->lang:Business::LANG_DEFAULT ;
        $slug = isset($request->slug)?$request->slug:'' ;
        $recruitment = App\Models\v1\Recruitment::getRecruitmentBySlug($lang, $slug);
        $data['recruitment'] = $recruitment;

        return view('client.pages.recruitment.recruitment_detail', $data);
    }
}