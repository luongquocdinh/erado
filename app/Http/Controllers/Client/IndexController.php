<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Business;
use App\Business\BusinessProduct;
use App\Business\BusinessImage;
use App\Business\BusinessNews;
use App\Business\BusinessPartner;
use App;

class IndexController extends Controller
{
    public function index (Request $request)
    {
        $products = BusinessProduct::getInstance()->getList();
        
        foreach ($products as $product) {
            $product->categories;
            $images = BusinessImage::getInstance()->getImageByType(Business::IMAGE_TYPE_PRODUCT, $product->id);
            if($product->images){
                $product->images = $images[0]->path;
            }

        }

        $news = BusinessNews::getInstance()->list();
        
        foreach ($news as $item) {
            $item['number_comment'] = $item->comment->count();
            $image_new = BusinessImage::getInstance()->getImageByType(Business::IMAGE_TYPE_NEWS, $item->id);
            $item->images = $image_new;
        }

        $partner = BusinessPartner::getInstance()->list();
        
        return view('client.pages.home')->with([
            'products' => $products,
            'news' => $news,
            'partner' => $partner
        ]);
    }
}