<?php

namespace App\Models\v1;

use App\Models\BaseModel;

class ProductTag extends BaseModel
{
    protected $table = 'product_tag';

    static protected $_instance = NULL;

    /**
     * Use singleton pattern
     *
     * @return ProductTag object
     */
    static public function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    static public function clearInstance()
    {
        self::$_instance = null;
    }

    protected $fillable = [
        'product_id', 'name', 'language',
        'created_at', 'updated_at'
    ];

    static public function getProductByProductTagId($lang, $product_id)
    {
        $items = \DB::table('product_tag')->select(
            'product_tag.*'
        )
            ->where('product_tag.product_id', '=', $product_id)
            ->where('product_tag.language', '=', $lang);

        return $items->get();
    }
}