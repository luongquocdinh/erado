<?php

namespace App\Models\v1;

use App\Helpers\Business;
use App\Models\BaseModel;

class Image extends BaseModel
{
    protected $table = 'images';

    static protected $_instance = NULL;

    /**
     * Use singleton pattern
     *
     * @return Image object
     */
    static public function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    static public function clearInstance()
    {
        self::$_instance = null;
    }

    protected $fillable = [
        'path', 'name', 'type', 'link_from_id', 'is_default',
        'created_at', 'updated_at'
    ];

    public static function getImages()
    {
        $items = \DB::table('images')->select(
            'images.*'
        )
            ->where([
                'images.type' => Business::IMAGE_TYPE_PRODUCT,
            ])->get();

        return $items;

    }
}