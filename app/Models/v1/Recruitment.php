<?php

namespace App\Models\v1;


use App\Helpers\Business;
use App\Models\BaseModel;

class Recruitment extends BaseModel
{
    protected $table = 'recruitment';

    static protected $_instance = NULL;

    /**
     * Use singleton pattern
     *
     * @return Recruitment object
     */
    static public function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    static public function clearInstance()
    {
        self::$_instance = null;
    }

    protected $fillable = [
        'title_vn', 'slug_vn', 'author_vn', 'content_vn', 'is_del', 'created_at', 'updated_at', 'sapo_vn',
        'title_en', 'slug_en', 'author_en', 'content_en', 'is_del', 'created_at', 'updated_at', 'sapo_en'
    ];

    static public function getAllRecruitment($lang)
    {
        $items = \DB::table('recruitment')->select(
            'recruitment.*',
            'recruitment.title_'.$lang. ' as title',
            'recruitment.author_'.$lang. ' as author',
            'recruitment.sapo_'.$lang. ' as sapo',
            'recruitment.slug_'.$lang. ' as slug',
            'images.path as image'

        )
            ->leftJoin('images', 'images.link_from_id', '=', 'recruitment.id')
            ->where('recruitment.is_del', '=', Business::IS_ACTIVE)
            ->where('images.type', '=', Business::IMAGE_TYPE_RECRUITMENT);

        $items = $items->get();
        foreach ($items as &$item){
            $item->link_detail = route('client.recruitment_detail', $item->id);
        }

        return $items->toArray();
    }

    static public function getRecruitmentBySlug($lang,$slug)
    {
        $items = \DB::table('recruitment')->select(
            'recruitment.*',
            'recruitment.title_'.$lang. ' as title',
            'recruitment.author_'.$lang. ' as author',
            'recruitment.content_'.$lang. ' as content',
            'recruitment.slug_'.$lang. ' as slug'
        )
            ->where('recruitment.slug_'.$lang, '=', $slug)
            ->where('recruitment.is_del', '=', Business::IS_ACTIVE);

        return $items->first();
    }
}