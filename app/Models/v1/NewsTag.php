<?php

namespace App\Models\v1;

use App\Helpers\Business;
use App\Models\BaseModel;

class NewsTag extends BaseModel
{
    protected $table = 'news_tag';

    static protected $_instance = NULL;

    /**
     * Use singleton pattern
     *
     * @return NewsTag object
     */
    static public function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    static public function clearInstance()
    {
        self::$_instance = null;
    }

    protected $fillable = [
        'news_id', 'name',
        'created_at', 'updated_at', 'language'
    ];

    static public function getNewsTagByNewID($lang, $news_id)
    {
        $items = \DB::table('news_tag')->select(
            'news_tag.*'
        )
            ->where('news_tag.news_id', '=', $news_id)
            ->where('news_tag.language', '=', $lang);

        return $items->get();
    }
}