<?php

namespace App\Models\v1;

use App\Helpers\Business;
use App\Models\BaseModel;
use App\Models\v1\Category;
use App\Models\v1\Comment;

class News extends BaseModel
{
    protected $table = 'news';

    static protected $_instance = NULL;

    /**
     * Use singleton pattern
     *
     * @return News object
     */
    static public function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    static public function clearInstance()
    {
        self::$_instance = null;
    }

    protected $fillable = [
        'title_vn', 'slug_vn', 'author_vn', 'content_vn', 'is_del', 'created_at', 'updated_at', 'sapo_vn',
        'title_en', 'slug_en', 'author_en', 'content_en', 'is_del', 'created_at', 'updated_at', 'sapo_en'
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function tags()
    {
        return $this->hasMany(NewsTag::class, 'news_id', 'id');
    }

    public function comment()
    {
        return $this->hasMany(Comment::class, 'news_id', 'id');
    }

    static public function getAllNews($lang, $cat)
    {
        $items = \DB::table('news')->select(
            'news.*',
            'news.title_'.$lang. ' as title',
            'news.author_'.$lang. ' as author',
            'news.sapo_'.$lang. ' as sapo',
            'news.slug_'.$lang. ' as slug',
            'images.path as image'

        )
            ->leftJoin('category_news', 'category_news.news_id', '=', 'news.id')
            ->leftJoin('categories', 'categories.id', '=', 'category_news.category_id')
            ->leftJoin('images', 'images.link_from_id', '=', 'news.id')
            ->distinct('news.id')
            ->where('news.is_del', '=', Business::IS_ACTIVE)
            ->where('images.type', '=', Business::IMAGE_TYPE_NEWS);
            if($cat != 'all'){
                $items->where('categories.cat_slug_'.$lang, '=', $cat);
            }
        $items = $items->get();

            foreach ($items as &$item){
                $item->link_detail = route('client.news_detail', $item->id);
            }

        return $items->toArray();
    }

    static public function getNewsBySlug($lang,$slug)
    {
        $items = \DB::table('news')->select(
            'news.*',
            'news.title_'.$lang. ' as title',
            'news.author_'.$lang. ' as author',
            'news.content_'.$lang. ' as content',
            'news.slug_'.$lang. ' as slug'
        )
            ->where('news.slug_'.$lang, '=', $slug)
            ->where('news.is_del', '=', Business::IS_ACTIVE);

        return $items->first();
    }


}