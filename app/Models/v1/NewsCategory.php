<?php

namespace App\Models\v1;

use App\Models\BaseModel;

class NewsCategory extends BaseModel
{
    protected $table = 'news_category';

    static protected $_instance = NULL;

    /**
     * Use singleton pattern
     *
     * @return NewsCategory object
     */
    static public function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    static public function clearInstance()
    {
        self::$_instance = null;
    }

    protected $fillable = [
        'news_id', 'category_id',
        'created_at', 'updated_at'
    ];
}
