<?php

namespace App\Models\v1;

use App\Helpers\Business;
use App\Models\BaseModel;
use App\Models\v1\Product;
use App\Models\v1\News;

class Category extends BaseModel
{
    protected $table = 'categories';

    static protected $_instance = NULL;

    /**
     * Use singleton pattern
     *
     * @return Category object
     */
    static public function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    static public function clearInstance()
    {
        self::$_instance = null;
    }

    protected $fillable = [
        'name_vn', 'name_en', 'description_vn', 'description_en',
        'is_active', 'type',
        'created_at', 'updated_at',
        'cat_slug_vn', 'cat_slug_en',
        'title_vn', 'title_en',
        'meta_vn', 'meta_en'
    ];

    public function products ()
    {
        return $this->belongsToMany(Product::class);
    }

    public function news ()
    {
        return $this->belongsToMany(News::class);
    }

    static public function getCatByType($lang, $type){
        $items = \DB::table('categories')->select(
            'categories.*',
            'categories.name_'.$lang. ' as name',
            'categories.description_'.$lang. ' as description',
            'categories.cat_slug_'.$lang. ' as cat_slug'
        )
            ->where('categories.type', '=', $type)
            ->where('categories.is_active', '=', Business::IS_ACTIVE);

        return $items->get();

    }
}