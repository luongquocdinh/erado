<?php

namespace App\Models\v1;

use App\Models\BaseModel;

class ProductCategory extends BaseModel
{
    protected $table = 'product_category';

    static protected $_instance = NULL;

    /**
     * Use singleton pattern
     *
     * @return ProductCategory object
     */
    static public function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    static public function clearInstance()
    {
        self::$_instance = null;
    }

    protected $fillable = [
        'product_id', 'category_id',
        'created_at', 'updated_at'
    ];
}