<?php

namespace App\Models\v1;


use App\Helpers\Business;
use App\Models\BaseModel;

class About extends BaseModel
{
    protected $table = 'about';

    static protected $_instance = NULL;

    /**
     * Use singleton pattern
     *
     * @return About object
     */
    static public function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    static public function clearInstance()
    {
        self::$_instance = null;
    }

    protected $fillable = [
        'title_vn', 'slug_vn', 'author_vn', 'content_vn', 'is_del', 'created_at', 'updated_at', 'sapo_vn',
        'title_en', 'slug_en', 'author_en', 'content_en', 'is_del', 'created_at', 'updated_at', 'sapo_en'
    ];

    static public function getAllAbout($lang)
    {
        $items = \DB::table('about')->select(
            'about.*',
            'about.title_'.$lang. ' as title',
            'about.author_'.$lang. ' as author',
            'about.sapo_'.$lang. ' as sapo',
            'about.slug_'.$lang. ' as slug',
            'images.path as image'

        )
            ->leftJoin('images', 'images.link_from_id', '=', 'about.id')
            ->where('about.is_del', '=', Business::IS_ACTIVE)
            ->where('images.type', '=', Business::IMAGE_TYPE_ABOUT);

        $items = $items->get();
        foreach ($items as &$item){
            $item->link_detail = route('client.about_us_detail', $item->id);
        }

        return $items->toArray();
    }

    static public function getAboutBySlug($lang,$slug)
    {
        $items = \DB::table('about')->select(
            'about.*',
            'about.title_'.$lang. ' as title',
            'about.author_'.$lang. ' as author',
            'about.content_'.$lang. ' as content',
            'about.slug_'.$lang. ' as slug'
        )
            ->where('about.slug_'.$lang, '=', $slug)
            ->where('about.is_del', '=', Business::IS_ACTIVE);

        return $items->first();
    }
}