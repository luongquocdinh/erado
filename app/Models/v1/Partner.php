<?php

namespace App\Models\v1;

use App\Models\BaseModel;

class Partner extends BaseModel
{
    protected $table = 'partner';

    static protected $_instance = NULL;

    /**
     * Use singleton pattern
     *
     * @return ProductTag object
     */
    static public function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    static public function clearInstance()
    {
        self::$_instance = null;
    }

    protected $fillable = [
        'name', 'icon', 'link', 'is_active',
        'created_at', 'updated_at'
    ];
}