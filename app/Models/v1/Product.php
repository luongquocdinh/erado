<?php

namespace App\Models\v1;

use App\Helpers\Business;
use App\Models\BaseModel;
use App\Models\v1\Category;
use App\Models\v1\ProductTag;

class Product extends BaseModel
{
    protected $table = 'products';

    static protected $_instance = NULL;

    /**
     * Use singleton pattern
     *
     * @return Product object
     */
    static public function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    static public function clearInstance()
    {
        self::$_instance = null;
    }

    protected $fillable = [
        'name_vn', 'name_en', 'min_price_vn', 'max_price_vn', 'min_price_en', 'max_price_en',
        'sapo_vn', 'sapo_en',  'is_active', 'unit_vn', 'unit_en',
        'description_vn', 'description_en', 'type', 'sku', 
        'slug_vn', 'slug_en', 'created_at', 'updated_at','id','title_vn', 'title_en',
        'meta_vn', 'meta_en'
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function tags()
    {
        return $this->hasMany(ProductTag::class, 'product_id', 'id');
    }

    static public function getAllProduct($lang, $cat)
    {
        $items = \DB::table('products')->select(
            'products.*',
            'products.name_'.$lang. ' as name',
            'products.min_price_'.$lang. ' as min_price',
            'products.max_price_'.$lang. ' as max_price',
            'products.slug_'.$lang. ' as slug',
            'products.unit_'.$lang. ' as unit',
            'products.sapo_'.$lang. ' as sapo',
            'products.description_'.$lang. ' as description'

        )
            ->leftJoin('category_product', 'category_product.product_id', '=', 'products.id')
            ->leftJoin('categories', 'categories.id', '=', 'category_product.category_id')
            ->where('products.is_active', '=', Business::IS_ACTIVE)
            ->distinct('products.id');
        if($cat != 'all'){
            $items->where('categories.cat_slug_'.$lang, '=', $cat);
        }
        $items = $items->get();


        foreach ($items as &$item){
            $item->link_detail = route('client.product_detail', $item->id);
        }

        return $items->toArray();
    }

    static public function getProductBySlug($lang,$slug)
    {
        $items = \DB::table('products')->select(
            'products.*',
            'products.name_'.$lang. ' as name',
            'products.min_price_'.$lang. ' as min_price',
            'products.max_price_'.$lang. ' as max_price',
            'products.slug_'.$lang. ' as slug',
            'products.unit_'.$lang. ' as unit',
            'products.sapo_'.$lang. ' as sapo',
            'products.description_'.$lang. ' as description'
        )
            ->where('products.slug_'.$lang, '=', $slug)
            ->where('products.is_active', '=', Business::IS_ACTIVE);

        return $items->first();
    }
}