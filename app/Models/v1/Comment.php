<?php

namespace App\Models\v1;

use App\Models\BaseModel;
use App\Models\v1\News;

class Comment extends BaseModel
{
    protected $table = 'comment';

    static protected $_instance = NULL;

    /**
     * Use singleton pattern
     *
     * @return Comment object
     */
    static public function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    static public function clearInstance()
    {
        self::$_instance = null;
    }

    protected $fillable = [
        'news_id', 'name', 'email', 'content',
        'created_at', 'updated_at'
    ];

    public function news()
    {
        return $this->belongsTo(News::class, 'id', 'news_id');
    }
}