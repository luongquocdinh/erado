<?php
namespace App\Helpers;

class Business
{
    const IS_ACTIVE = 1;
    const IS_NOT_ACTIVE = 0;

    const LANG_DEFAULT = 'vn';
    const LANG_VN = 'vn';
    const LANG_EN = 'en';

    const PER_PAGE = 10;

    const VALIDATE_ERROR = 402;
    const SUCCESS = 200;

    // IMAGE TYPE
    const IMAGE_TYPE_PRODUCT = 1;
    const IMAGE_TYPE_NEWS = 2;
    const IMAGE_TYPE_PARTNER = 3;
    const IMAGE_TYPE_RECRUITMENT = 4;
    const IMAGE_TYPE_ABOUT = 5;
    const IMAGE_TYPE_CATEGORY_PRODUCT = 6;

    // PRODUCT TYPE
    const PRODUCT_TYPE_1 = 1;
    const PRODUCT_TYPE_2 = 2;
    const PRODUCT_TYPE_3 = 3;

    // CATEGORY TYPE
    const CATEGORY_PRODUCT = 1;
    const CATEGORY_NEWS = 2;
}
