<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_vn');
            $table->string('name_en');
            $table->decimal('min_price_vn', 36, 2);
            $table->decimal('max_price_vn', 36, 2);
            $table->decimal('min_price_en', 36, 2);
            $table->decimal('max_price_en', 36, 2);
            $table->string('unit_vn')->nullable();
            $table->string('unit_en')->nullable();
            $table->text('sapo_vn')->nullable();
            $table->text('sapo_en')->nullable();
            $table->integer('type');
            $table->text('slug_vn');
            $table->text('slug_en');
            $table->text('description_vn')->nullable();
            $table->text('description_en')->nullable();
            $table->string('sku')->nullable();
            $table->boolean('is_active')->default(1);
            $table->integer('created_at')->nullable();
            $table->integer('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
