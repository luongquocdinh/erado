<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title_vn');
            $table->string('title_en');
            $table->string('sapo_vn');
            $table->string('sapo_en');
            $table->text('slug_vn');
            $table->text('slug_en');
            $table->string('author_vn')->nullable();
            $table->string('author_en')->nullable();
            $table->text('content_vn')->nullable();
            $table->text('content_en')->nullable();
            $table->integer('created_at')->nullable();
            $table->integer('updated_at')->nullable();
            $table->integer('is_del')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
