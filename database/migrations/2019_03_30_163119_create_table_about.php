<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAbout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title_vn');
            $table->string('title_en');
            $table->text('sapo_vn');
            $table->text('sapo_en');
            $table->text('slug_vn');
            $table->text('slug_en');
            $table->text('author_vn');
            $table->text('author_en');
            $table->text('content_vn');
            $table->text('content_en');
            $table->boolean('is_del')->default(1);
            $table->integer('created_at')->nullable();
            $table->integer('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_about');
    }
}
