(function( $ ) {
	"use strict";
	var get_woocommerce_content = function (currentUrl) {
		$('.product-layout.products').css('opacity', 0);
		$('.product-layout-wrapper > .wc-loading').removeClass('hide');
		
		
		$( '.product-layout.products' ).find('.item').hide();
		$( '.product-layout.products' ).find('.cat-slug-'+currentUrl).show();

		if(currentUrl == 'all'){
			$( '.product-layout.products' ).find('.item').show();
		}

		$('.product-layout.products').css('opacity', 1);
        $('.product-layout-wrapper > .wc-loading').addClass('hide');
	}
	$(document).on('click', '.filter-grid a', function (e) {
		// $('html, body').animate({
		// 	scrollTop : 0
		// }, 500);

		// This will prevent event triggering more then once
		if (e.handled !== true) {
			e.handled = true;
			e.preventDefault();
			$(this).closest('ul').find('.current-cat').removeClass('current-cat');
			$(this).closest('li').addClass('current-cat');
			$('.yith-wcan-reset-navigation').trigger('click');
			get_woocommerce_content($(this).attr('data-filter'));
		}

	});
})( jQuery );