(function( $ ) {
	"use strict";
	//Helper function to get content by url
	var get_woocommerce_content = function (currentUrl) {
		$('.product-layout.products').css('opacity', 0);
		$('.product-layout-wrapper > .wc-loading').removeClass('hide');
		if (currentUrl) {
			// Make sure the URL has a trailing-slash before query args (fix 301 redirect)
			currentUrl = currentUrl.replace(/\/?(\?|#|$)/, '/$1');
			window.history.pushState({ 'url': currentUrl, 'title': '' }, '', currentUrl);

			$.ajax({
				url: currentUrl,
				dataType: 'html',
				cache: false,
				headers: { 'cache-control': 'no-cache' },
				method: 'POST',
				success: function (response) {

					// Update shop content
					$( '.product-layout.products' ).html($(response).find('.product-layout.products').html());
					$( '.woocommerce-result-count' ).html($(response).find('.woocommerce-result-count').html());
					if($(response).find('.woocommerce-pagination').length > 0) {
						$( '.shop-action-bottom .woocommerce-pagination' ).html($(response).find('.woocommerce-pagination').html());
					} else {
						$( '.shop-action-bottom .woocommerce-pagination' ).empty();
					}

					if($(response).find('.fl-ajax-load-more').length > 0) {
						$( '.fl-ajax-load-more' ).html($(response).find('.fl-ajax-load-more').html());
					} else {
						$( '.fl-ajax-load-more' ).empty();
					}

					$('.shop-filters').html($(response).find('.shop-filters').html());

					document.title = $(response).filter('title').text();
				},
				complete: function () {
					$('.product-layout.products').css('opacity', 1);
	                $('.product-layout-wrapper > .wc-loading').addClass('hide');

					//load price slider again
					$( 'input#min_price, input#max_price' ).hide();
					$( '.price_slider, .price_label' ).show();

					var min_price = $( '.price_slider_amount #min_price' ).data( 'min' ),
						max_price = $( '.price_slider_amount #max_price' ).data( 'max' ),
						current_min_price = $( '.price_slider_amount #min_price' ).val(),
						current_max_price = $( '.price_slider_amount #max_price' ).val();

					$( '.price_slider:not(.ui-slider)' ).slider({
						range: true,
						animate: true,
						min: min_price,
						max: max_price,
						values: [ current_min_price, current_max_price ],
						create: function() {

							$( '.price_slider_amount #min_price' ).val( current_min_price );
							$( '.price_slider_amount #max_price' ).val( current_max_price );

							$( document.body ).trigger( 'price_slider_create', [ current_min_price, current_max_price ] );
						},
						slide: function( event, ui ) {

							$( 'input#min_price' ).val( ui.values[0] );
							$( 'input#max_price' ).val( ui.values[1] );

							$( document.body ).trigger( 'price_slider_slide', [ ui.values[0], ui.values[1] ] );
						},
						change: function( event, ui ) {

							$( document.body ).trigger( 'price_slider_change', [ ui.values[0], ui.values[1] ] );
						}
					});
				}
			});
		}
	}
	//Woocommerce ranged price filter, sorting widget
	$(document).on('click', '.widget_ranged_price_filter li a,.widget_order_by_filter li a, .widget_layered_nav li a', function (e) {
		$('html, body').animate({
			scrollTop : 0
		}, 500);

		// This will prevent event triggering more then once
		if (e.handled !== true) {
			e.handled = true;
			e.preventDefault();
			$(this).closest('ul').find('li.current').removeClass('current');
			$(this).closest('li').toggleClass('current');
			get_woocommerce_content($(this).attr('href'));
		}
	});

	//Woocommerce categories
	$(document).on('click', '#fl-shop-categories li a, .widget_product_categories li a', function (e) {
		$('html, body').animate({
			scrollTop : 0
		}, 500);

		// This will prevent event triggering more then once
		if (e.handled !== true) {
			e.handled = true;
			e.preventDefault();
			$(this).closest('ul').find('.current-cat').removeClass('current-cat');
			$(this).closest('li').addClass('current-cat');
			$('.yith-wcan-reset-navigation').trigger('click');
			get_woocommerce_content($(this).attr('href'));
		}

	});
	//Woocommerce pagination
	$(document).on('click', '.shop-action-bottom .woocommerce-pagination li a', function (e) {
		$('html, body').animate({
			scrollTop : 0
		}, 500);

		// This will prevent event triggering more then once
		if (e.handled !== true) {
			e.handled = true;
			e.preventDefault();
			$('.yith-wcan-reset-navigation').trigger('click');
			get_woocommerce_content($(this).attr('href'));
		}

	});
	//Woocommerce tag cloud widget
	$(document).on('click', '.widget_product_tag_cloud a', function (e) {
		$('html, body').animate({
			scrollTop : 0
		}, 500);

		// This will prevent event triggering more then once
		if (e.handled !== true) {
			e.handled = true;
			e.preventDefault();
			$(this).closest('.tagcloud').find('.current-tag').removeClass('current-tag');
			$(this).addClass('current-tag');
			get_woocommerce_content($(this).attr('href'));
		}
	});
	$(document).on('click', '.price_slider_amount .button', function (e) {
		$('html, body').animate({
			scrollTop : 0
		}, 500);

		if (e.handled !== true) {
			e.handled = true;
			e.preventDefault();
			var min_price = $('.price_slider_amount #min_price').val();
			var max_price = $('.price_slider_amount #max_price').val();
			var l = window.location;
			var shop_uri = l.origin + l.pathname;
			var href = l.href;
			var concat = shop_uri == href  ? '?' : '&';
			href = href + concat + $.param(
					{
						min_price: min_price,
						max_price: max_price
					}
				);
			get_woocommerce_content(href);
		}
	});
})( jQuery );
